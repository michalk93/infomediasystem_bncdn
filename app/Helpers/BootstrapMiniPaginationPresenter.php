<?php
/**
 * Created by PhpStorm.
 * User: mkolbusz
 * Date: 8/7/16
 * Time: 12:47 AM
 */

namespace App\Helpers;


use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Pagination\BootstrapThreePresenter;
use Illuminate\Support\HtmlString;

class BootstrapMiniPaginationPresenter extends BootstrapThreePresenter
{
    /**
     * BootstrapMiniPaginationPresenter constructor.
     */
    public function __construct(Paginator $paginator)
    {
        parent::__construct($paginator);
    }

    /**
     * Convert the URL window into Bootstrap HTML.
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function render()
    {
        if ($this->hasPages()) {
            return new HtmlString(sprintf(
                '<ul class="pagination pagination-sm">%s %s %s</ul>',
                $this->getPreviousButton(),
                $this->getLinks(),
                $this->getNextButton()
            ));
        }

        return '';
    }
}