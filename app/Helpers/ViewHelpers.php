<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;


class ViewHelpers {
    public static function isActiveRoute($routeName){
        return Route::currentRouteName() == $routeName ? 'active' : '';
    }

    public static function getHoursPhrase($hours){
        switch($hours){
            case 1: return "godzina";
            case 2:
            case 3:
            case 4: return "godziny";
            default: return "godzin";
        }
    }

    public static function words($string, $number){
        return Str::words($string, $number);
    }
}