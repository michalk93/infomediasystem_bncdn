<?php namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\School;
use App\Repositories\OfferRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

/**
* Category controller for categories in school's offer
*/
class AdminCategoryController extends Controller
{


	/**
	 * AdminCategoryController constructor.
	 */
	public function __construct(OfferRepository $offer)
	{
		$this->offer = $offer;
	}

	public function getAll($school)
	{
		$categories = Category::getCategories($school, true);
		return view('admin.categories.index')->with('categories', $categories);
	}

	/**
	 * @param $school
	 * @return $this|\Illuminate\Http\RedirectResponse
     */
	public function postNew($school)
	{
		$validator = Validator::make(Input::all(), Category::$rules);
		if($validator->fails()){
			return redirect()->route('admin-categories', $school)->withErrors($validator);
		}
		$id_school 	= School::getId($school);
		$position 	= Category::getMaxPosition($school) + 1;
		$inputs = array('name' => Input::get('name'), 'id_school' => $id_school, 'position' => $position);
		DB::table('categories')->insert($inputs);
		return redirect()->route('admin-categories', $school);
	}

	public function getDelete($school, $id)
	{
		$offers = $this->offer->findWhere(['id_category' => $id]);
		foreach($offers as $offer){
			if($offer->filename != null){
				File::delete('uploaded_files/'.$school.'/'.$offer->filename);
			}
			$offer->delete();
		}

		DB::table('categories')->where('id', $id)->delete();
		return redirect()->route('admin-categories', $school);
	}

	public function getEdit($school, $id)
	{
		$category = Category::get($id);
		return view('admin.categories.edit')->with('category', $category);
	}

	public function postEdit($school, $id){
		$inputs = array('name' => Input::get('name'));

		$validator = Validator::make($inputs, Category::$rules);
		if($validator->fails()){
			return redirect()->route('admin-editcategory', $school)->withErrors($validator);
		}

		DB::table('categories')->where('id', $id)->update($inputs);
		return redirect()->route('admin-categories', $school);

	}

	public function getUp($school, $id)
	{
		$currentPosition = DB::table('categories')->where('id', $id)->select('position')->first()->position;
		$newPosition = $currentPosition - 1;
		DB::table('categories')->where('position', $newPosition)->update(array('position' => $currentPosition));
		DB::table('categories')->where('id', $id)->update(array('position' => $newPosition));
		return redirect()->route('admin-categories', $school);
	}

	public function getDown($school, $id)
	{
		$currentPosition = DB::table('categories')->where('id', $id)->select('position')->first()->position;
		$newPosition = $currentPosition + 1;
		DB::table('categories')->where('position', $newPosition)->update(array('position' => $currentPosition));
		DB::table('categories')->where('id', $id)->update(array('position' => $newPosition));
		return redirect()->route('admin-categories', $school);
	}
}