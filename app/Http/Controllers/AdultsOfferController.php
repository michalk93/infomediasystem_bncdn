<?php namespace App\Http\Controllers;

/**
* Controller for offer of school
*/
class AdultsOfferController extends OfferController
{

	public function postSignup($school, $id)
	{
		$offer = Offer::getOneOffer($school, $id);
		$inputs = Input::all();
		$validator = Validator::make($inputs, ApplicationOffer::$aAultsRules);
		if($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput($inputs);
		}
		$content = View::make('subviews.offer-pdf')->with(array('data' => $inputs, 'offer' => $offer));
		if(Input::has('newsletter')){
			Newsletter::add($inputs['email'], School::getId($school));
		}

		$filename = md5(time()).'.pdf';
		$pdfPath = public_path().'/reports/adults/'.$filename;
		$inputs['filename'] = $filename;
		ApplicationOffer::saveNew($inputs, $offer, $school);
		
		File::put($pdfPath, PDF::load($content)->output());
		$data = array(
			'offer' => $offer,
			'user'	=> $inputs
		);




		Mail::queue('emails.signup-offer', $data, function($message) use ($inputs, $pdfPath){
			$message->to($inputs['email'], $inputs['firstname'].' '.$inputs['lastname'])->subject('Zgłoszenie na kurs');
			$message->attach($pdfPath);
		});
		Session::put('msgSignUpSuccess', Offer::$messages['signup_success']);
		return Redirect::route('offer', $school);
	}


	/**
	* Delete the application item from database and remove application's file from server
	**/
	public function getApplicationDelete($school, $id)
	{
		if(ApplicationOffer::remove($id)){
			return Redirect::route('admin-applications', $school)->with('message', 'Zgłoszenie zostało usunięte pomyślnie.');
		}

		return Redirect::route('admin-applications', $school)->with('message', 'Błąd podczas usuwania zgłoszenia. Skontaktuj się z administratorem.');
	}

};