<?php namespace App\Http\Controllers;

/**
* Controller to manage artisan commands on remote server
*/
class ArtisanController extends Controller
{

	public static function runMigrations($isSeeded = false)
	{
		Artisan::call('migrate');
		if($isSeeded) Artisan::call('db:seed');
	}
}