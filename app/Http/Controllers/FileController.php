<?php namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Page;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class FileController extends Controller {

	public function index()
	{
        $files = File::all();
		return view('admin.file.index', compact('files'));
	}


	public function create()
	{
        $pages = Page::all();
		return view('admin.file.create', compact('pages'));
	}


	public function save(Request $request, $school)
	{
        $validator = Validator::make($request->all(), File::$rules);
        if($validator->fails()){
            return redirect()->route('files-add')->withErrors($validator)->withInput();
        }

        $pageId = $request->input('pages_id');
        $file = $request->file('file');
        $filename = md5(time()).".".$file->getClientOriginalExtension();
        $destinationPath = 'uploaded_files/'.$school.'/pages/'.$pageId;
        if(!\File::exists($destinationPath)){
            \File::makeDirectory($destinationPath, 0777, true);
        }

        $file->move($destinationPath, $filename);
        if($file->isValid()){
            return redirect()->route('files-add')->withInput();
        }

        $request->merge(['filepath' => $filename]);
        File::create($request->except('_token'));

        return redirect()->route('admin-files', $school);
	}


    public function download($school, $id)
    {
        $file = File::find($id);
        if($file){
            $storePath = 'uploaded_files/'.$school.'/pages/'.$file->pages_id.'/'.$file->filepath;
            return response()->download($storePath, Str::ascii($file->filepath));
        }
        return redirect()->back();
    }


    public function delete($school, $id)
    {
        try{
            $file = File::find($id);
            if(!$file) {
                throw new Exception("No file found");
            }

            $storePath = 'uploaded_files/'.$school.'/pages/'.$file->pages_id.'/'.$file->filepath;
            if(!\File::delete($storePath)){
                throw new Exception("Error while deleting file");
            }

            $file->delete();
        } catch (Exception $e) {
            return redirect()->back()->with('message', $e->getMessage());
        }

        return redirect()->back()->with('message', 'File was deleted successfully.');
    }

}