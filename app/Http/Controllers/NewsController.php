<?php namespace App\Http\Controllers;
use App\Models\ApplicationOffer;
use App\Models\News;
use App\Models\Newsletter;
use App\Models\School;
use App\Repositories\NewsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
* Controller to manage news 
*/
class NewsController extends Controller
{



	//------------------------------------------
	// USERS METHODS
	//------------------------------------------
	/**
	 * NewsController constructor.
	 */
	public function __construct(NewsRepository $news)
	{
		$this->news = $news;
	}

	public function getIndex($school)
	{
		$news = News::getFromSchool($school, 8);
		return view('news')->with('news', $news);
	}

	public function getNews($school, $id)
	{
		return json_encode(News::getOne($id));
	}

	public function getApiNews($school)
	{
		$news = News::getFromSchool($school);
		return $news;
	}


	//------------------------------------------
	// ADMIN METHODS
	//------------------------------------------

	/*
	*
	*/
	public function getAll($school)
	{
		$news = News::getFromSchool($school, 10);
		return view('admin.news.index')->with('news', $news);
	}

	public function getNew($school)
	{
		return view('admin.news.add');
	}

	public function postNew(Request $request, $school)
	{
		$inputs = array(
			'content'		=> $request->input('content'),
			'id_school'		=> School::getId($school),
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s'),
		);

		$validator = Validator::make($inputs, News::$rules);
		if($validator->fails()){
			return redirect()->route('admin-addnews', $school)->withErrors($validator)->withInput();
		}

		DB::table('news')->insert($inputs);

		if($request->has('newsletter')){
			Newsletter::sendNews($school, $inputs);
		}

		flash(trans('news.create.success'), 'success');
		return redirect()->route('admin-news', $school);
	}

	public function getDelete($school, $id)
	{
		DB::table('news')->where('id', $id)->delete();
		flash(trans('news.delete.success'), 'success');
		return redirect()->route('admin-news', $school);
	}

	public function getEdit($school, $id)
	{
		$news = News::getOne($id);
		return view('admin.news.edit')->with('news', $news);
	}

	public function postEdit(Request $request, $school, $id)
	{
		$inputs = array(
			'content'		=> $request->input('content'),
			'updated_at'	=> date('Y-m-d H:i:s')
		);

		DB::table('news')->where('id', $id)->update($inputs);
		flash(trans('news.edit.success'), 'success');
		return redirect()->route('admin-news', $school);
	}

	/**
	* 
	**/
	public function getDeleteGroup(Request $request, $school)
	{
		if(DB::table($this->news->makeModel()->getTable())->whereIn('id', $request->input('ids'))->delete() == 0){
			flash(trans('news.delete.error-group'), 'danger');
		}
		flash(trans('news.delete.success-group'), 'success');
		return redirect()->route('admin-news', $school);
	}
}
