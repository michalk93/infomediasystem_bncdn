<?php namespace App\Http\Controllers;
use App\Models\School;
use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
* 
*/
class NewsletterController extends Controller
{
	public function getNewsletter($school)
	{
		return view($school.'.newsletter');
	}

	public function postNewsletter(Request $request, $school)
	{
		$inputs = array(
			'email' 		=> $request->input('email'),
			'id_school' 	=> School::getId($school),
			'created_at'	=> date('Y-m-d')
		);
		$rules 	= array('email' => 'required|email|unique:newsletter,email');
		$validator = Validator::make($inputs, $rules);

		if($validator->fails()){
			return ['result' => $validator->errors()->first('email')];
		}

		DB::table('newsletter')->insert($inputs);
		Mailgun::lists()->addMember(config('mail.newsletter-mailing-list'), ['address' => $inputs['email']]);
		return ['result' => 'Zostałeś pomyślnie zapisany do newslettera'];
	}

	public function getAll(Request $request, $school)
	{
		$school = $request->attributes->get('school');
		$emails = DB::table('newsletter')->where('id_school', $school->id)->orderBy('id', 'DESC')->paginate(10);
		return view('admin.newsletter.index')->with('emails', $emails);
	}	

	public function getDelete($school, $id)
	{
		DB::table('newsletter')->where('id', $id)->delete();
		flash(trans('newsletter.delete.success'));
		return redirect()->route('admin-newsletter', $school);
	}

	public function getUnsubscribe($school)
	{
		return view($school.'.unsubscribe');
	}

	public function postUnsubscribe(Request $request, $school)
	{
		DB::table('newsletter')->where('email', $request->input('email'))->delete();
		Mailgun::lists()->deleteMember(config('mail.newsletter-mailing-list'), $request->input('email'));
		return redirect()->route('school-home', $school);
	}
}