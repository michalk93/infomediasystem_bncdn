<?php namespace App\Http\Controllers;
use App\Events\OfferApplicationWasSubmited;
use App\Helpers\BootstrapMiniPaginationPresenter;
use App\Models\ApplicationOffer;
use App\Models\Category;
use App\Models\News;
use App\Models\Newsletter;
use App\Models\Offer;
use App\Models\Status;
use App\Repositories\CategoryRepository;
use App\Repositories\NewsRepository;
use App\Repositories\OfferRepository;
use Barryvdh\Debugbar\Middleware\Debugbar;
use Bogardo\Mailgun\Facades\Mailgun;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/**
* Controller for offer of school
*/
class OfferController extends Controller
{


	/**
	 * OfferController constructor.
	 */
	public function __construct(OfferRepository $offer, NewsRepository $news, ApplicationOffer $application, CategoryRepository $category)
	{
		$this->offer = $offer;
        $this->application = $application;
		$this->category = $category;
	}

	public function getIndexAdmin(Request $request)
	{
        $school = $request->attributes->get('school');
		if($request->has('set_new_filter')){
			foreach($request->query() as $filter => $value){
				Session::put($filter, $value);
			}
		}
        $offers = $this->offer->with(['category', 'status'])->orderBy('position', 'ASC')->findWhere([
            ['id_status', 'LIKE', Session::get('status_filter', '%')],
            ['id_category', 'LIKE', Session::get('category_filter', '%')],
            ['offer_'.$school->name.'.name', 'LIKE', '%'.Session::get('name_filter').'%']
        ]);

//		$perPage = 10;
//		$offer = new LengthAwarePaginator($offer->slice($request->page*$perPage, $perPage), $offer->count(), $perPage, $request
//		->page);
//		$offer->setPath($offer->resolveCurrentPath());
//        $offer->presenter(function($paginator){
//            return new BootstrapMiniPaginationPresenter($paginator);
//        });

        $perPage = 10;
        $offers = new LengthAwarePaginator($offers->slice(($request->page-1)*$perPage, $perPage), $offers->count(), $perPage, $request->page);
        $offers->setPath($offers->resolveCurrentPath());

		return view('admin.offer.index')
			->with('offers', $offers)
			->with('categories', Category::getCategories($school->name))
			->with('status', Status::getAll());
	}

	public function getNew($school)
	{
		$categories = Category::getCategories($school);
		$status = Status::getAll();
		return view('admin.offer.add')->with(array('school' => $school, 'categories' => $categories, 'status' => $status));
	}

	public function postNew(Request $request, $school)
	{
		$validator = Validator::make($request->all(), Offer::$rules);
		if($validator->fails()){
			return redirect()->route('admin-addoffer', $school)->withErrors($validator)->withInput();
		}
		$file = $request->file('offerfile');
		if($file){
			$filename = md5(time()).".".$file->getClientOriginalExtension();
			$destinationPath = public_path().'/uploaded_files/'.$school;
			$file->move($destinationPath, $filename);
			if($file->isValid()){
				$offer = $this->offer->findWhere(['id_school' => $request->attributes->get('schoool')->id]);
                flash(trans('offer.new.error-file'), 'danger');
				return view('admin.offer.index')
                    ->with(['school' => $school, 'offer' => $offer]);
			}
		}else {
			$filename = null;
		}

		$inputs = array(
			'name'				=> $request->input('name'),
			'id_category'		=> $request->input('id_category'),
			'id_status'			=> $request->input('id_status'),
			'hours'				=> $request->input('hours'),
			'price'				=> $request->input('price'),
			'recipient'			=> $request->input('recipient'),
			'range'				=> $request->input('range'),
			'syllabus'			=> $request->input('syllabus'),
			'qualifications'	=> $request->input('qualifications'),
			'additional'		=> $request->input('additional'),
			'filename'			=> $filename,
			'file_desc'			=> $request->input('file_desc'),
			'created_at'		=> date('Y:m:d'),
			'position'			=> $this->offer->orderBy('position', 'DESC')->findWhere(['id_category' => $request->input('id_category')])->first()->position+1
		);

		if($request->has('on_date')){
			$inputs['date_at'] = $request->input('date_at');
		}

		$this->offer->create($inputs);
		DB::table('categories')->where('id', $inputs['id_category'])->update(['num_offer' => ('num_offer' + 1)]);

		if($request->has('news')){
			$news = News::getNewsOffer($inputs);
			News::add($school, $news['content']);
		}
		if($request->has('newsletter')){
			Newsletter::sendAllNewOffer($school, array('offer' => $inputs, 'category' => Category::get($inputs['id_category'])));
		}

        flash(trans('offer.create.success'), 'success');
		return $this->getIndexAdmin($request);
	}

	public function getIndex(Request $request, $school, $id_category = null)
	{
		$categories = $this->category->orderBy('position', 'ASC')->findWhere(['id_school' => $request->attributes->get('school')->id]);
		if(is_null($id_category)){
			$id_category = $categories->first()->id;
		}
		$offers = $this->offer->orderBy('position', 'ASC')->findWhere(['id_category' => $id_category]);

        if(is_null($request->page)) {
            $request->page = 1;
        }
		$perPage = 7;
		$offers = new LengthAwarePaginator($offers->slice(($request->page-1)*$perPage, $perPage), $offers->count(), $perPage, $request->page);
		$offers->setPath($offers->resolveCurrentPath());

		return view($school.'.offer.index')
			->with('categories', $categories)
			->with('offer', $offers)
			->with('id_category', $id_category);
	}


	public function getEdit(Request $request, $school, $id)
	{
		$offer = $this->offer->with(['status', 'category'])->find($id);
		$categories = $this->category->findWhere(['id_school' => $request->attributes->get('school')->id]);
		$status = Status::getAll();
			return view('admin.offer.edit')
			->with('school', $school)
			->with('offer', $offer)
			->with('categories', $categories)
			->with('status', $status);
	}

	public function postEdit(Request $request, $school, $id)
	{

        $offer = $this->offer->find($id);
		$validator = Validator::make($request->all(), Offer::$rules);
		if($validator->fails()){
			return redirect()->route('admin-editoffer', [$school, $id])->withErrors($validator)->withInput();
		}

		$file = $request->file('offerfile');
		if($file){
			$filename = md5(time()).".".$file->getClientOriginalExtension();
			$destinationPath = 'uploaded_files/'.$school;
			$file->move($destinationPath, $filename);
			if($file->isValid()){
                flash(trans('offer.edit.error-file'), 'danger');
				return redirect()->back();
			}
		}

		$inputs = [
			'name'				=> $request->input('name'),
			'id_category'		=> $request->input('id_category'),
			'id_status'			=> $request->input('id_status'),
			'hours'				=> $request->input('hours'),
			'price'				=> $request->input('price'),
			'recipient'			=> $request->input('recipient'),
			'range'				=> $request->input('range'),
			'syllabus'			=> $request->input('syllabus'),
			'qualifications'	=> $request->input('qualifications'),
			'additional'		=> $request->input('additional'),
			'file_desc'			=> $request->input('file_desc'),
			'created_at'		=> Carbon::now(),
            'date_at'           => null
		];
        
        if($request->has('on_date')){
            $inputs['date_at'] = Carbon::createFromFormat('Y-m-d', $request->input('date_at'));
        }

		if($file){
			$inputs['filename']	= $filename;
		}

        $offer->update($inputs);

        flash(trans('offer.edit.success'), 'success');
		return redirect()->route('admin-offer', $school);
	}

	public function getDelete($school, $id)
	{
		$offer = $this->offer->find($id);
		if($offer->filename != null){
			File::delete('uploaded_files/'.$school.'/'.$offer->filename);
		}
		$offer->delete();
		flash(trans('offer.delete.success'), 'success');
		return redirect()->route('admin-offer', $school);
	}

	public function getDetails($school, $id)
	{
		$offer = DB::table('offer_'.$school)->where('offer_'.$school.'.id', $id)
					->join('categories', 'offer_'.$school.'.id_category', '=', 'categories.id')
					->join('status', 'offer_'.$school.'.id_status', '=', 'status.id')
					->select('offer_'.$school.'.*', 'status.name AS status_name', 'categories.id AS category_id', 'categories.name AS category_name')
					->first();
		$categories = Category::getCategories($school);
		return view($school.'.offer.details')->with(array('offer' => $offer, 'categories' => $categories));
	}


	/**
	* Return download of file located in uploaded_files/$school/ directory
	*/
	public function getDownload($school, $filename)
	{
		return Response::download('uploaded_files/'.$school.'/'.$filename);
	}

	/**
	* Return view with the form to sign up on the course
	*/
	public function getSignup($school, $id)
	{
		$offer = $this->offer->with('category')->find($id);
		return view($school.'.offer.signup')->with('offer', $offer);
	}

	/**
	* Process filled form from the user and save data to databas and generate PDF application
	*/
	public function postSignup(Request $request, $school, $id)
	{
		$offer = $this->offer->find($id);
		$inputs = $request->all();

        $classname = 'App\\Models\\'.ucwords($school).'ApplicationOffer';
		$oApplicationOffer = new $classname;

		$validator = Validator::make($inputs, $oApplicationOffer::$rules);

		if($validator->fails()){
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$inputs['firstname'] = ucwords($inputs['firstname']);
		$inputs['lastname'] = ucwords($inputs['lastname']);
		$inputs['secondname'] = ucwords($inputs['secondname']);
		$inputs['birthplace'] = ucwords($inputs['birthplace']);
		$inputs['created_at'] = Carbon::now();
		$inputs['id_offer'] = $offer->id;
		$inputs['id_school'] = $request->attributes->get('school')->id;
		$inputs['filename'] = md5(time()).'.pdf';

        $oApplicationOffer = $this->application->create($inputs);

		$data = array(
			'offer' => $offer,
			'inputs' => $inputs
		);

		Event::fire(new OfferApplicationWasSubmited($data));

		flash(trans('offer.signup.success'), 'success');
		return redirect()->route('offer', $school);
	}

	/**
	* Download application as PDF file
	**/
	public function getApplicationDownload($school, $id)
	{
		$filename = ApplicationOffer::find($id)->filename;
		return redirect(ApplicationOffer::$pathApplicationFiles."$school/$filename");
	}

	/**
	* Get applications from database and return view with table of applications
	*/
	public function getApplications(Request $request, $school)
	{
		if($request->has('set_new_filter')){
			foreach($request->query() as $filter => $value){
				Session::put($filter, $value);
			}
		}
		$school = $request->attributes->get('school');


		$applications = DB::table('applications')
			->join('offer_'.$school->name, 'offer_'.$school->name.'.id', '=', 'id_offer')
			->where('id_school', $school->id)
			->where('lastname', 'LIKE', '%'.Session::get('lastname_filter').'%')
			->where('offer_'.$school->name.'.name', 'LIKE', '%'.Session::get('course_name_filter').'%')
			->where('email', 'LIKE', '%'.Session::get('email_filter').'%')
			->orderBy('id', 'desc')
			->select('applications.*', 'offer_'.$school->name.'.name AS course_name')
			->paginate(10);
		return view('admin.offer.applications')
			->with('applications', $applications);
	}

	/**
	* Change status of application to confirmed and send e-mail to user to inform him
	*/
	public function getApplicationConfirm($school, $id)
	{
		DB::table('applications')->where('id', $id)->update(array('status' => true));

		$application = DB::table('applications')
			->join('offer_'.$school, 'offer_'.$school.'.id', '=', 'id_offer')
			->where('applications.id', $id)
			->select('applications.*', 'offer_'.$school.'.name AS course_name')
			->first();

        Mailgun::send('emails.application-confirm', array('data' => $application), function($msg) use ($application){
			$msg->to($application->email, $application->firstname.' '.$application->lastname)->subject('Zgłoszenie zatwierdzone');
		});

        flash(trans('offer.application.confirmation.success'), 'success');
		return redirect()->route('admin-applications', $school);
	}

	/**
	* Delete the application item from database and remove application's file from server
	**/
	public function getApplicationDelete($school, $id)
	{
		if(ApplicationOffer::remove($school, $id)){
            flash(trans('offer.application.delete.success'), 'success');
			return redirect()->route('admin-applications', $school);
		}

        flash(trans('offer.application.delete.error'), 'danger');
		return redirect()->route('admin-applications', $school);
	}

	/**
	* Move offer item one up on displaying list
	**/
	public function getOfferUp($school, $id)
	{
		$offer = $this->offer->find($id);
		$offerAbove = $this->offer->orderBy('position', 'ASC')->findWhere([
            'id_category' => $offer->category->id,
            ['position', '<', $offer->position]
        ])->first();

		if($offerAbove != null){
            $positionTmp = $offerAbove->position;
            $offerAbove->update(['position' => $offer->position]);
            $offer->update(['position' => $positionTmp]);

            flash(trans('offer.move-up.success'), 'success');
			return redirect()->route('admin-offer', $school);
		}
        flash(trans('offer.move-up.failed'), 'danger');
		return redirect()->route('admin-offer', $school);
	}

	/**
	* Move offer item one down on displaying list
	**/
	public function getOfferDown($school, $id)
	{
		$offer = $this->offer->find($id);
		$offerBelow = $this->offer->orderBy('position', 'ASC')->findWhere([
            'id_category' => $offer->category->id,
            ['position', '>', $offer->position]
        ])->first();
		if($offerBelow != null){
            $positionTmp = $offerBelow->position;
            $offerBelow->update(['position' => $offer->position]);
            $offer->update(['position' => $positionTmp]);

            flash(trans('offer.move-down.success'), 'success');
			return redirect()->route('admin-offer', $school)->with('message', '');
		}

        flash(trans('offer.move-down.failed'), 'danger');
		return redirect()->route('admin-offer', $school);
	}

};