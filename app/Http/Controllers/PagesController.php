<?php namespace App\Http\Controllers;



use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class PagesController extends Controller
{

    public function show($school, $slug)
    {
        $site = Page::with('files')->where('slug', $slug)->first();
        if(!$site){
            App::abort(404);
        }
        return view($school.'.static-site', compact('site'));
    }


    public function index($school)
    {
        $sites = Page::paginate(10);
        return view('admin.page.index', compact('sites'));
    }


    public function create($school)
    {
        return view('admin.page.create');
    }


    public function edit($school, $id)
    {
        $page = Page::find($id);
        return view('admin.page.edit', compact('page'));
    }


    public function save(Request $request, $school)
    {
        $validator = Validator::make($request->all(), Page::$rules);

        if($validator->fails()){
            return redirect()->route('page-create', $school)->withErrors($validator)->withInput();
        }

        $request->merge(['slug' => Str::slug($request->input('title'))]);
        $page = Page::find($request->input('id'));
        if($page){
            $page->update($request->except('_token'));
        }else{
            $page = new Page($request->except('_token'));
            $page->save();
        }

        return redirect()->route('admin-pages', $school);
    }


    public function delete($school, $id)
    {
        $page = Page::find($id);
        if($page){
            $page->delete();
        }
        return Redirect::route('admin-pages', $school);
    }
}
