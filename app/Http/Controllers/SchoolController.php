<?php namespace App\Http\Controllers;
use App\Http\Requests\ContactFormRequest;
use App\Models\News;
use App\Models\Offer;
use App\Repositories\NewsRepository;
use app\Repositories\SliderRepository;
use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

/**
* School Controller
*/
class SchoolController extends Controller
{


	/**
	 * SchoolController constructor.
	 */
	public function __construct(NewsRepository $news, SliderRepository $slider)
	{
		$this->news = $news;
		$this->slider = $slider;
	}

	public function getIndex(Request $request, $school)
	{
		$news 		= News::getFromSchool($request->attributes->get('school')->name, 4);
		$slides 	= $this->slider->findByField('id_school', $request->attributes->get('school')->id);
		//$lastOffers	= Offer::getLast($school, 5);
		$postgraduateStudies = Offer::getLast($school,5,15);
		return view($school.'.start')->with(array('news' => $news, 'slides' => $slides))
			->with('postgraduateStudies', $postgraduateStudies);
	}

	public function getRecruitment($school)
	{
		return View::make($school.'.recruitment');
	}

	public function getAboutUs($school)
	{
		return View::make($school.'.about-us');
	}

	public function getContact($school)
	{
		return View::make($school.'.contact');
	}

	public function sendEmail(ContactFormRequest $request, $school)
	{
		$inputs = $request->all();

		Mailgun::send('emails.contact.contact-form', $inputs, function($message) use ($inputs){
            $message->from($inputs['email'], $inputs['name']);
            $message->replyTo($inputs['email'], $inputs['name']);
            $message->subject($inputs['subject']);
			$message->to('bncdn@poczta.fm', 'BNCDN');
		});

		return redirect()->route('school-home', ['school' => $school]);

	}

	/**
	* 
	**/
	public function postQuery($school)
	{
		$inputs = Input::all();
		if(!Input::has('question1')) $inputs['question1'] = '';
		if(!Input::has('question2')) $inputs['question2'] = '';
		if(!Input::has('question3')) $inputs['question3'] = '';
		if(!Input::has('question4')) $inputs['question4'] = '';

		$ip = $_SERVER['REMOTE_ADDR'];
		$filename = 'users_paths/query_'.$ip.'.txt';
		if(!file_exists($filename)){
			$handle = fopen($filename, "a");
			fclose($handle);
		};
		$data = date('Y-d-m H:i:s')."\n".$inputs['question1']."\n".$inputs['question2']."\n".$inputs['question3']."\n".$inputs['question4'];
		$data .= "\n\n".$inputs['question1-text']."\n\n".$inputs['question2-text']."\n\n".$inputs['question3-text']."\n\n".$inputs['question5-text']."\n----------\n";
		File::append($filename, $data);

		return Redirect::back();
	}
}