<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;

/**
* Search Controller
*/
class SearchController extends Controller
{
	public function getIndex(Request $request, $school)
	{
		$rules = array('search_text' => 'required|min:3');
		$validator = \Validator::make($request->all(), $rules);
		if($validator->fails()){
			return view($school.'.search-results')->with('error', $validator->messages()->first('search_text'));
		}
		$search_text = $request->input('search_text');
		$results = array('offer' => array(), 'news' => array());

		$results['offer'] = \DB::table('offer_'.$school)->where('name', 'like', '%'.$search_text.'%')->get();
		$results['news'] = \DB::table('news')->where('id_school', $request->attributes->get('school')->id)->where('content', 'like', '%'.$search_text.'%')->get();

		return view($school.'.search-results')->with('results', $results);
	}
}