<?php namespace App\Http\Controllers;

use App\Models\School;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag as MessageBag;
use Intervention\Image\Facades\Image;
use MyValidator;

/**
* Slider controller
*/
class SliderController extends Controller
{
	//---------------------------------------
	// VARIABLES
	//---------------------------------------

	//---------------------------------------
	// METHODS
	//---------------------------------------
	public function getIndex($school)
	{
		$slides = Slider::getSlides($school);
		return view('admin.slider.index')->with(array('slides' => $slides));
	}

	public function getNew($school)
	{
		return view('admin.slider.add')->with('school', $school);
	}

	public function postNew(Request $request, $school)
	{
		$validator = Validator::make($request->all(), Slider::$rules);
		if($validator->fails()){
			return redirect()->route('admin-addslide', $school)->withErrors($validator)->withInput();
		}

		$filename = $this->uploadImage($request->file('file'), $school);

		if(!$filename){
			$messageBag = new MessageBag();
			$messageBag->add('file', MyValidator::$messages['file']['error']);

			return redirect()->route('admin-addslide', array('school' => $school))->withErrors($messageBag);
		}

		$inputs = array(
			'filename'		=> $filename,
			'description'	=> $request->input('description'),
			'id_school'		=> School::getId($school),
			'link'			=> $request->input('link')
		);

		DB::table('sliders')->insert($inputs);

		flash(trans('slider.create.success'), 'success');
		return redirect()->route('admin-slider', ['school' => $school]);
	}

	public function getEdit($school, $id)
	{
		$slide = Slider::getSlide($id);
		return view('admin.slider.edit')->with('slide', $slide);
	}

	public function postEdit(Request $request, $school, $id)
	{
		$school = $request->attributes->get('school');
		$validator = Validator::make($request->all(), Slider::$editRules);
		if($validator->fails()){
			dd($validator->errors());
			return redirect()->route('admin-editslide', [$school->name, $id])->withErrors($validator)->withInput();
		}
		$inputs = array();
		if($request->file('image')){
			$filename = $this->uploadImage($request->file('image'), $school->name);
			if(!$filename){
				return redirect()->route('admin-addslide', ['school' => $school->name, 'message' => MyValidator::$messages['file']['error']]);
			}

			Slider::deleteImage($school->name, $id);

			$inputs['filename']	= $filename;
		}

		$inputs['description'] = $request->input('description');
		$inputs['link'] = $request->input('link');
		

		DB::table('sliders')->where('id', $id)->update($inputs);

		flash(trans('slider.edit.success'), 'success');
		return redirect()->route('admin-slider', $school->name);
	}

	/*
	* Deleting slide from database and slide's image
	* @school 	Name of school
	* @id 		Id of slide
	*/
	public function getDelete($school, $id)
	{
//		$file = DB::table('sliders')->where('id', $id)->select('filename')->first();
		Slider::deleteImage($school, $id);
		DB::table('sliders')->where('id', $id)->delete();

		flash(trans('slider.delete.success'), 'success');
		return redirect()->route('admin-slider', $school);
	}

	//----------------------------------------------------
	// PRIVATE METHODS
	//----------------------------------------------------

	private function uploadImage($file, $school){
		$image 	= Image::make($file->getRealPath());						

		// Checking if image have right dimensions
		if($image->width() != Slider::SLIDE_WIDTH && $image->height() != Slider::SLIDE_HEIGHT){
			return false;
		}

		$filename = md5(time()).".".$file->getClientOriginalExtension();	// Creating filename with md5 hashing
		$destinationPath = Slider::$uploadPath.$school;						// Path where will storage the file
		$file->move($destinationPath, $filename);
		if($file->isValid()){
			return false;
		}

		return $filename;

	}
}