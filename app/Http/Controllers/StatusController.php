<?php namespace App\Http\Controllers;
use App\Models\Status;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
* Status Controller
*/
class StatusController extends Controller
{
	/**
	 * @var StatusRepository
	 */
	protected $status;



	/**
	 * StatusController constructor.
	 */
	public function __construct(StatusRepository $status)
	{
		$this->status = $status;
	}


	public function getIndex()
	{
		return view('admin.status.index')->with('status', $this->status->all());
	}

	public function postNew(Request $request)
	{
		$validator = Validator::make($request->all(), Status::$rules);
		if($validator->fails()){
			return redirect()->route('admin-status')->withErrors($validator);
		}
		$this->status->create(['name' => $request->input('name')]);
		flash(trans('status.add.success'), 'success');
		return redirect()->route('admin-status');
	}

	public function getEdit($id_status)
	{
		return view('admin.status.edit')->with('status', $this->status->find($id_status));
	}

	public function postEdit(Request $requet, $id_status)
	{
		$validator = Validator::make($requet->all(), Status::$rules);
		if($validator->fails()){
			return redirect()->back()->withErrors($validator);
		}

		$this->status->update(['name' => $requet->input('name')], $id_status);
		flash(trans('status.edit.success'), 'success');
		return redirect()->route('admin-status');
	}

	public function getDelete($id_status)
	{
		$this->status->delete($id_status);
		flash(trans('status.delete.success'), 'success');
		return redirect()->back();
	}
}