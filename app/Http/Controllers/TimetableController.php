<?php namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\School;
use App\Models\Timetable;
use App\Repositories\CategoryRepository;
use app\Repositories\TimetableRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

/**
* Timetable controller
*/
class TimetableController extends Controller
{
	/**
	 * TimetableController constructor.
	 */
	public function __construct(TimetableRepository $timetable, CategoryRepository $category)
	{
		$this->timetable = $timetable;
		$this->category = $category;
	}


	/**
	* Display list of timetable
	**/
	public function getIndex($school, $id_category = null)
	{
		if(is_null($id_category)){
			$category = Category::getFirstCategory($school);
			if(!$category){
				return view($school.'.timetable')->with(array('categories' => null, 'offer' => null, 'id_category' => null));
			}
			$id_category = $category->id;
		}

		$timetables = Timetable::where('id_category', $id_category)->orderBy('updated_at', 'DESC')->get();
		$categories = Category::getCategories($school);
		return view($school.'.timetable')
			->with('timetables', $timetables)
			->with('categories', $categories)
			->with('id_category', $id_category);
	}


//ADMIN
	/**
	* Display list of timetable for admin
	**/
	public function getIndexAdmin(Request $request, $school)
	{
		$timetables = $this->timetable->findWhere(['id_school' => $request->attributes->get('school')->id]);

		$perPage = 10;
		$timetables = new LengthAwarePaginator($timetables->slice($request->page*$perPage, $perPage), $timetables->count(), $perPage, $request
			->page);
		$timetables->setPath($timetables->resolveCurrentPath());

		return view('admin.timetable.index')
			->with('timetables', $timetables);
	}


	/**
	* Display form to add new timetable file
	**/
	public function getNew(Request $request, $school)
	{
		$categories = $this->category->findWhere(['id_school' => $request->attributes->get('school')->id]);
		return view('admin.timetable.new')
			->with('categories', $categories);
	}

	/**
	* Processing data from form to add new timetable file
	**/
	public function postNew(Request $request, $school)
	{
		$validator = Validator::make($request->all(), Timetable::$rules);

		if($validator->fails()){
			return redirect()->route('admin-timetable-add', $school)->withErrors($validator)->withInput();
		}

		$file = $request->file('file');
		$filename = md5(time()).".".$file->getClientOriginalExtension();
		$destinationPath = public_path().Timetable::$storePath.$school;
		$file->move($destinationPath, $filename);
		if($file->isValid()){
			$messageBag = new MessageBag(array('file' => 'Błąd podczas przesyłania pliku. Spróbuj ponownie lub skontaktuj się z administratorem.'));
			return redirect()->route('admin-timetable-add', $school)->withErrors($messageBag)->withInput();
		}

		$inputs = array(
			'id_category'	=> $request->input('id_category'),
			'id_school'		=> School::getId($school),
			'description'	=> $request->input('description'),
			'filename'		=> $filename,
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s')
		);

		DB::table('timetable')->insert($inputs);
		flash(trans('timetable.create.success'), 'success');
		return redirect()->route('admin-timetable', $school);
	}

	/**
	* 
	**/
	public function getDelete($school, $id)
	{
		$timetable = $this->timetable->find($id);
		if(File::delete(public_path().Timetable::$storePath.$school.'/'.$timetable->filename)){
			flash(trans('timetable.delete.success'), 'success');
			$timetable->delete();
		}else {
			flash(trans('timetable.delete.error-file'), 'danger');
		}

		return redirect()->route('admin-timetable', $school);
	}


	/**
	* 
	**/
	public function getTimetableDownload($school, $id)
	{
		$timetable = Timetable::find($id);
		return Response::download(Timetable::$storePath.$school.'/'.$timetable->filename, Str::ascii($timetable->description));
	}

	/**
	* Display form to edit timetable
	**/
	public function getEdit($school, $id)
	{
		return view('admin.timetable.edit')
			->with('categories', Category::getCategories($school))
			->with('timetable', Timetable::find($id));
	}

	/**
	* Processing edit form of timetable
	**/
	public function postEdit(Request $request, $school, $id)
	{
		$validator = Validator::make($request->all(), Timetable::$rulesUpdate);

		if($validator->fails()){
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$timetable = $this->timetable->find($id);
		$file = $request->file('file');

		if($file){
			if(!File::delete(public_path().Timetable::$storePath.$school.'/'.$timetable->filename)) {
				$messageBag = new MessageBag(array('file' => trans('timetable.edit.error-file-delete')));
				return redirect()->back()->withErrors($messageBag)->withInput();
			}

			$filename = md5(time()).".".$file->getClientOriginalExtension();
			$destinationPath = public_path().Timetable::$storePath.$school;
			$file->move($destinationPath, $filename);

			if($file->isValid()){
				$messageBag = new MessageBag(array('file' => trans('timetable.edit.error-file-upload')));
				return redirect()->back()->withErrors($messageBag)->withInput();
			}
			$timetable->filename = $filename;

		}

		$timetable->id_category = $request->input('id_category');
		$timetable->id_school = $request->attributes->get('school')->id;
		$timetable->description = $request->input('description');
		$timetable->updated_at = date('Y-m-d H:i:s');

		$timetable->save();

		flash(trans('timetable.edit.success'), 'success');
		return redirect()->route('admin-timetable', $school);
	}
}