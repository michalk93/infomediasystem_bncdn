<?php namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\News;
use App\Models\Newsletter;
use App\Models\Offer;
use App\Models\Slider;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

/**
* UserController class for controlling users
*/
class UserController extends Controller
{
	/**
	* Get start view when user is logged or login form when is unlogged
	*/
	public function getIndex()
	{
		if(Auth::check()){
			$offer_number 		= Offer::getRecordsNumber();
			$news_number 		= News::getRecordsNumber();
			$slides_number		= Slider::getRecordsNumber();
			$newsletter_number	= Newsletter::getRecordsNumber();
			$categories_number	= Category::getRecordsNumber();
			$status_number		= Status::getRecordsNumber();
			$applications_number = array(
				'all'	=> DB::table('applications')->count(),
				'confirmed' => DB::table('applications')->where('status', 1)->count(),
				'notconfirmed' => DB::table('applications')->where('status', 0)->count(),
				'teachers'	=> array(
					 'all' => DB::table('applications')->where('id_school', 1)->count(),
					 'confirmed' => DB::table('applications')->where('id_school', 1)->where('status', 1)->count(),
					 'notconfirmed' => DB::table('applications')->where('status', 0)->where('id_school', 1)->count(),
				),
				'adults' => array(
					'all' => DB::table('applications')->where('id_school', 2)->count(),
					'confirmed' => DB::table('applications')->where('id_school', 2)->where('status', 1)->count(),
					'notconfirmed' => DB::table('applications')->where('status', 0)->where('id_school', 2)->count(),
				)
			);
			return view('admin.start')->with(array(
				'offer_number' => $offer_number,
				'news_number' => $news_number,
				'slides_number' => $slides_number,
				'newsletter_number' => $newsletter_number,
				'categories_number' => $categories_number,
				'status_number'		=> $status_number,
				'applications_number' => $applications_number
			));
		}
		return redirect()->route('admin-login');
	}

	public function getLogin()
	{
		if(!Auth::check()){
			return view('admin.login');
		}
		return redirect()->route('admin-start');
	}
	
	/**
	* Login process for user
	*/
	public function postLogin(Request $request)
	{
		if(Auth::check()){
			return view('admin.start');
		}
		$email		= $request->input('email');
		$password	= $request->input('password');

		if(Auth::attempt(array('email' => $email, 'password' => $password), true)){
			return redirect()->route('admin-start');
		}

		flash(trans('auth.failed'), 'danger');
		return view('admin.login');
	}

	public function logout()
	{
		if(Auth::check()){
			Auth::logout();
			flash(trans('auth.logout'), 'success');
			return redirect()->route('admin-login');
		}
	}

	public function getChangePassword()
	{
		return view('admin.password');
	}

	public function postChangePassword(Request $request, $id)
	{
		$rules = array(
			'password'				=> 'required|min:5|confirmed',
			'password_confirmation'	=> 'required|min:5'
		);

		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			return redirect()->route('admin-change-password')->withErrors($validator);
		}
		
		$inputs = array('password' => Hash::make(Input::get('password')));
		DB::table('users')->where('id', $id)->update($inputs);
		return redirect()->route('admin-start');
	}
}