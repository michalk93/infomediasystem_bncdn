<?php

namespace App\Http\Middleware;

use app\Repositories\SchoolRepository;
use Closure;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class SchoolIdentifier
{
    /**
     * SchoolIdentifier constructor.
     */
    public function __construct(SchoolRepository $school)
    {
        $this->school = $school;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = Route::getRoutes()->match($request);
        $school = $this->school->findByField('name', $route->parameter('school'))->first();
        $request->attributes->add(['school' => $school]);
        View::share('school', $school);
        return $next($request);
    }
}
