<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route to home site with two type of schools to choose
Route::get('/', array('as' => 'home', function()
{
    return Redirect::route('school-home', 'teachers');
}));



/*Route::get('iron', function(){
	$data = array('name' => 'test variable');
	Mail::queue('emails.test', $data, function($message) {
		$message->to('adres@cmoki.pl', 'MK')->subject('Test message');
	});
});*/
/**
 * ADMINISTRATOR SITES ROUTING
 */
Route::group(array('prefix' => 'admin'), function()
{

//	Route::get('migrate/{isSeeded?}', 'ArtisanController@runMigrations');

    // Administration login page or start page when user is logged
    Route::get('login', array('as' => 'admin-login', 'uses' => 'UserController@getLogin'));
    Route::post('login', array('uses' => 'UserController@postLogin'));

    Route::group(array('before' => ''), function(){
        // Administration start page (access after successfully login)
        Route::any('/', array('as' => 'admin-start', 'uses' => 'UserController@getIndex'));
        Route::get('logout', array('as' => 'admin-logout', 'uses' => 'UserController@logout'));
        Route::get('status', array('as' => 'admin-status', 'uses' => 'StatusController@getIndex'));
        Route::post('status', array('as' => 'admin-status-post', 'uses' => 'StatusController@postNew'));
        Route::get('editstatus/{id_status}', array('as' => 'admin-editstatus', 'uses' => 'StatusController@getEdit'));
        Route::post('editstatus/{id_status}', array('as' => 'admin-editstatus-post', 'uses' => 'StatusController@postEdit'));
        Route::get('delstatus/{id_status}', array('as' => 'admin-delstatus', 'uses' => 'StatusController@getDelete'));
        Route::get('password', array('as' => 'admin-change-password', 'uses' => 'UserController@getChangePassword'));
        Route::post('password/{id}', array('as' => 'admin-change-password-post', 'uses' => 'UserController@postChangePassword'));

        Route::group(array('prefix' => '{school}'), function(){

            // Offer routes
            Route::group(array('prefix' => 'offer'), function(){
                Route::get('/', array('as' => 'admin-offer', 'uses' => 'OfferController@getIndexAdmin'));
                Route::get('add', array('as' => 'admin-addoffer', 'uses' => 'OfferController@getNew'));
                Route::post('add', array('as' => 'admin-addoffer-post', 'uses' => 'OfferController@postNew'));
                Route::get('edit/{id}', array('as' => 'admin-editoffer', 'uses' => 'OfferController@getEdit'));
                Route::post('edit/{id}', array('as' => 'admin-editoffer-post', 'uses' => 'OfferController@postEdit'));
                Route::get('delete/{id}', array('as' => 'admin-deloffer', 'uses' => 'OfferController@getDelete'));
                Route::get('applications', array('as' => 'admin-applications', 'uses' => 'OfferController@getApplications'));
                Route::get('applications/confirm/{id}', array('as' => 'admin-application-confirm', 'uses' => 'OfferController@getApplicationConfirm'));
                Route::get('applications/delete/{id}', array('as' => 'admin-application-delete', 'uses' => 'OfferController@getApplicationDelete'));
                Route::get('application/moveup/{id}', array('as' => 'admin-offer-up', 'uses' => 'OfferController@getOfferUp'));
                Route::get('application/movedown/{id}', array('as' => 'admin-offer-down', 'uses' => 'OfferController@getOfferDown'));
                Route::get('application/download/{id}', array('as' => 'admin-application-download', 'uses' => 'OfferController@getApplicationDownload'));
            });


            // Categroies routes
            Route::group(array('prefix' => 'categories'), function(){
                Route::get('/', array('as' => 'admin-categories', 'uses' => 'AdminCategoryController@getAll'));
                Route::post('/', array('as' => 'admin-categories-post', 'uses' => 'AdminCategoryController@postNew'));
                Route::get('delete/{id}', array('as' => 'admin-delcategory', 'uses' => 'AdminCategoryController@getDelete'));
                Route::get('edit/{id}', array('as' => 'admin-editcategory', 'uses' => 'AdminCategoryController@getEdit'));
                Route::post('edit/{id}', array('as' => 'admin-editcategory-post', 'uses' => 'AdminCategoryController@postEdit'));
                Route::get('up/{id}', array('as' => 'admin-upcategory', 'uses' => 'AdminCategoryController@getUp'));
                Route::get('down/{id}', array('as' => 'admin-downcategory', 'uses' => 'AdminCategoryController@getDown'));
            });


            // News routes
            Route::group(array('prefix' => 'news'), function(){
                Route::get('/', array('as' => 'admin-news', 'uses' => 'NewsController@getAll'));
                Route::get('add', array('as' => 'admin-addnews', 'uses' => 'NewsController@getNew'));
                Route::post('add', array('as' => 'admin-addnews-post', 'uses' => 'NewsController@postNew'));
                Route::get('edit/{id}', array('as' => 'admin-editnews', 'uses' => 'NewsController@getEdit'));
                Route::post('edit/{id}', array('as' => 'admin-editnews-post', 'uses' => 'NewsController@postEdit'));
                Route::get('delete/{id}', array('as' => 'admin-delnews', 'uses' => 'NewsController@getDelete'));
                Route::post('group/delete', array('as' => 'admin-delnews-group', 'uses' => 'NewsController@getDeleteGroup'));
            });

            // Newsletter routes
            Route::group(array('prefix' => 'newsletter'), function(){
                Route::get('/', array('as' => 'admin-newsletter', 'uses' => 'NewsletterController@getAll'));
                Route::get('/delete/{id}', array('as' => 'admin-delnewsletter', 'uses' => 'NewsletterController@getDelete'));
            });

            // Slider routes
            Route::group(array('prefix' => 'slider'), function(){
                Route::get('/', array('as' => 'admin-slider', 'uses' => 'SliderController@getIndex'));
                Route::get('add', array('as' => 'admin-addslide', 'uses' => 'SliderController@getNew'));
                Route::post('add', array('as' => 'admin-addslide-post', 'uses' => 'SliderController@postNew'));
                Route::get('delete/{id}', array('as' => 'admin-delslide', 'uses' => 'SliderController@getDelete'));
                Route::get('edit/{id}', array('as' => 'admin-editslide', 'uses' => 'SliderController@getEdit'));
                Route::post('edit/{id}', array('as' => 'admin-editslide-post', 'uses' => 'SliderController@postEdit'));
            });

            Route::group(array('prefix' => 'timetable'), function(){
                Route::get('/', array('as' => 'admin-timetable', 'uses' => 'TimetableController@getIndexAdmin'));
                Route::get('add', array('as' => 'admin-timetable-add', 'uses' => 'TimetableController@getNew'));
                Route::post('add', array('as' => 'admin-timetable-add-post', 'uses' => 'TimetableController@postNew'));
                Route::get('delete/{id}', array('as' => 'admin-timetable-delete', 'uses' => 'TimetableController@getDelete'));
                Route::get('edit/{id}', array('as' => 'admin-timetable-edit', 'uses' => 'TimetableController@getEdit'));
                Route::post('edit/{id}', array('as' => 'admin-timetable-edit-post', 'uses' => 'TimetableController@postEdit'));
            });

            Route::group(['prefix' => 'pages'], function(){
                Route::get('/', ['as' => 'admin-pages', 'uses' => 'PagesController@index']);
                Route::get('/create', ['as' => 'page-create', 'uses' => 'PagesController@create']);
                Route::post('/create', ['as' => 'admin-page-create-post', 'uses' => 'PagesController@save']);
                Route::get('/delete/{id}', ['as' => 'page-delete', 'uses' => 'PagesController@delete']);
                Route::get('/edit/{id}', ['as' => 'page-edit', 'uses' => 'PagesController@edit']);
            });

            Route::group(['prefix' => 'files'], function(){
                Route::get('/', ['as' => 'admin-files', 'uses' => 'FileController@index']);
                Route::get('/create', ['as' => 'files-add', 'uses' => 'FileController@create']);
                Route::post('/save', ['as' => 'files-add-post', 'uses' => 'FileController@save']);
                Route::get('/delete/{id}', ['as' => 'files-delete', 'uses' => 'FileController@delete']);
            });
        });
    });

});


/**
 *	USERS SITES ROUTING
 */

Route::group(array('prefix' => '{school}'), function(){

    // TEMP ROUTES
    Route::post('/szukaj', array('as' => 'query-form', 'uses' => 'SchoolController@postQuery'));

    // Teacherschool start site
    Route::get('/', array('as' => 'school-home', 'uses' => 'SchoolController@getIndex'));

    // Offert site
    Route::get('offer/{id_category?}', array('as' => 'offer', 'uses' => 'OfferController@getIndex'));
    Route::get('oferta/{id}/szczegoly', array('as' => 'offer-details', 'uses' => 'OfferController@getDetails'));
    Route::get('offer/files/{filename}', array('as' => 'offer-download', 'uses' => 'OfferController@getDownload'));
    Route::get('oferta/{id}/zapisz-sie', array('as' => 'offer-signup', 'uses' => 'OfferController@getSignup'));
    Route::post('oferta/{id}/zapisz-sie', array('as' => 'offer-signup-post', 'uses' => 'OfferController@postSignup'));

    // Recruitment site
    Route::get('recruitment', array('as' => 'recruitment', 'uses' => 'SchoolController@getRecruitment'));

    // About us site
    Route::get('about', array('as' => 'about', 'uses' => 'SchoolController@getAboutUs'));

    // Contact site
    Route::get('contact', array('as' => 'contact', 'uses' => 'SchoolController@getContact'));
    Route::post('contact', array('as' => 'contact-email-send', 'uses' => 'SchoolController@sendEmail'));

    // News site
    Route::get('news/{id}', array('as' => 'news', 'uses' => 'NewsController@getNews'));
    Route::get('news/archives', array('as' => 'news-archives', 'uses' => 'NewsController@getIndex'));

    // Seacher site
    Route::get('search', array('as' => 'search', 'uses' => 'SearchController@getIndex'));

    // Newsletter
    Route::get('newsletter', array('as' => 'newsletter', 'uses' => 'NewsletterController@getNewsletter'));
    Route::post('newsletter', array('as' => 'newsletter-post', 'uses' => 'NewsletterController@postNewsletter'));
    Route::get('newsletter/unsubscribe', array('as' => 'newsletter-unsubscribe', 'uses' => 'NewsletterController@getUnsubscribe'));
    Route::post('newsletter/unsubscribe', array('as' => 'newsletter-unsubscribe-post', 'uses' => 'NewsletterController@postUnsubscribe'));

    // Timetable
    Route::get('timetable/{id?}', array('as' => 'timetable', 'uses' => 'TimetableController@getIndex'));
    Route::get('timetable/download/{id}', array('as' => 'timetable-download', 'uses' => 'TimetableController@getTimetableDownload'));
    Route::get('timetable/show/{id}', array('as' => 'timetable-show', 'uses' => 'TimetableController@getTimetableShow'));


    Route::get('forum', array('as' => 'module-forum', function($school){
        //return Redirect::home();
        return Redirect::intended('http://www.qa.bncdn.pl');
    }));
    Route::get('elearning', array('as' => 'module-elearning', function($school){
        return Redirect::intended('http://www.platforma.bncdn.pl');
        //return header('Location: http://www.platforma.bncdn.pl');
    }));

    Route::get("/{slug}",['as' => 'static_site', 'uses' => 'PagesController@show']);
    Route::get('/download/{id}', ['as' => 'files-download', 'uses' => 'FileController@download']);

});

Route::group(array('prefix' => 'api/{school}'), function(){
    Route::get('news', ['uses' => 'NewsController@getApiNews']);
});
