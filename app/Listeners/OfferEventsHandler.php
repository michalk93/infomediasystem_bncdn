<?php

namespace App\Listeners;

use App\Models\Newsletter;
use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Request;
use Vsmoraes\Pdf\Pdf;

class OfferEventsHandler implements ShouldQueue {
    /**
     * OfferEventsHandler constructor.
     */
    public function __construct(Request $request, Pdf $pdf)
    {
        $this->request = $request;
        $this->pdf = $pdf;
    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen('App\Events\OfferApplicationWasSubmited', 'App\Listeners\OfferEventsHandler@pdf');
        $events->listen('App\Events\OfferApplicationWasSubmited', 'App\Listeners\OfferEventsHandler@mail');
        $events->listen('App\Events\OfferApplicationWasSubmited', 'App\Listeners\OfferEventsHandler@newsletter');
    }


    /**
     * Generate application form PDF and save it
     *
     * @param \App\Events\OfferApplicationWasSubmited $event
     * @return void
     */
    public function pdf($event)
    {

        $school = $this->request->attributes->get('school');
        $content = view($school->name."/offer/pdf-application")->with(array('data' => $event->data))->render();
        $directory = public_path().'/reports/'.$school->name.'/';
        File::put($directory.$event->data['inputs']['filename'],  $this->pdf->load($content, 'A4', 'portrait')->output());
    }

    /**
     * Send e-mail notification with application form as attachment about sign up process
     *
     * @param \App\Events\OfferApplicationWasSubmited $event
     */
    public function mail($event)
    {
        $data = $event->data;

        $school = $this->request->attributes->get('school');

        Mailgun::send('emails.signup-offer', $data, function($message) use ($data, $school){
            $message->to($data['inputs']['email'], $data['inputs']['firstname'].' '.$data['inputs']['lastname'])->subject('Zgłoszenie na kurs - '.$data['offer']->name);

            $directory = public_path().'/reports/'.$school->name.'/';
            $message->attach($directory.$data['inputs']['filename']);
        });
    }



    /**
     * Add email address to newsletter
     *
     * @param \App\Events\OfferApplicationWasSubmited $event
     * @return bool
     */
    public function newsletter($event)
    {
        if(!isset($event->data['inputs']['newsletter'])){
            return;
        }
        $rules 	= array('email' => 'required|email|unique:newsletter,email');
        $validator = Validator::make($event->data['inputs'], $rules);

        if($validator->fails()){
            return false;
        }

        Newsletter::create($event->data['inputs']);
        Mailgun::lists()->addMember(config('mail.newsletter-mailing-list'), ['address' => $event->data['inputs']['email']]);
        return true;
    }
}