<?php
namespace App\Models;

/**
* Application of offer model
*/
class AdultsApplicationOffer extends ApplicationOffer
{
	public static $rules = array(
		'lastname' => 'required|alpha_pl',
		'firstname' => 'required|alpha_pl',
		'secondname' => 'alpha_pl',
		'birthdate' => 'required|date|before:1995-01-01',
		'birthplace' => 'required|alpha_pl',
		'address' => 'required',
		'email' => 'required|email',
		'phonenumber' => 'required',
		'education' => 'required',
		'agreement' => 'accepted'
	);
	
	// Table name in database
	protected $table = "applications";

	// Path to folder where apllications were stored
	public static $pathApplicationFiles = '/reports/adults/';

}