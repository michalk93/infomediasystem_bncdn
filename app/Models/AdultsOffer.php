<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdultsOffer extends Offer
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'offer_adults';
}
