<?php
namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

/**
* Application of offer model
*/
class ApplicationOffer extends Model
{
	// Table name in database
	protected $table = "applications";

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['firstname', 'lastname', 'email', 'id_offer', 'id_school', 'filename', 'status', 'created_at'];

	// Path to folder where apllications were stored
	public static $pathApplicationFiles = '/reports/';


	/**
	* Save new application
	**/
	public static function saveNew($aDataUser, $oOffer, $strSchool)
	{
		$inputs = array(
			'firstname'	=> $aDataUser['firstname'],
			'lastname'	=> $aDataUser['lastname'],
			'email'		=> $aDataUser['email'],
			'id_offer'	=> $oOffer->id,
			'id_school'	=> School::getId($strSchool),
			'filename'	=> $aDataUser['filename'],
			'created_at'=> Carbon::now()
		);

		return DB::table('applications')->insert($inputs);
	}

	/**
	* Remove application file and row from database
	**/
	public static function remove($school, $id)
	{
		$application = self::find($id);
		if(self::removeApplicationFile($school, $application->filename)){
			return $application->delete();
		}
		return false;
	}

	/**
	* Remove application file from server
	**/
	public static function removeApplicationFile($school, $filename)
	{
		return File::delete(public_path().self::$pathApplicationFiles.$school."/".$filename);
	}


	public function school()
	{
		return $this->hasOne('App\Models\School', 'id_school', 'id');
	}

	public function offer()
	{
		return $this->hasOne('App\Models\Offer', 'offer');
	}

}