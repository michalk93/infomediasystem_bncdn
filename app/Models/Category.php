<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
* Category model
*/
class Category extends Model
{
	//-----------------------------------------
	// Variables
	//-----------------------------------------

	// Rules for validation
	public static $rules = array(
		'name'	=> 'required|between:5,100|unique:categories,name',
	);

	// Number of items displayed per page
	public static $itemsPerPage = 10;


	//-----------------------------------------
	// Methods
	//-----------------------------------------

	public static function getCategories($school, $pagination = false)
	{
		$id_school = School::getId($school);
		if(!$school){
			return false;
		}
		if($pagination){
			$categories = Category::where('id_school', $id_school)->orderBy('position', 'asc')->paginate(self::$itemsPerPage);
		}else {
			$categories = Category::where('id_school', $id_school)->orderBy('position', 'asc')->get();
		}
		return $categories;
		
	}

	public static function getFirstCategory($school)
	{
		$id_school = School::getId($school);
		if(!$school){
			return false;
		}
		return DB::table('categories')->where('id_school', $id_school)->orderBy('position', 'asc')->first();
	}

	public static function get($id){
		$category = DB::table('categories')->where('id', $id)->first();
		if($category){
			return $category;
		}
		return false;
	}

	public static function getMaxPosition($school)
	{
		$id_school = School::getId($school);
		$max = DB::table('categories')->where('id_school', $id_school)->max('position');
		return $max;
	}

	public static function getMinPosition($school)
	{
		$id_school = School::getId($school);
		$min = DB::table('categories')->where('id_school', $id_school)->min('position');
		return $min;
	}

	public static function getRecordsNumber()
	{
		return array(
			'teachers'	=> DB::table('categories')->where('id_school', 1)->count(),
			'adults'	=> DB::table('categories')->where('id_school', 2)->count()
		);
	}
}