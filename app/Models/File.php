<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
* Status model
*/
class File extends Model
{

	// Table name in database
	protected $table = 'files';

    public $timestamps = true;

    public $fillable = ['description', 'pages_id', 'filepath'];

    public static $rules = [
        'description' => 'required|max:255',
        'pages_id' => 'required|exists:pages,id',
        'file' => 'required|max:5120'
    ];

}