<?php

/**
* Validator class
*/
class MyValidator
{
	//-----------------------------------------
	// VARIABLES
	//-----------------------------------------

	public static $messages = array(
		'file' => array(
			'error'			=> 'Błąd podczas przesyłania pliku',
			'success'		=> 'Plik został przesłany pomyślnie',
			'dimensions'	=> 'Plik ma niepoprawne wymiary'
		),
	);

	//-----------------------------------------
	// METHODS
	//-----------------------------------------
	
	public static function printMessage($errors, $name){
		if($errors->has($name)){
			$label = '<span class="label label-danger">'.$errors->first($name, ':message').'</span>';
			return $label;
		}	
	}
}