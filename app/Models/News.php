<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class News extends Model
{
    //-----------------------------------------
    // Variables
    //-----------------------------------------

    // Table name in database
    protected $table = 'news';

    // Number of items displayed per page
    const DEFAULT_PER_PAGE = 4;

    // Rules for validation
    public static $rules = array(
        'content'	=> 'required',
    );



    //-----------------------------------------
    // Methods
    //-----------------------------------------


    public static function getFromSchool($school, $perPage = self::DEFAULT_PER_PAGE)
    {
        $id_school = School::getId($school);
        $news = DB::table('news')->where('id_school', $id_school)->orderBy('updated_at', 'desc')->paginate($perPage);
        $news->each(function($item) {
            $item->printableDate = self::printDate($item->updated_at);
        });
        return $news;
    }

    public static function getOne($id)
    {
        $news = DB::table('news')->where('id', $id)->first();
        return $news;
    }

    public static function getNewsOffer(array $data){
        $category = Category::get($data['id_category']);
        $status = Status::get($data['id_status']);
        $news['content'] = '<p>Przedstawiamy Państwu nową ofertę: <strong>'.$data['name'].'</strong> w kategorii '.$category->name.'</p><p>Obecny status oferty to: '.$status->name.'</p>';

        return $news;
    }

    public static function add($school, $content)
    {
        $id_school = School::getId($school);
        $inputs = array(
            'content'		=> $content,
            'id_school'		=> $id_school,
            'created_at'	=> date('Y-m-d H:m:i'),
            'updated_at'	=> date('Y-m-d H:m:i')
        );

        DB::table('news')->insert($inputs);
    }

    public static function getRecordsNumber(){
        return array(
            'teachers'	=> DB::table('news')->where('id_school', 1)->count(),
            'adults'	=> DB::table('news')->where('id_school', 2)->count()
        );
    }

    public static function printDate($date)
    {
        $months = array("STY", "LUT", "MAR", "KWI", "MAJ", "CZE", "LIP", "SIE", "WRZ", "PAŹ", "LIS", "GRU");
        $date = strtotime($date);
        $printDate = array(
            'day'	=> date('d', $date),
            'month'	=> $months[date('n', $date)-1],
            'year'	=> date('Y', $date)
        );

        return $printDate;
    }
}
