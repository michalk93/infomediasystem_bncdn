<?php

namespace App\Models;

use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
* Newsletter engine
*/
class Newsletter extends Model
{
	protected $table = 'newsletter';

    protected $fillable = ['email', 'id_school', 'created_at'];
    public  $timestamps = false;

	public static function add($email, $id_school)
	{
		$emails = DB::table('newsletter')->select('email')->get();
		foreach($emails as $mail){
			if($mail->email == $email){
				return false;
			}
		}

		DB::table('newsletter')->insert(array(
			'email' 		=> $email,
			'id_school'		=> $id_school,
			'created_at'	=> date('Y-m-d')
		));
	}

	public static function sendAllNewOffer($school, $data)
	{
		$id_school = School::getId($school);
		$emails = DB::table('newsletter')->where('id_school', $id_school)->get();
		foreach($emails as $email){
			$data['id_email'] = $email->id;
			Mail::queue('emails.newsletter.offer', $data, function($message) use ($email) {
				$message->to($email->email)->subject('Nowa oferta bncdn.pl');
			});
		}
	}

	/**
	* Send news to all newsletter's addresses
	**/
	public static function sendNews($school, $data)
	{

        Mailgun::send('emails.newsletter.news', $data, function($message) {
            $message->to(config('mail.newsletter-mailing-list'));
            $message->subject("Aktulaności - Newsletter BNCDN");
        });



	}

	public static function getRecordsNumber()
	{
		return array(
			'teachers'	=> DB::table('newsletter')->where('id_school', 1)->count(),
			'adults'	=> DB::table('newsletter')->where('id_school', 2)->count()
		);
	}
}