<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


/**
* Offer model
*/
class Offer extends Model
{
	//---------------------------------------
	// Variables
	//---------------------------------------

	// How many offers display per page
	const OFFER_PER_PAGE = 8;

	// Messages for offerts
	public static $messages = array(
		'save_success'	=> '<div class="alert alert-success">Oferta została zapisana pomyślnie.</div>',
		'file_error'	=> '<div class="alert alert-error">Błąd podczas przesyłania pliku na serwer. Skontaktuj się z administratorem</div>',
		'signup_success' => '<div class="alert alert-success text-center"><p>Zgłoszenie zostało złożone pomyślnie.</p><p>Na adres e-mail podany w formularzu zostanie wysłane, w ciągu 24 godzin, potwierdzenie oraz dalsze instrukcje.</p></div>',
	);

	// Rules for offer form
	public static $rules = array(
		'name'			=> 'required|between:5,128',
		'id_category'	=> 'required|numeric|min:1',
		'id_status'		=> 'required|numeric|min:1',
		'hours'			=> 'required|max:30',
		'price'			=> 'required|max:30',
		'file_desc'		=> 'max:100',
		'date_at'		=> 'date|date_format:Y-m-d'
	);


	protected $fillable = ['name','id_category', 'id_status', 'hours', 'price', 'recipient', 'range', 'syllabus', 'qualifications', 'additional', 'filename', 'file_desc', 'created_at', 'position', 'id_school', 'date_at'];

	public $timestamps = false;


	//---------------------------------------
	// Methods
	//---------------------------------------

	public static function getOneOffer($school, $id)
	{
		return DB::table('offer_'.$school)
					->where('offer_'.$school.'.id', $id)
					->join('status', 'offer_'.$school.'.id_status', '=', 'status.id')
					->join('categories', 'offer_'.$school.'.id_category', '=', 'categories.id')
					->select('offer_'.$school.'.*', 'status.name AS status_name', 'categories.name AS category_name')
					->first();
	}

	public static function getOffer($school, $number = self::OFFER_PER_PAGE)
	{
		$offer = DB::table('offer_'.$school)
					->orderBy('id', 'desc')
					->join('status', 'offer_'.$school.'.id_status', '=', 'status.id')
					->join('categories', 'offer_'.$school.'.id_category', '=', 'categories.id')
					->select('offer_'.$school.'.*', 'status.name AS status_name', 'categories.name AS category_name')
					->paginate($number);

		return $offer;
	}

	public static function getAllOffer($school){
		$offer = DB::table('offer_'.$school)
					->orderBy('id', 'desc')
					->join('status', 'offer_'.$school.'.id_status', '=', 'status.id')
					->join('categories', 'offer_'.$school.'.id_category', '=', 'categories.id')
					->select('offer_'.$school.'.*', 'status.name AS status_name', 'categories.name AS category_name')
					->get();

		return $offer;
	}

	public static function getAllOfferFromCategory($school, $id_category){
		$offer = DB::table('offer_'.$school)
					->where('id_category', $id_category)
					->orderBy('position', 'ASC')
					->join('status', 'offer_'.$school.'.id_status', '=', 'status.id')
					->select('offer_'.$school.'.*', 'status.name AS status_name')
					->paginate(self::OFFER_PER_PAGE);
		return $offer->isEmpty() ? null : $offer;
	}

	public static function getLast($school, $number, $id_category = null)
	{
		$lastOffers = null;
		if(is_null($id_category)){
			$lastOffers = DB::table('offer_'.$school)->take($number)->orderBy('created_at', 'desc')->orderBy('id', 'desc')->get();
		}else{
			$lastOffers = DB::table('offer_'.$school)->where('id_category', $id_category)->take($number)->orderBy('created_at', 'desc')->orderBy('id', 'desc')->get();
		}

		return $lastOffers;
	}

	public static function getRecordsNumber(){
		return array(
			'teachers'	=> DB::table('offer_teachers')->count(),
			'adults'	=> DB::table('offer_adults')->count(),
		);
	}

	public function category()
	{
		return $this->hasOne('App\Models\Category', 'id', 'id_category');
	}

	public function status()
	{
		return $this->hasOne('App\Models\Status', 'id', 'id_status');
	}

//	public function school(){
//		return $this->hasOne('App\Models\School', 'id', 'id_school');
//	}
}