<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
* Status model
*/
class Page extends Model
{

	// Table name in database
	protected $table = 'pages';

    public $timestamps = true;

    public $fillable = ['slug', 'title', 'content'];

    public static $rules = [
        'title' => 'required|max:255',
        'content' => 'required'
    ];


    public function files()
    {
        return $this->hasMany('App\Models\File', 'pages_id');
    }


}