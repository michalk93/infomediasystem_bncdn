<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
* School model
*/
class School extends Model
{
	protected $table = 'schools';

	public static function getId($school)
	{
		$school = DB::table('schools')->where('name', $school)->select('id')->first();
		if(is_object($school))
			return $school->id;
		else return false;
	}

	public static function getName($id)
	{
		$school = self::find($id);
		if(is_object($school))
			return $school->name;
		else return false;
	}
}