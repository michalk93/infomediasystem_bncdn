<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

/**
* Slider model
*/
class Slider extends Model
{

	//----------------------------------
	// CONSTANS
	//----------------------------------
	const SLIDE_WIDTH	= 1000;
	const SLIDE_HEIGHT	= 350;

	//----------------------------------
	// VARIABLES
	//----------------------------------

	// Table name in database
	protected $table = 'sliders';

	// Rules for validation
	public static $rules = array(
		'file'	=> 'required|image|mimes:jpg,jpeg',
		'link'	=> 'max:128|url'
	);

	//Rules for edit validation
	public static $editRules = array(
		'file'	=> 'image|mimes:jpg,jpeg',
		'link'	=> 'max:128|url'
	);

	//Path to folder for uploaded images of slider
	public static $uploadPath = 'img/slider/';


	//----------------------------------
	// METHODS
	//----------------------------------

	/*
	* Get all alides from $school
	* @school Name of school
	*/
	public static function getSlides($school)
	{
		$id_school = School::getId($school);
		$slides = DB::table('sliders')->where('id_school', $id_school)->get();
		return $slides;
	}

	/*
	* Get one slide with $id
	* @id 		Id of slide in database
	*/
	public static function getSlide($id)
	{
		$slide = DB::table('sliders')->where('id', $id)->first();
		return $slide;
	}

	public static function deleteImage($school, $id){
		$slide = self::getSlide($id);
		File::delete(self::$uploadPath.$school.'/'.$slide->filename);
	}

	public static function getRecordsNumber(){
		return array(
			'teachers'	=> DB::table('sliders')->where('id_school', 1)->count(),
			'adults'	=> DB::table('sliders')->where('id_school', 2)->count()
		);
	}
}