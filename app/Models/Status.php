<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
* Status model
*/
class Status extends Model
{
	//-----------------------------------------
	// Variables
	//-----------------------------------------

	// Table name in database
	protected $table = 'status';

	// Rules for status input
	public static $rules = array(
		'name'	=> 'required|between:5,60|unique:status,name'
	);

	protected $fillable = ['name'];

	public $timestamps = false;


	//-----------------------------------------
	// Methods
	//-----------------------------------------


	public static function getAll()
	{
		return DB::table('status')->get();
	}

	public static function get($id)
	{
		$status = DB::table('status')->where('id', $id)->first();
		return $status;
	}

	public static function getRecordsNumber()
	{
		return array(
			'all'	=> DB::table('status')->count(),
		);
	}
	
}