<?php
namespace App\Models;
use Illuminate\Support\Facades\DB;


/**
* Application of offer model
*/
class TeachersApplicationOffer extends ApplicationOffer
{
	// Validation rules
	public static $rules = array(
		'lastname' => 'required|alpha_pl',
		'firstname' => 'required|alpha_pl',
		'secondname' => 'alpha_pl',
		'birthdate' => 'required|date|before:1995-01-01',
		'birthplace' => 'required|alpha_pl',
		'address' => 'required',
		'email' => 'required|email',
		'phonenumber' => 'required',
		'school' => 'required',
		'specialization' => 'required',
		'workplace' => 'required',
		'position' => 'required|alpha_pl',
		'subject' => 'required|alpha_pl',
		'degree' => 'required|alpha_pl',
		'seniority' => 'required',
		'agreement' => 'accepted',
		'g-recaptcha-response' => 'required|recaptcha'
	);


	// Path to folder where apllications were stored
	public static $pathApplicationFiles = '/reports/teachers/';

}