<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
* 
*/
class Timetable extends Model
{
	protected $table = 'timetable';

	public static $storePath = '/timetables/';

	public static $rules = array(
		'description'	=> 'required|max:255',
		'file'			=> 'required'
	);

	public static $rulesUpdate = array(
		'description'	=> 'required|max:255',
	);


	public function school()
	{
		return $this->hasOne('App\Models\School', 'id', 'id_school');
	}

	public function category()
	{
		return $this->hasOne('App\Models\Category', 'id', 'id_category');
	}
}