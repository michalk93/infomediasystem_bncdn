<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('alpha_pl', function($attribute, $value, $parameters, $validator)
        {
            return preg_match("/^[A-Za-zĄąĆćĘęŁłŃńÓóŚśŹźŻż\s]+$/", $value);
        });

        Validator::extend('one_word', function($attribute, $value, $parameters, $validator)
        {
            return preg_match("/^[^\s]+$/", $value);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
