<?php

namespace App\Providers;

use App\Helpers\ViewHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        View::share('school', $request->segment(1));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
