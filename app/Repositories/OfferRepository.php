<?php
namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Container\Container as Application;

class OfferRepository extends BaseRepository {
    /**
     * OfferRepository constructor.
     */
    public function __construct(Application $app, Request $request)
    {
//        dd($request);
        $this->school = $request->attributes->get('school');

        parent::__construct($app);
    }


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
//        dd($this->school);
        $className = 'App\\Models\\'.ucfirst($this->school->name).'Offer';
        if(class_exists($className)){
            return $className;
        }

        return 'App\\Models\\Offer';
    }
}