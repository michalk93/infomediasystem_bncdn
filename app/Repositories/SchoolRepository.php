<?php
/**
 * Created by PhpStorm.
 * User: mkolbusz
 * Date: 7/31/16
 * Time: 6:25 PM
 */

namespace app\Repositories;


use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

class SchoolRepository extends BaseRepository
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\\Models\\School';
    }
}