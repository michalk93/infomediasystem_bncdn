<?php
/**
 * Created by PhpStorm.
 * User: mkolbusz
 * Date: 7/31/16
 * Time: 6:19 PM
 */

namespace app\Repositories;


use Prettus\Repository\Eloquent\BaseRepository;

class TimetableRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\\Models\\Timetable';
    }
}