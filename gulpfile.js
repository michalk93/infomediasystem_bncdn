'use strict';
require('es6-promise').polyfill();

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');

var sassConfig = {outpuStyle: 'compressed'};

var sassDir = './resources/assets/sass/';

gulp.task('serve',['sass'], function() {
    browserSync.init({
        proxy: "localhost/infomediasystem/bncdn/public"
    });

    gulp.watch(sassDir + '**/*.scss', ['sass']);
    gulp.watch("./resource/views/**/*.blade.php").on('change', browserSync.reload);
});

gulp.task('sass', function(){
    return gulp.src(sassDir + '**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass(sassConfig).on('error', sass.logError))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('maps'))

        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.stream());
});

gulp.task('bootstrap-sass', function(){
    return gulp.src(sassDir + 'admin/main.scss')
        .pipe(sass(sassConfig).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))

        .pipe(gulp.dest('./public/css/admin'));
})

gulp.task('sass:watch', function (){
    gulp.watch(sassDir + '**/*.scss', ['sass']);
});

gulp.task('default', ['serve'])