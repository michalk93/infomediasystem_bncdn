// bncdn.js

$(document).ready(function($) {
	//-----------------------------------------------------
	// HOME PAGE SCRIPTS
	//-----------------------------------------------------

	// Slider on home page
	slidesNumber = $('#homeCarousel .carousel-inner .item').length;
	randomSlide = Math.floor(Math.random()*slidesNumber);
	$('#homeCarousel .carousel-inner .item').eq(randomSlide).addClass('active');
	$('#homeCarousel .carousel-indicators li').eq(randomSlide).addClass('active');
	$('.carousel').carousel({
		interval: 3500
	});

	// Social icons animation
	$('div#social-icons a.social-link').hover(
		function(){
			$(this).stop(true, false).animate({'margin-left': '0px'}, 'slow').children('img').stop(true. false).animate({'margin-top': '-64px'}, 'slow');
		}, 
		function(){
			$(this).animate({'margin-left': '-150px'}, 'slow').children('img').stop(true, false).animate({'margin-top': '0px'}, 'slow');;
		}
	);
	// News ajax loading
	$('#newsDetailModal').on('show.bs.modal', function(e){
		var url = $(e.relatedTarget).data('href');
		$('#newsDetailModal .modal-body .ajax-content').html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');

		$.get(url, function(news) {
			news = JSON.parse(news);
			var months = ["STY", "LUT", "MAR", "KWI", "MAJ", "CZE", "LIP", "SIE", "WRZ", "PAŹ", "LIS", "GRU"];
			var date = new Date(news.updated_at);

			$('#newsDetailModal .modal-body .news-content').html(news.content);
			$('#newsDetailModal .modal-body .news-date .date-day').text((date.getDate() < 10 ? "0" : "") + date.getDate());
			$('#newsDetailModal .modal-body .news-date .date-month').text(months[date.getMonth()]);
		});
	});
	$('#newsDetailModal').on('click', 'button', function(){
		$('#newsDetailModal').modal('hide');
	});
	
	//-----------------------------------------------------
	// END
	//-----------------------------------------------------


});