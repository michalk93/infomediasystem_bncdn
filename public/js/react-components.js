var NewsletterComponent = React.createClass({

    getInitialState: function(){
        return {email: '', result: ''};
    },

    handleEmailChange: function(e){
        this.setState({email: e.target.value});
    },

    handleSubmit: function(e){
        e.preventDefault();
        $.post('/teachers/newsletter', {
            email: this.state.email
        }, function(data, status, xhr){
            console.log(data);
            data.email = '';
            this.setState(data);
        }.bind(this));
    },

    render: function(){

        return (<div>
            <h4 className="text-center">Newsletter</h4>
            <form onSubmit={this.handleSubmit}>
                <p>{this.state.result}</p>
                <input type="text" name="email" placeholder="kowalski@gmail.com" value={this.state.email} onChange={this.handleEmailChange} />
                <button type="submit" className="btn btn-default btn-sm">Zapisz się</button>
            </form>
            </div>
        );
    }
});


var NewsBoxComponent= React.createClass({
    getInitialState: function(){
        return {data: []};
    },

    componentDidMount: function(){
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data){
                this.setState({data: data.data});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },

    render: function(){
        return (
            <div id="box-news">
                <NewsListComponent data={this.state.data} />
            </div>
        );
    }
});

var NewsListComponent = React.createClass({
    render: function(){
        var newsNodes = this.props.data.map(function(news){
            return (
                <NewsComponent key={news.id} date={news.updated_at}>{news.content}</NewsComponent>
            );
        })
        return (
            <div className="commentList">
                {newsNodes}
            </div>
        );
    }
});


var NewsComponent = React.createClass({
    render: function(){
        return (
            <article className="news-item clearfix">
                <div className="pull-left news-date">{this.props.date}<br/>{this.props.date}</div>
                <section>{this.props.children}</section>

                <footer className="text-right">
                    <a href="teachers/news/{this.props.news.id}" className="btn btn-default more" title="Kliknij, aby przeczytać więcej">czytaj więcej</a>
                </footer>
            </article>
        );
    }
})




ReactDOM.render(<NewsletterComponent/>, document.getElementById('newsletter'));
//ReactDOM.render(<NewsBoxComponent url="/api/teachers/news"/>, document.getElementById('news-box'));