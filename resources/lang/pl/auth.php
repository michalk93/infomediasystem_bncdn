<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Podane dane logowania nie są prawidłowe.',
    'throttle' => 'Zbyt wiele prób logowania. Spróbuj za :seconds sekund.',
    'logout' => 'Zostałeś pomyślnie wylogowany.'

];
