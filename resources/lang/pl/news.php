<?php
return [
    'create' => [
        'success' => 'News został dodany.'
    ],
    'edit' => [
        'success' => 'News został zapisany.'
    ],
    'delete' => [
        'success' => 'News został usunięty.',
        'success-group' => 'Aktualności zostały usunięte pomyślnie',
        'error-group' => 'Wystąpił błąd podczas usuwania. Spróbuj ponownie lub skontaktuj się z administratorem.'
    ]
];