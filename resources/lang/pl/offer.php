<?php

return [
    'create' => [
        'success' => 'Nowa oferta została dodana.',
        'error-file' => 'Błąd podczas przesyłania pliku na serwer. Skontaktuj się z administratorem',
    ],
    'delete' => [
        'success' => 'Oferta została usunięta.'
    ],
    'edit' => [
        'success' => 'Oferta zostałą zmieniona.',
        'error-file' => 'Błąd podczas przesyłania pliku na serwer. Skontaktuj się z administratorem.',
    ],
    'move-up' => [
        'success' => 'Oferta została przesunięta w górę o jeden stopień.',
        'failed' => 'Nie można przesunąć oferty w górę. Może być to spowodowane tym, iż oferta jest najwyżej położona w swojej kategorii.'
    ],
    'move-down' => [
        'success' => 'Oferta została przesunięta w dół o jeden stopień.',
        'failed' => 'Nie można przesunąć oferty w dół. Może być to spowodowane tym, iż oferta jest najniżej położona w swojej kategorii.'
    ],

    'signup' => [
        'success' => '<p>Zgłoszenie zostało złożone pomyślnie.</p><p>Na adres e-mail podany w formularzu zostanie wysłane, w ciągu 24 godzin, potwierdzenie oraz dalsze instrukcje.</p>'
    ],

    'application' => [
        'confirmation' => [
            'success' => 'Zgłoszenie zostało potwierdzone'
        ],
        'delete' => [
            'success' => 'Zgłoszenie zostało usunięte pomyślnie',
            'error' => 'Błąd podczas usuwania zgłoszenia. Skontaktuj się z administratorem'
        ]

    ]
];