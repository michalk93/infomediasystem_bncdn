<?php
return [
    'create' => [
        'success' => 'Nowy slajd został dodany.'
    ],
    'edit' => [
        'success' => 'Slajd został zapisany.'
    ],
    'delete' => [
        'success' => 'Slajd został usuniety'
    ]
];