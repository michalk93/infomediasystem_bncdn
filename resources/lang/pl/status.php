<?php

return [
    'add' => [
        'success' => 'Status został dodany.',
        'failed' => 'Wystapił błąd. Spróbuj ponownie lub skontaktuj się z administratorem.'
    ],
    'edit' => [
        'success' => 'Status został zmieniony.'
    ],
    'delete' => [
        'success' => 'Status został usunięty.'
    ]
];