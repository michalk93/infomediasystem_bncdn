<?php
return [
    'create' => [
        'success' => 'Nowy harmonogram został dodany.',
    ],
    'delete' => [
        'success' => 'Harmonogram został usunięty.',
        'error-file' => 'Błąd podczas usuwania pliku. Skontaktuj się z administratorem.'
    ],
    'edit' => [
        'success' => 'Harmonogram zaktualizowany pomyślnie.',
        'error-file-delete' => 'Błąd podczas usuwania poprzedniego harmonogramu. Skontaktuj się z administratorem',
        'error-file-upload' => 'Błąd podczas przesyłania pliku. Spróbuj ponownie lub skontaktuj się z administratorem.'
    ]
];