@extends('admin.default')

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="row">
	<div class="col-sm-12">
		{!! Form::open(['route' => ['admin-editcategory-post', $school, $category->id]]) !!}
		<div class="well">
			<label>Edytuj kategorię</label>
			<div class="input-prepend input-append">
				<span class="add-on"><i class="icon-edit"></i></span>
				<input type="text" name="name" value="{{ $category->name }}" class="form-control" required>
				<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-save"></span> Aktualizuj status</button>
				{{ MyValidator::printMessage($errors, 'name') }}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop