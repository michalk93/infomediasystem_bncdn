@extends('admin.default')

@section('admin-content')
@include('admin.parts.messages')
<div class="row">
  <div class="col-sm-12">
    <div class="well">
      {!! Form::open(['route' => ['admin-categories', $school->name]]) !!}
      <label>Dodaj nową kategorię {{ MyValidator::printMessage($errors, 'name') }}</label>
      <div class="input-prepend input-append"><span class="add-on"><i class="icon-plus-sign"></i></span>
        <input type="text" name="name" placeholder="100 znaków" maxlength="100" autofocus="autofocus" tabindex="1" required>
        <button type="submit" class="btn btn-default" tabindex="2">Dodaj kategorię</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <table class="table">
      <thead>
      <tr>
        <th>ID/Poz</th>
        <th>Nazwa kategorii</th>
        <th>Liczba ofert</th>
        <th style="width: 36px;">Menu</th>
      </tr>
      </thead>
      <tbody>
      @if($categories)
        @foreach ($categories as $category)
          <tr>
            <td>{{$category->id}}/{{$category->position}}</td>
            <td>{{$category->name}}</td>
            <td>{{$category->num_offer}}</td>
            <td>
              @if(count($categories) > 1)
                @if($category->position > 1)
                  <a href="{{ route('admin-upcategory', array($school->name, $category->id)) }}" title="Przesuń w górę"><span class="glyphicon glyphicon-arrow-up"></span></a>
                @endif
                @if($category->position < App\Models\Category::getMaxPosition($school))
                  <a href="{{ route('admin-downcategory', array($school->name, $category->id)) }}" title="Przesuń w dół"><span class="glyphicon glyphicon-arrow-down"></span></a>
                @endif
              @endif
              <div>
                <a href="{{ route('admin-editcategory', array($school->name, $category->id)) }}"><span class="glyphicon glyphicon-pencil"></span></a>
                <a href="#removeConfirmModal{{$category->id}}" role="button" data-toggle="modal" title="Usuń kategorię"><span class="glyphicon glyphicon-trash"></span></a>
              </div>
            </td>


            <!-- Modal -->
            <div class="modal fade" id="removeConfirmModal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="removeConfirmModal{{$category->id}}Label">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="removeConfirmModal{{$category->id}}Label">Usuwanie kategorii</h4>
                  </div>
                  <div class="modal-body">
                    <p>Czy na pewno chcesz usunąć kategorię: <strong>{{ $category->name }}</strong> ?</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Nie usuwaj</button>
                    <a role="button" href="{{ route('admin-delcategory', array($school->name, $category->id)) }}" class="btn btn-default">Usuń</a>
                  </div>
                </div>
              </div>
            </div>
          </tr>
        @endforeach
      @else
        <div class="alert alert-block">
          <h4>Brak kategorii
            <a href="{{ route('admin-categories', $school) }}" class="btn btn-large pull-right">Dodaj kategorię</a>
          </h4>
          <p>Nie dodano jeszcze żadnej kategorii dla tej grupy.</p>
        </div>
      @endif

      </tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    @if($categories)
      {{ $categories->render() }}
    @endif
  </div>
</div>
@stop