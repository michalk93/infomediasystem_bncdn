<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	{{ HTML::style('css/admin/main.css') }}
	@yield('admin-css')

	<title>BNC - administracja</title>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<nav class="navbar navbar-default">
				{{--<div class="container-fluid">--}}
						<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Administracja BNC</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="{{route('admin-start')}}">Home</a></li>


						{{-- GENERAL ADMIN MENU --}}
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ogólne <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('admin-status') }}">Statusy ofert</a></li>
							</ul>
						</li>

						{{-- TEACHERS ADMIN MENU --}}
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BNC Nauczyciele <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('admin-offer', 'teachers') }}">Oferta</a></li>
								<li><a href="{{ route('admin-applications', 'teachers') }}">Zgłoszenia</a></li>
								<li><a href="{{ route('admin-news', 'teachers') }}">Aktualności</a></li>
								<li><a href="{{ route('admin-newsletter', 'teachers')}}">Newsletter</a></li>
								<li><a href="{{ route('admin-slider', 'teachers')}}">Slider</a></li>
								<li><a href="{{ route('admin-timetable', 'teachers')}}">Harmonogramy</a></li>
								<li class="divider" role="separator"></li>
								<li><a href="{{ route('admin-categories', 'teachers') }}">Kategorie</a></li>
								<li><a href="{{ route('admin-pages', 'teachers') }}">Strony</a></li>
								<li><a href="{{ route('admin-files', 'teachers') }}">Pliki</a></li>
							</ul>
						</li>

						{{-- ADULTS ADMIN MENU --}}
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BNC Dorośli <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('admin-offer', 'adults') }}">Oferta</a></li>
								<li><a href="{{ route('admin-applications', 'adults') }}">Zgłoszenia</a></li>
								<li><a href="{{ route('admin-news', 'adults') }}">Aktualności</a></li>
								<li><a href="{{ route('admin-newsletter', 'adults')}}">Newsletter</a></li>
								<li><a href="{{ route('admin-slider', 'adults')}}">Slider</a></li>
								<li class="divider"></li>
								<li><a href="{{ route('admin-categories', 'adults') }}">Kategorie</a></li>
							</ul>
						</li>
					</ul>


					<ul class="nav navbar-nav navbar-right">
						<li><a href="{{ route('home')}}" target="_blank">Podgląd</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->email }} <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="{{route('admin-change-password')}}"><i class="icon-cog"></i> Zmień hasło</a></li>
								<li class="divider"></li>
								<li><a href="{{ route('admin-logout') }}"><i class="icon-off"></i> Wyloguj</a></li>
							</ul>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
				{{--</div><!-- /.container-fluid -->--}}
			</nav>
		</div>
	</div>

	@yield('admin-content')

</div>
{{ HTML::script('js/jquery-1.10.1.min.js', array('type' => 'text/javascript'))}}
{{ HTML::script('js/bootstrap/bootstrap.min.js', array('type' => 'text/javascript'))}}
@yield('admin-js')
</body>
</html>