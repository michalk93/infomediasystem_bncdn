@extends('admin.default')

@section('admin-css')
    {{ HTML::style('css/bootstrap/bootstrap-fileupload.min.css') }}
    {{ HTML::style('css/bootstrap/bootstrap-switch.css') }}
@stop

@section('admin-js')
    {{ HTML::script('js/bootstrap/bootstrap-fileupload.min.js') }}
    {{ HTML::script('js/bootstrap/bootstrap-switch.min.js')	}}
    {{ HTML::script('js/ckeditor/ckeditor.js') }}
@stop

{{------------------}}
{{-- SECTION START--}}

@section('admin-content')
@include('admin.parts.validation-errors')
    <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['route' => ['files-add-post', $school->name], 'enctype' => 'multipart/form-data']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            {{-- File description --}}
            <div class="control-group">
                <label class="control-label">Opis pliku {!! MyValidator::printMessage($errors, 'description') !!}</label>
                <div class="controls">
                    <input id="description" name="description" type="text" placeholder="255 znaków" maxlength="255" class="form-control" value="{{ old('description') }}" required>
                    <p>Tutaj wpisujemy opis pliku który będzie wyświetlany na stronie użytkownikowi, w celu pobrania pliku.</p>

                </div>
            </div>

            {{-- Page --}}
            <div class="control-group">
                <label class="control-label">Strona powiązana {!! MyValidator::printMessage($errors, 'pages_id') !!}</label>
                <div class="controls">
                    @if($pages)
                        <select id="page-id" name="pages_id" type="text"  class="form-control">
                            @foreach($pages as $page)
                                <option value="{{$page->id}}">{!! $page->title !!}</option>
                            @endforeach
                        </select>
                        <p>Wybieramy nazwę/tytuł, z którą jest skojarzony plik i będzie wyświetlany na niej w celu pobrania.</p>
                    @else
                        <span class="alert"><span>Brak dodanych stron. </span><a class="btn btn-small" href="{{ route('admin-pages') }}">Dodaj stronę</a></span>
                    @endif

                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Plik</label>
                <div class="controls">
                    <input type="file" name="file" />
                </div>
            </div>

            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Dodaj plik</button>
        </div>
    </div>
@stop