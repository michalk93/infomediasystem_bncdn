@extends('admin.default')

@section('admin-content')
@include('admin.parts.messages')
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ action('FileController@create', $school->name) }}" class="btn btn-primary">Dodaj plik</a>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="col-sm-12">
            <p class="alert alert-info">{!! Session::get('message') !!}</p>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            @if(count($files))
                <div class="well">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nazwa pliku</th>
                            <th>Opis pliku</th>
                            <th>ID strony</th>
                            <th style="width: 36px;">Menu</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($files as $file)
                            <tr>
                                <td>{{$file->id}}</td>
                                <td>{{$file->filepath}}</td>
                                <td>{{$file->description}}</td>
                                <td>{{$file->pages_id}}</td>
                                <td>
                                    <a href="{{ route('files-delete', [$school->name, $file->id]) }}" role="button" data-toggle="modal"><span class="glyphicon glyphicon-remove" title="Usuń plik"></span></a>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-block">
                    <h4>Brak plików</h4>
                    <p>Nie dodano jeszcze żadnych plików. Powyżej możesz dodać nowy plik.</p>
                </div>
            @endif
        </div>
    </div>

@stop