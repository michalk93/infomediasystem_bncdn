<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  {{ HTML::style('css/admin/main.css') }}
  {{ HTML::script('js/jquery-1.10.1.min.js', array('type' => 'text/javascript'))}}
  {{ HTML::script('js/bootstrap/bootstrap.min.js', array('type' => 'text/javascript'))}}
  <title>BNC - Logowanie</title>
</head>
<body>
<div id="viewLogin" class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="well">
        <h1 class="text-center">BESKIDZKIE NIEPUBLICZNE CENTRUM</h1>
      </div>
    </div>
  </div>
  @include('admin.parts.messages')
  <div class="row">
    <div class="col-sm-12 col-md-6 col-md-offset-3">
      {{Form::open(['route' => 'admin-login', 'class' => 'form-horizontal'])}}

      <div class="control-group">
        <!-- Username -->
        <label class="control-label"  for="username">Adres e-mail</label>
        <div class="controls">
          <input type="email" id="email" name="email" placeholder="Wprowadź adres e-mail..." class="form-control" autofocus="autofocus" tabindex="1">
        </div>
      </div>

      <div class="control-group">
        <!-- Password-->
        <label class="control-label" for="password">Hasło</label>
        <div class="controls">
          <input type="password" id="password" name="password" placeholder="Wprowadź hasło..." class="form-control" tabindex="2">
        </div>
      </div>

      <button type="submit" class="btn btn-success" tabindex="3">Zaloguj</button>

      {{Form::close()}}
    </div>
  </div>
</div>

</body>
</html>