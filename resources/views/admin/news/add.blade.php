@extends('admin.default')

@section('admin-css')
	{{ HTML::style('css/bootstrap/bootstrap-switch.min.css') }}
@stop

@section('admin-js')
	{{ HTML::script('js/ckeditor/ckeditor.js') }}
	{{ HTML::script('js/bootstrap/bootstrap-switch.min.js')	}}

	<script type="text/javascript">
		$('input[type="checkbox"]').bootstrapSwitch();
	</script>
@stop

@section('admin-css')
	{{HTML::style('css/bootstrap/bootstrap-switch.css')}}
@stop

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="well text-center"><strong>DODAWANIE NEWS'A DO AKTUALNOŚCI</strong></div>
{{ Form::open(['route' => ['admin-addnews-post', $school->name]]) }}


{{-- News content --}}
<div class="control-group">
	<label class="control-label">Treść wiadomości {!!  MyValidator::printMessage($errors, 'content')  !!}</label>
	<div class="controls">
		<textarea id="news-content" name="content" type="text" placeholder="100 znaków" maxlength="100" class="ckeditor" tabindex="1"></textarea>

	</div>
</div>

{{-- Aktualności --}}
<div class="control-group">
	<label class="control-label">Newsletter</label>
	<div class="controls">
		<input data-on-text="TAK" data-off-text="NIE" name="newsletter" type="checkbox" checked="checked" tabindex="2">
		<span>(strona może przez jakiś czas nie odpowiadać, prosimy o cierpliwość)</span>
	</div>
	
</div>

{{-- Form button --}}
<div class="control-group">
	<div class="controls">
		<button type="submit" class="btn btn-default" tabindex="3"><span class="glyphicon glyphicon-save"></span> Zapisz</button>
	</div>
</div>

{{ Form::close() }}


@stop