@extends('admin.default')

@section('admin-js')
	{{ HTML::script('js/ckeditor/ckeditor.js') }}
@stop

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="well text-center"><strong>EDYTOWANIE ISTNIEJACEGO NEWS'A</strong></div>
{{ Form::open(array('route' => array('admin-editnews-post', $school->name, $news->id), 'class' => 'form-horizontal')) }}

{{-- News content --}}
<div class="control-group">
	<label class="control-label">Treść wiadomości</label>
	<div class="controls">
		<textarea id="news-content" name="content" type="text" placeholder="100 znaków" maxlength="100" class="input-xlarge ckeditor" tabindex="2">
			{{ $news->content }}
		</textarea>
		{{ MyValidator::printMessage($errors, 'content') }}
	</div>
</div>

{{-- Form button --}}
<div class="control-group">
	<div class="controls">
		<input value="Aktualizuj" type="submit" class="btn btn-success" tabindex="3">
	</div>
</div>

{{ Form::close() }}

<script type="text/javascript">
	CKEDITOR.replace('.ckeditor');
</script>
@stop