@extends('admin.default')

@section('admin-content')
@include('admin.parts.messages')
	<div class="row">
		<div class="col-sm-12">
			<a href="{!! route('admin-addnews', $school->name) !!}" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Nowa wiadomość</a>
			<button class="btn btn-default" onclick="$('#news-form').submit()"><span class="glyphicon glyphicon-trash"></span> Usuń zaznaczone</button>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-sm-12">
			<table class="table">
				<thead>
				<tr>
					<th><input type="checkbox" id="all_checkbox_group"></th>
					<th>ID</th>
					<th>Treść wiadomości</th>
					<th>Data utworzenia</th>
					<th>Ostatnia aktualizacja</th>
					<th style="width: 36px;">Menu</th>
				</tr>
				</thead>
				<tbody>
				@if(!$news->isEmpty())
					{!! Form::open(['route' => ['admin-delnews-group', $school->name], 'id' => 'news-form']) !!}
					@foreach ($news as $oneNews)
						<tr>
							<td><input type="checkbox" name="ids[]" class="group_checkbox" value="{{$oneNews->id}}"></td>
							<td>{{$oneNews->id}}</td>
							<td>{!! $oneNews->content !!}</td>
							<td>{{$oneNews->created_at}}</td>
							<td>{{$oneNews->updated_at}}</td>
							<td>
								<a href="{{ route('admin-editnews', array($school->name, $oneNews->id)) }}" title="Edytuj newsa"><span class="glyphicon glyphicon-pencil"></span></a>
								<a data-target="#removeConfirmModal{{$oneNews->id}}" role="button" title="Usuń newsa" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
							<!-- Modal -->
							<div id="removeConfirmModal{{$oneNews->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removeConfirmModal{{$oneNews->id}}Label">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h3 class="modal-title" id="removeConfirmModal{{$oneNews->id}}Label">Usuwanie news'a</h3>
										</div>
										<div class="modal-body">
											<p>Czy na pewno chcesz usunąć news'a z dnia {{ $oneNews->updated_at }}?</p>
										</div>
										<div class="modal-footer">
											<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Nie usuwaj</button>
											<a href="{{ route('admin-delnews', array($school->name, $oneNews->id)) }}" class="btn btn-primary">Usuń</a>
										</div>
									</div>
								</div>
							</div>
						</tr>
					@endforeach
					{!! Form::close() !!}
				@else
					<div class="alert alert-block">
						<h4>Brak aktualności
							<a href="{{ URL::route('admin-addnews', $school->name) }}" class="btn btn-large pull-right">Dodaj news'a</a>
						</h4>
						<p>Nie dodano jeszcze żadnych aktualności</p>
					</div>
				@endif
				</tbody>
			</table>
			{!! $news->render() !!}
		</div>
	</div>

@stop

@section('admin-js')
	<script type="text/javascript">
		$(function(){
			$('#all_checkbox_group').on('change', function(){
				var state = this.checked;
				$('.group_checkbox').each(function(){
					this.checked =  state;
				});
			})
		});
	</script>
@stop