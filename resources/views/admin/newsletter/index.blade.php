@extends('admin.default')

@section('admin-content')
@include('admin.parts.messages')
<div class="row">
    <div class="col-sm-12">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Adres e-mail</th>
                <th>Data</th>
                <th style="width: 36px;">Menu</th>
            </tr>
            </thead>
            {{$emails->links()}}
            <tbody>
            @if($emails)

                @foreach ($emails as $email)
                    <tr>
                        <td>{{$email->id}}</td>
                        <td>{{$email->email}}</td>
                        <td>{{$email->created_at}}</td>
                        <td>
                            <a href="#removeConfirmModal{{$email->id}}" role="button" data-toggle="modal" title="Usuń adres"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                        <!-- Modal -->
                        <div class="modal fade" id="removeConfirmModal{{$email->id}}" tabindex="-1" role="dialog" aria-labelledby="removeConfirmModal{{$email->id}}Label">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Usuwanie adresu z newsletter'a</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Czy na pewno chcesz usunąć adres: <strong>{{ $email->email }}</strong> ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Nie usuwaj</button>
                                        <a href="{{route('admin-delnewsletter', array($school->name, $email->id))}}" class="btn btn-default">Usuń</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                @endforeach
            @else
                <div class="alert alert-block">
                    <h4>Brak adresów e-mail
                    </h4>
                    <p>Nikt jeszcze nie zapisał się do newsletter'a</p>
                </div>
            @endif

            </tbody>
        </table>
        {!! $emails->render() !!}
    </div>
</div>
	
@stop