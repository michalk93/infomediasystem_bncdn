@extends('admin.default')

@section('admin-content')
<div style="padding: 10px 100px">
 <form class="form-horizontal" action="{{ route('offer-signup-post', array($school, $offer->id)) }}" method="post">
    <fieldset>
        {{-- Sign up offer form --}}
		<div class="text-center">
			<h4>Formularz zgłoszeniowy</h4>
		</div>
		<fieldset>
			<legend>Kurs</legend>
			 {{-- Lastname --}}
	        <div class="control-group">
	            <label class="control-label">Kategoria kursu</label>
	            <div class="controls">
	                <input id="course" name="lastname" type="text" placeholder="Kowalski"
	                class="input-xlarge" autofocus="autofocus" required>
	                <p class="help-block"></p>
	            </div>
	        </div>
		</fieldset>
		<fieldset>
			<legend>Dane personalne</legend>
	        {{-- Lastname --}}
	        <div class="control-group">
	            <label class="control-label">Nazwisko</label>
	            <div class="controls">
	                <input id="lastname" name="lastname" type="text" placeholder="Kowalski"
	                class="input-xlarge" autofocus="autofocus" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Firstname --}}
	        <div class="control-group">
	            <label class="control-label">Pierwsze imię</label>
	            <div class="controls">
	                <input id="firstname" name="firstname" type="text" placeholder="Jan"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Secondname --}}
	        <div class="control-group">
	            <label class="control-label">Drugie imię</label>
	            <div class="controls">
	                <input id="secondname" name="secondname" type="text" placeholder="Krzysztof"
	                class="input-xlarge">
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Date of birth --}}
	        <div class="control-group">
	            <label class="control-label">Data urodzenia</label>
	            <div class="controls" required>
	                <input id="birthdate" name="birthdate" type="date" placeholder="1996-05-12"
	                class="input-xlarge">
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Place of birth --}}
	        <div class="control-group">
	            <label class="control-label">Miejsce urodzenia</label>
	            <div class="controls">
	                <input id="birthplace" name="birthplace" type="text" placeholder="Kraków"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Address --}}
	        <div class="control-group">
	            <label class="control-label">Adres zamieszkania</label>
	            <div class="controls">
	                <input id="address" name="address" type="text" placeholder="Sucha Beskidzka ul. Kościelna 5, 34-200 Sucha Beskidzka"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- E-mail address --}}
	        <div class="control-group">
	            <label class="control-label">Adres e-mail</label>
	            <div class="controls">
	                <input id="email" name="email" type="email" placeholder="twoj@email.pl"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Phone number --}}
	        <div class="control-group">
	            <label class="control-label">Telefon kontaktowy</label>
	            <div class="controls">
	                <input id="phonenumber" name="phonenumber" type="text" placeholder="000 000 000"
	                class="input-xlarge">
	                <p class="help-block"></p>
	            </div>
	        </div>
		</fieldset>
        <fieldset>
        	<legend>Wykształcenie</legend>

	        {{-- Name of school --}}
	        <div class="control-group">
	            <label class="control-label">Uczelnia</label>
	            <div class="controls">
	                <input id="school" name="school" type="text" placeholder="Uniwersytet Jagielloński w Krakowie"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Specialization --}}
	        <div class="control-group">
	            <label class="control-label">Specjalizacja</label>
	            <div class="controls">
	                <input id="specialization" name="specialization" type="text" placeholder="Nauczanie zintegrowane"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>
        </fieldset>
        <fieldset>
        	<legend>Sytuacja zawodowa</legend>

        	{{-- Place of work --}}
	        <div class="control-group">
	            <label class="control-label">Miejsce pracy</label>
	            <div class="controls">
	                <input id="workplace" name="workplace" type="text" placeholder="Szkoła podstawowa im. Jana Nowaka w Krakowie"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Position --}}
	        <div class="control-group">
	            <label class="control-label">Stanowisko</label>
	            <div class="controls">
	                <input id="position" name="position" type="text" placeholder="Nauczyciel klas 1-3"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Teaching subject --}}
	        <div class="control-group">
	            <label class="control-label">Przedmiot nauczania</label>
	            <div class="controls">
	                <input id="subject" name="subject" type="text" placeholder="Matematyka"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Degree of advancement --}}
	        <div class="control-group">
	            <label class="control-label">Stopień awansu zawodowego</label>
	            <div class="controls">
	                <input id="degree" name="degree" type="text" placeholder=""
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	        </div>

	        {{-- Seniority --}}
	        <div class="control-group">
	            <label class="control-label">Staż pracy w oświacie</label>
	            <div class="controls">
	                <input id="seniority" name="seniority" type="text" placeholder="10 lat"
	                class="input-xlarge" required>
	                <p class="help-block"></p>
	            </div>
	       </div>
        </fieldset>
		<hr>
		{{-- Agreement to processing personal data --}}
		<div class="control-group">
			<label class="control-label">
				<input id="agreement" name="agreement" type="checkbox" class="input-xlarge" required>
			</label>
            <div class="controls" style="text-align: justify">
                <em>"Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w zgłoszeniu na potrzeby realizacji procesu rekrutacji i realizacji kursu oraz wykonywania zadań określonych prawem realizowanych przez BNCDN, zgodnie z ustawą z dnia 29.08.1997 r. o ochronie danych osobowych (Dz.U. z 1997 nr 133, poz. 883)."</em>
                <p class="help-block"></p>
            </div>
        </div>

        {{-- Agreement to receiving newsletter --}}
		<div class="control-group">
			<label class="control-label">
				<input id="newsletter" name="newsletter" type="checkbox" class="input-xlarge">
			</label>
            <div class="controls" style="text-align: justify">
                <em>Chcę otrzymywać informacje o nowych ofertach na adres e-mail podany podczas rejestracji</em>
                <p class="help-block"></p>
            </div>
        </div>
    </fieldset>
    <div class="text-center">
	    
	<div class="btn-group">
		<a href="{{ route('offer', $school) }}" class="btn">Rezygnuj</a>
		<input type="reset" value="Wyczyść formularz" class="btn">
	    <input type="submit" value="Przejdź dalej" class="btn btn-success">
	</div>
	    
	</div>
</form>
</div>
@stop