@extends('admin.default')

@section('admin-css')
	{{ HTML::style('css/bootstrap/bootstrap-switch.min.css') }}
@stop

@section('admin-js')
	{{ HTML::script('js/bootstrap/bootstrap-switch.min.js')	}}
	{{ HTML::script('js/ckeditor/ckeditor.js') }}

	<script>
		$(function () {
			$('input[type="checkbox"]').bootstrapSwitch();

			$('#myTab a').click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});

			$('input#onDateCheckbox').on('switchChange.bootstrapSwitch', function(){
				if($(this).is(':checked')){
					$('div#date_control').fadeIn();
				}else {
					$('div#date_control').fadeOut();
				}
			});

//			CKEDITOR.replace('.ckeditor');
		})
	</script>
@stop

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="row">
	<div class="col-sm-12">
		{{ Form::open(['route' => ['admin-addoffer-post', 'school' => $school], 'class' => 'form-horizontal','enctype' => 'multipart/form-data'])}}

		<ul class="nav nav-tabs" id="myTab">
			<li class="active"><a href="#general">Ogólne informacje</a></li>
			<li><a href="#recipient">Adresaci</a></li>
			<li><a href="#range">Zakres</a></li>
			<li><a href="#syllabus">Program</a></li>
			<li><a href="#qualifications">Kwalifikacje</a></li>
			<li><a href="#additional">Dodatkowe informacje</a></li>
			<li><a href="#attachments">Załącznik/Plik</a></li>
			<input type="submit" class="btn btn-success pull-right" value="Zapisz ofertę">
		</ul>

			<div class="tab-content">
				<div class="tab-pane active well" id="general">
					<fieldset>
						{{-- Name of course --}}
						<div class="control-group">
							<label class="control-label">Nazwa kursu</label>
							<div class="controls">
								<input id="name" name="name" type="text" placeholder="128 znaków" maxlength="128" class="form-control" value="{{ old('name') }}" required>
								{!! MyValidator::printMessage($errors, 'name')  !!}
							</div>

						</div>

						{{-- Kategoria kursu --}}
						<div class="control-group">
							<label class="control-label">Kategoria</label>
							<div class="controls">
								@if($categories)
									<select id="id-category" name="id_category" type="text" class="form-control">
										@foreach($categories as $category)
											<option value="{{$category->id}}">{{$category->name}}</option>
										@endforeach
									</select>
								@else
									<span class="alert"><span>Brak dodanych kategorii. </span><a class="btn btn-small" href="{{ route('admin-categories', $school) }}">Dodaj kategorię</a></span>
								@endif
								{!! MyValidator::printMessage($errors, 'id_category')  !!}
							</div>
						</div>

						{{-- Status kursu --}}
						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								@if($status)
									<select id="id-status" name="id_status" type="text"  class="form-control">
										@foreach($status as $oneStatus)
											<option value="{{$oneStatus->id}}">{{$oneStatus->name}}</option>
										@endforeach
									</select>
									<input type="checkbox" data-size="small" data-on-text="TAK" data-off-text="NIE" data-label-text="DATA" name="on_date" id="onDateCheckbox" />
								@else
									<span class="alert"><span>Brak dodanych statusów. </span><a class="btn btn-small" href="{{ route('admin-status') }}">Dodaj status</a></span>
								@endif
								{!! MyValidator::printMessage($errors, 'id_status')  !!}
							</div>
						</div>

						{{-- Data --}}
						<div class="control-group" id="date_control" style="display: none;">
							<label class="control-label">Data</label>
							<div class="controls">
								<input id="date_at" name="date_at" placeholder="RRRR-MM-DD" value="{{old('date_at')}}" type="date" class="form-control">
								{!! MyValidator::printMessage($errors, 'date_at')  !!}
							</div>
						</div>

						{{-- Liczba godzin --}}
						<div class="control-group">
							<label class="control-label">Liczba godzin</label>
							<div class="controls">
								<input id="hours" name="hours" type="text" placeholder="max 30 znaków" class="form-control" value="{{old('hours')}}" required>
								{!! MyValidator::printMessage($errors, 'hours')  !!}
							</div>
						</div>

						{{-- Cena --}}
						<div class="control-group">
							<label class="control-label">Cena</label>
							<div class="controls">
								<input id="price" name="price" type="text" placeholder="max 30 znaków" class="form-control inputmask" value="{{old('price')}}" required>
								{!! MyValidator::printMessage($errors, 'price')  !!}
							</div>
						</div>

						{{-- Aktualności --}}
						<div class="control-group">
							<label class="control-label">Aktualności</label>
							<div class="controls">
								<input id="news-allow" name="news" type="checkbox" data-size="small" data-on-text="TAK" data-off-text="NIE">
								<span>Dodaj do aktualności informację o nowym kursie</span>
							</div>
						</div>

						{{-- Newsletter --}}
						<div class="control-group">
							<label class="control-label">Newsletter</label>
							<div class="controls">
								<input data-size="small" data-on-text="TAK" data-off-text="NIE" id="newsletter-allow" name="newsletter" type="checkbox" checked="checked">
								<span>Wyślji newsletter z informacją o nowym kursie</span>
							</div>
						</div>

					</fieldset>
				</div>
				<div class="tab-pane well" id="recipient">
					<div class="control-group">
						<label class="control-label">Adresaci oferty</label>
						<div class="controls">
							<textarea name="recipient" class="ckeditor">{{old('recipient')}}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane well" id="range">
					<div class="control-group">
						<label class="control-label">Zakres oferty</label>
						<div class="controls">
							<textarea name="range" class="ckeditor">{{old('range')}}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane well" id="syllabus">
					<div class="control-group">
						<label class="control-label">Program oferty</label>
						<div class="controls">
							<textarea name="syllabus" class="ckeditor">{{old('syllabus')}}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane well" id="qualifications">
					<div class="control-group">
						<label class="control-label">Kwalifikacje</label>
						<div class="controls">
							<textarea name="qualifications" class="ckeditor">{{old('qualifications')}}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane well" id="additional">
					<div class="control-group">
						<label class="control-label">Dodatkowe informacje</label>
						<div class="controls">
							<textarea name="additional" class="ckeditor">{{old('additional')}}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane well" id="attachments">
					<div class="control-group">
						<label class="control-label">Plik</label>
						<div class="controls">
							<input type="file" name="offerfile" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Krótki opis pliku</label>
						<div class="controls">
							<input id="file-desc" name="file_desc" type="text" placeholder="100 znaków" maxlength="100" class="form-control">
							{!! MyValidator::printMessage($errors, 'file_desc')  !!}
						</div>
					</div>
				</div>
			</div>
		{{ Form::close() }}
	</div>
</div>


@stop