@extends('admin.default')

@section('admin-content')
@include('admin.parts.messages')
<div class="well">
    {{ $applications->links() }}
    <table class="table">
      <thead>
        <form>
          <input type="hidden" name="set_new_filter" value="true">
          <tr>
            <td></td>
            <td></td>
            <td><input class="form-control" type="text" name="lastname_filter" value="{{Session::get('lastname_filter')}}" placeholder="Wpisz nazwisko lub jego fragment" ></td>
            <td><input class="form-control" type="text" name="course_name_filter" value="{{Session::get('course_name_filter')}}" placeholder="Wpisz nazwę kursu lub jej fragment" ></td>
            <td><input class="form-control" type="text" name="email_filter" value="{{Session::get('email_filter')}}" placeholder="Wpisz e-mail lub jego fragment" ></td>
            <td></td>
            <td></td>
            <td></td>
              <td style="width: 36px;">
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-filter"></span> Filtruj</button>
              </td>
          </tr>
      </form>
        <tr>
          <th>ID</th>
          <th>Imię</th>
          <th>Nazwisko</th>
          <th>Nazwa kursu</th>
          <th>Adres e-mail</th>
          <th style="min-width: 100px">Data</th>
          <th>Zgłoszenie</th>
          <th style="width: 36px;">Status</th>
          <th style="width: 36px;">Menu</th>
        </tr>
      </thead>
      <tbody>
      	@if($applications)
	        @foreach ($applications as $application)
            <tr>
              <td>{{$application->id}}</td>
              <td>{{$application->firstname}}</td>
              <td>{{$application->lastname}}</td>
              <td>{{$application->course_name}}</td>
              <td>{{$application->email}}</td>
              <td>{{$application->created_at}}</td>
              <td><a href="{{ asset('reports/'.$school->name.'/'.$application->filename)}}" target="_blank">Pobierz</a></td>
              <td>
              @if($application->status)
                Zatwierdzono
              @else
                <a href="{{ route('admin-application-confirm', array($school->name, $application->id)) }}" title="Zatwierdź" class="btn">Zatwierdź</a>
              @endif
              </td>
              <td>
                <div class="btn-group">
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                  Akcja
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a data-target="#removeApplicationConfirm{{$application->id}}" data-toggle="modal" role="button">Usuń</a>
                  </li>
                </ul>
              </div>
              </td>
            </tr>

            <div class="modal fade" id="removeApplicationConfirm{{$application->id}}" tabindex="-1" role="dialog" aria-labelledby="removeApplicationConfirm{{$application->id}}Label">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Potwierdzenie usunięcia</h4>
                  </div>
                  <div class="modal-body">
                    <p>Usunięcie zgłoszenia spowoduje wymazanie wpisu w bazie danych oraz skasowanie pliku PDF z serwera. Zgłoszenia nie będzie można już przywrócić..</p>
                    <p>Czy jesteś pewny, że chcesz usunąć zgłoszenie na kurs {{$application->course_name}} użytkownika {{$application->firstname}} {{$application->lastname}} z dnia {{$application->created_at}}?</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Nie usuwaj</button>
                    <a href="{{ route('admin-application-delete', array($school->name, $application->id)) }}" class="btn btn-primary">Usuń</a>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
    		@else
          <div class="alert alert-block">
            <h4>Brak zgłoszeń
            </h4>
            <p>Nikt jeszcze nie zgłosił się na żaden kurs.</p>
          </div>
    		@endif
       
      </tbody>
    </table>
    {{ $applications->links() }}
</div>

@stop

@section('admin-js')
  <script>
  $(document).ready(function(){
    $('a.remove-link').click('function')
  })
  </script>
@stop