@extends('admin.offer.add')

@section('admin-css')
	{{--{{HTML::style('css/bootstrap/bootstrap-fileupload.min.css')}}--}}
	{{HTML::style('css/bootstrap/bootstrap-switch.min.css')}}
@stop

@section('admin-js')
	{{--{{HTML::script('js/bootstrap/bootstrap-fileupload.min.js')}}--}}
	{{HTML::script('js/bootstrap/bootstrap-switch.min.js')}}
	{{ HTML::script('js/ckeditor/ckeditor.js') }}

	<script>
		$(function () {
			$('#onDateCheckbox').bootstrapSwitch();

			$('#myTab a').click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});

			$('input#onDateCheckbox').on('switchChange.bootstrapSwitch', function(){
				if($(this).is(':checked')){
					$('div#date_control').fadeIn();
				}else {
					$('div#date_control').fadeOut();
				}
			});

			CKEDITOR.replace('.ckeditor');
		})
	</script>
@stop

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="row">
	<div class="col-sm-12">
		{{ Form::open([ 'route' => ['admin-editoffer-post', 'school' => $school, 'id' => $offer->id],
	'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}

		<ul class="nav nav-tabs" id="myTab">
			<li class="active"><a href="#general">Ogólne informacje</a></li>
			<li><a href="#recipient">Adresaci</a></li>
			<li><a href="#range">Zakres</a></li>
			<li><a href="#syllabus">Program</a></li>
			<li><a href="#qualifications">Kwalifikacje</a></li>
			<li><a href="#additional">Dodatkowe informacje</a></li>
			<li><a href="#attachments">Załącznik/Plik</a></li>
			<button class="btn btn-primary pull-right" type="submit"><span class="glyphicon glyphicon-save"></span> Zapisz ofertę</button>
		</ul>

		<div class="row">
			<div class="tab-content col-sm-12">
				<div class="tab-pane active" id="general">
					<fieldset>
						{{-- Name of course --}}
						<div class="control-group">
							<label class="control-label">Nazwa kursu</label>
							<div class="controls">
								<input id="name" name="name" type="text" placeholder="128 znaków" maxlength="128" value="{{ $offer->name }}" class="form-control" required>
								{{ MyValidator::printMessage($errors, 'name') }}
							</div>
						</div>

						{{-- Kategoria kursu --}}
						<div class="control-group">
							<label class="control-label">Kategoria</label>
							<div class="controls">
								<select id="id-category" name="id_category" type="text" placeholder="" class="form-control">
									@foreach ($categories as $category)
										@if ($category->id === $offer->category->id)
											<option value="{{$category->id}}" selected>{{$category->name}}</option>
										@else
											<option value="{{$category->id}}">{{$category->name}}</option>
										@endif
									@endforeach
								</select>
								{{ MyValidator::printMessage($errors, 'id_category') }}
							</div>
						</div>

						{{-- Status kursu --}}
						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<select id="id-status" name="id_status" type="text" placeholder="" class="form-control">
									@foreach ($status as $oneStatus)
										@if ($oneStatus->id === $offer->id_status)
											<option value="{{$oneStatus->id}}" selected>{{$oneStatus->name}}</option>
										@else
											<option value="{{$oneStatus->id}}">{{$oneStatus->name}}</option>
										@endif
									@endforeach
								</select>
								{{ MyValidator::printMessage($errors, 'id_status') }}
								@if($offer->date_at)
									<div class="make-switch" data-on-label="TAK" data-off-label="NIE" data-text-label="DATA">
										<input type="checkbox" name="on_date" id="onDateCheckbox" checked="checked">
									</div>
								@else
										<input type="checkbox" name="on_date" id="onDateCheckbox" data-size="mini" data-on-text="TAK" data-off-text="NIE" data-label-text="DATA">
								@endif
							</div>
						</div>

						{{-- Data --}}
						@if($offer->date_at)
							<div class="control-group" id="date_control">
								<label class="control-label">Data</label>
								<div class="controls">
									<input id="date_at" name="date_at" placeholder="RRRR-MM-DD" value="{{$offer->date_at}}" type="date" class="form-control">
									{{ MyValidator::printMessage($errors, 'date_at') }}
								</div>
							</div>
						@else
							<div class="control-group" id="date_control" style="display: none;">
								<label class="control-label">Data</label>
								<div class="controls">
									<input id="date_at" name="date_at" placeholder="RRRR-MM-DD" type="date" class="form-control">
									{{ MyValidator::printMessage($errors, 'date_at') }}
								</div>
							</div>
						@endif

						{{-- Liczba godzin kursu --}}
						<div class="control-group">
							<label class="control-label">Liczba godzin</label>
							<div class="controls">
								<input id="hours" name="hours" type="number" min="0" value="{{ $offer->hours }}" class="form-control">
								{{ MyValidator::printMessage($errors, 'hours') }}
							</div>
						</div>

						{{-- Cena --}}
						<div class="control-group">
							<label class="control-label">Cena</label>
							<div class="controls">
								<input id="price" name="price" placeholder="0000.00" type="text" value="{{ $offer->price }}" class="form-control inputmask" data-mask="9999,99 zł">
								{{ MyValidator::printMessage($errors, 'price') }}
							</div>
						</div>

					</fieldset>
				</div>
				<div class="tab-pane" id="recipient">
					<div class="control-group">
						<label class="control-label">Adresaci oferty</label>
						<div class="controls">
							<textarea name="recipient" class="ckeditor">{{ $offer->recipient }}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="range">
					<div class="control-group">
						<label class="control-label">Zakres oferty</label>
						<div class="controls">
							<textarea name="range" class="ckeditor"> {{ $offer->range }} </textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="syllabus">
					<div class="control-group">
						<label class="control-label">Program oferty</label>
						<div class="controls">
							<textarea name="syllabus" class="ckeditor"> {{ $offer->syllabus }} </textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="qualifications">
					<div class="control-group">
						<label class="control-label">Kwalifikacje</label>
						<div class="controls">
							<textarea name="qualifications" class="ckeditor">{{ $offer->qualifications }}</textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="additional">
					<div class="control-group">
						<label class="control-label">Dodatkowe informacje</label>
						<div class="controls">
							<textarea name="additional" class="ckeditor"> {{ $offer->additional }} </textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane well" id="attachments">
					<div class="control-group">
						<label class="control-label">Istniejący plik</label>
						<div class="controls">
							<div>{{ $offer->filename }}</div>
						</div>
					</div>
					<hr>
					<div class="control-group">
						<label class="control-label">Wgraj nowy plik lub zastąp istniejący</label>
						<div class="controls">
							<input type="file" name="offerfile" />
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Krótki opis pliku</label>
						<div class="controls">
							<input id="file-desc" name="file_desc" type="text" placeholder="100 znaków" maxlength="100" class="form-control" value="{{ $offer->file_desc }}">
							{{ MyValidator::printMessage($errors, 'file_desc') }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">

			</div>
		</div>
		{{ Form::close() }}

	</div>
</div>

@stop