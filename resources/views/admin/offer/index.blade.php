@extends('admin.default')

@section('admin-content')
@include('admin.parts.messages')
<div class="row">
	<div class="col-sm-12 col-md-6">
		<a href="{{ URL::route('admin-addoffer', $school->name) }}" class="btn btn-default">
			<span class="glyphicon glyphicon-plus"></span> Dodaj kurs
		</a>
	</div>
	<div class="col-sm-12 col-md-6">
		<div class="pull-right clearfix">{{ $offers->render() }}</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<table class="table">
			<thead>
			<form>
				<input type="hidden" name="set_new_filter" value="true">
				<tr>
					<td></td>
					<td>
						<input type="text" name="name_filter" value="{{Session::get('name_filter')}}" placeholder="Wpisz nazwę kursu lub wyrazy, które ona zawiera" class="form-control">
					</td>
					<td></td>
					<td>
						<select name="category_filter" class="form-control">
							<option value="%">Wszystkie</option>
							@foreach($categories as $category)
								@if(Session::get('category_filter') == $category->id)
									<option value="{{$category->id}}" selected>{{$category->name}}</option>
								@else
									<option value="{{$category->id}}">{{$category->name}}</option>
								@endif
							@endforeach
						</select>
					</td>
					<td></td>
					<td>
						<select name="status_filter" class="form-control">
							<option value="%">Wszystkie</option>
							@foreach($status as $oneStatus)
								@if(Session::get('status_filter') == $oneStatus->id)
									<option value="{{$oneStatus->id}}" selected>{{$oneStatus->name}}</option>
								@else
									<option value="{{$oneStatus->id}}">{{$oneStatus->name}}</option>
								@endif
							@endforeach
						</select>
					</td>
					<td style="width: 36px;">
						<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-filter"></span> Filtruj</button>
					</td>
				</tr>
			</form>
			<tr>
				<th>Poz</th>
				<th>Nazwa kursu</th>
				<th>Cena</th>
				<th>Kategoria</th>
				<th>Liczba godzin</th>
				<th>Status</th>
				<th style="width: 36px;">Menu</th>
			</tr>

			</thead>

			<tbody>
			@if($offers)
				@foreach ($offers as $offer)
					<tr>
						<td>{{$offer->position}}</td>
						<td>{!! $offer->name !!}</td>
						<td>{!! $offer->price !!}</td>
						<td>{!! $offer->category->name !!}</td>
						<td>{!! $offer->hours !!}</td>
						<td>{!! $offer->status->name !!}</td>
						<td>
							<div>
								<a href="{{ route('admin-editoffer', [$school->name, $offer->id]) }}" title="Edytuj ofertę">
									<span class="glyphicon glyphicon-pencil"></span>
								</a>
								<a data-target="#removeConfirmModal{{$offer->id}}" role="button" title="Usuń ofertę" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span></a>
							</div>
							<div>
								<a href="{{ route('admin-offer-up', [$school->name, $offer->id]) }}" title="W górę"><span class="glyphicon glyphicon-arrow-up"></span></a>
								<a href="{{ route('admin-offer-down', [$school->name, $offer->id]) }}" title="W dół"><span class="glyphicon glyphicon-arrow-down"></span></a>
							</div>
						</td>
						<div id="removeConfirmModal{{$offer->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removeConfirmModal{{$offer->id}}Label">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h3 class="modal-title" id="removeConfirmModal{{$offer->id}}Label">Usuwanie oferty</h3>
									</div>
									<div class="modal-body">
										<p>Czy na pewno chcesz usunąć ofertę <strong>{{ $offer->name }}</strong> w kategorii <strong>{{$offer->category->name}}</strong>?</p>
									</div>
									<div class="modal-footer">
										<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Nie usuwaj</button>
										<a href="{{ route('admin-deloffer', [$school->name, $offer->id]) }}" class="btn btn-primary">Usuń</a>
									</div>
								</div>
							</div>
						</div>
					</tr>
				@endforeach
			@else
				<div class="alert alert-block">
					<h4>Brak ofert
						<a href="{{ URL::route('admin-addoffer', $school) }}" class="btn btn-large pull-right">Dodaj ofertę</a>
					</h4>
					<p>Nie dodano jeszcze żadnej oferty dla danej grupy.</p>
				</div>
			@endif

			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="pull-right clearfix">{{ $offers->render() }}</div>
	</div>
</div>
@stop