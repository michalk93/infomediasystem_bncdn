@extends('admin.default')

@section('admin-css')
    {{ HTML::style('css/bootstrap/bootstrap-fileupload.min.css') }}
    {{ HTML::style('css/bootstrap/bootstrap-switch.css') }}
@stop

@section('admin-js')
    {{ HTML::script('js/bootstrap/bootstrap-fileupload.min.js') }}
    {{ HTML::script('js/bootstrap/bootstrap-switch.min.js')	}}
    {{ HTML::script('js/ckeditor/ckeditor.js') }}
@stop

{{------------------}}
{{-- SECTION START--}}

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="row">
    <div class="col-sm-12">
        {{ Form::open(['route' => ['admin-page-create-post', $school->name], 'enctype' => 'multipart/form-data'])}}
        {{-- Site title --}}
        <div class="control-group">
            <label class="control-label">Tytuł strony</label>
            <div class="controls">
                <input id="title" name="title" type="text" placeholder="200 znaków" maxlength="200" class="form-control" value="{{ old('title') }}" required>
                <p>Powyżej wpisujemy tytuł strony, który będzie wyświetlany na pasku adresu. Maksymalnie 200 znaków. Na jego podstawie tworzony jest adres do strony. Np. dla tytułu "Przykładowy tytuł" strona będzie miała adres: <i>www.bncdn.pl/teachers/przykladowy-tytul</i> </p>
                {{ MyValidator::printMessage($errors, 'title') }}
            </div>
        </div>

        {{-- Site content --}}
        <div class="control-group">
            <label class="control-label">Zawartość strony</label>
            <div class="controls">
                <textarea name="content" class="ckeditor"></textarea>
                {{ MyValidator::printMessage($errors, 'content') }}
            </div>
        </div>

        <button type="submit" class="btn btn-default">Zapisz stronę</button> <p class="label label-info">Po dodaniu strony należy skontaktować się z programistą w celu dodania na stronie linku w odpowiednim miejscu</p>
        {{ Form::close() }}
    </div>
</div>

@stop