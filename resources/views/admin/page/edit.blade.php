@extends('admin.default')

@section('admin-css')
    {{ HTML::style('css/bootstrap/bootstrap-switch.css') }}
@stop

@section('admin-js')
    {{ HTML::script('js/bootstrap/bootstrap-switch.min.js')	}}
    {{ HTML::script('js/ckeditor/ckeditor.js') }}
@stop

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="row">
    <div class="col-sm-12">
        {!!  Form::open(['route' => ['admin-page-create-post', $school->name], 'enctype' => 'multipart/form-data']) !!}
        {!!  Form::input('hidden', 'id', $page->id) !!}

        {{-- Site title --}}
        <div class="control-group">
            <label class="control-label">Tytuł strony {{ MyValidator::printMessage($errors, 'title') }}</label>
            <div class="controls">
                <input id="title" name="title" type="text" placeholder="200 znaków" maxlength="200" class="form-control" value="{!! $page->title !!}" required>
            </div>
        </div>

        {{-- Site content --}}
        <div class="control-group">
            <label class="control-label">Zawartość strony {{ MyValidator::printMessage($errors, 'content') }}</label>
            <div class="controls">
                <textarea name="content" class="ckeditor">{!! $page->content !!}</textarea>
            </div>
        </div>


        <button type="submit" class="btn btn-default">Zapisz stronę</button> <p class="label label-info">Po dodaniu strony należy skontaktować się z programistą w celu dodania na stronie linku w odpowiednim miejscu</p>
        {{ Form::close() }}
    </div>
</div>

@stop