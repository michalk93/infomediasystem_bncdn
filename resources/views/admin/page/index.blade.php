@extends('admin.default')

@section('admin-content')
@include('admin.parts.messages')
    <div class="btn-toolbar">
        <a href="{{ action('PagesController@create', $school->name) }}" class="btn btn-primary">Dodaj stronę</a>
    </div>
    @if(count($sites))
    <div class="well">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Tytuł strony</th>
                <th>Slug/URL</th>
                <th>Treść (32 początkowe słowa)</th>
                <th style="width: 36px;">Menu</th>
            </tr>
            </thead>
            <tbody>

                @foreach ($sites as $site)
                    <tr>
                        <td>{{$site->id}}</td>
                        <td>{{$site->title}}</td>
                        <td>{{$site->slug}}</td>
                        <td>{!! \Illuminate\Support\Str::words($site->content, 32) !!}</td>
                        <td>
                            <a target="_blank" href="{{ route('static_site', [$school->name, $site->slug]) }}" title="Zobacz stronę"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <a href="{{ route('page-edit', [$school->name, $site->id]) }}" title="Edytuj ofertę"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a href="{{ route('page-delete', [$school->name, $site->id]) }}" role="button" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span></a>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @else
    <div class="alert alert-block">
        <h4>Brak stron</h4>
        <p>Nie dodano jeszcze żadnych stron. Powyżej możesz dodać nową stronę.</p>
    </div>
    @endif
@stop