<div id="removeConfirmModal{{$oneStatus->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removeConfirmModal{{$oneStatus->id}}Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="removeConfirmModal{{$oneStatus->id}}Label">Usuwanie statusu</h3>
            </div>
            <div class="modal-body">
                <p>Czy na pewno chcesz usunąć status: <strong>{{ $oneStatus->name }}</strong>?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Nie usuwaj</button>
                <a href="{{ route('admin-delstatus', $oneStatus->id) }}" class="btn btn-primary">Usuń</a>
            </div>
        </div>
    </div>
</div>