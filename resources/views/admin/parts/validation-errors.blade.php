@if($errors->any())
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all('<li>:message</li>') as $error)
                    {!! $error !!}
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif