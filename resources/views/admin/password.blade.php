@extends('admin.default')

@section('admin-content')

<div class="well text-center">
	<strong>ZMIANA HASŁA DLA {{ Auth::user()->email }}</strong>
</div>

{{ Form::open(array(
	'route' => array('admin-change-password-post', Auth::user()->id),
	'method' => 'POST',
	'class' => 'form-horizontal'
)) }}

<div class="row well">
	<div class="control-group">
		<label class="control-label">Nowe hasło</label>
		<div class="controls">
			<input id="hours" name="password" type="password" class="input-xlarge">
			{{ MyValidator::printMessage($errors, 'password') }}
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Powtórz nowe hasło</label>
		<div class="controls">
			<input id="hours" name="password_confirmation" type="password" class="input-xlarge">
			{{ MyValidator::printMessage($errors, 'password_confirmation') }}
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<input type="submit" value="Zmień hasło" class="btn">
		</div>
	</div>
</div>
{{ Form::close() }}

@stop