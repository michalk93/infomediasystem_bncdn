@extends('admin.default')

@section('admin-css')
	{{ HTML::style('css/bootstrap/bootstrap-fileupload.min.css') }}
@stop

@section('admin-js')
	{{ HTML::script('js/bootstrap/bootstrap-fileupload.min.js') }}
	{{ HTML::script('js/ckeditor/ckeditor.js') }}
@stop

{{------------------}}
{{-- SECTION START--}}

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="row">
	<div class="col-sm-12">
		{!! Form::open(['route' => ['admin-addslide-post', 'school' => $school],'files' => 'true']) !!}
		<div class="control-group">
			<label class="control-label">Plik</label>
			<div class="controls">
				<input type="file" name="file" accept="image/jpg" required>
				{!! MyValidator::printMessage($errors, 'file')  !!}
				<p class="help-block">Plik graficzny: rozmiar {{ App\Models\Slider::SLIDE_WIDTH }}px x {{ App\Models\Slider::SLIDE_HEIGHT }}px | format: jpg</p>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Link</label>
			<div class="controls">
				<input id="link" name="link" type="text" placeholder="128 znaków | http://www.adres.pl/parametry" maxlength="128" class="form-control" value="{{ old('link') }}" required>
				<p class="help-block">Wklej lub wpisz adres www, do którego zostanie przeniesiony użytkownik po kliknięciu w slajd</p>
				{!! MyValidator::printMessage($errors, 'link')  !!}
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Opis</label>
			<div class="controls">
				<textarea name="description" id="slidefile-desc" class="ckeditor"></textarea>
				{!! MyValidator::printMessage($errors, 'description')  !!}
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<input type="submit" class="btn btn-success" value="Dodaj slajd">
			</div>
		</div>
		{!!  Form::close()  !!}
	</div>
</div>
@stop