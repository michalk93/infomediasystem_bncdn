@extends('admin.default')

@section('admin-css')
	{{ HTML::style('css/bootstrap/bootstrap-fileupload.min.css') }}
@stop

@section('admin-js')
	{{ HTML::script('js/bootstrap/bootstrap-fileupload.min.js') }}
	{{ HTML::script('js/ckeditor/ckeditor.js') }}
@stop

{{------------------}}
{{-- SECTION START--}}

@section('admin-content')
@include('admin.parts.validation-errors')
	<div class="row" id="viewEditSlide">
		<div class="col-sm-12">
			{!!  Form::open(['route' => ['admin-editslide-post', 'school' => $school->name, 'id' => $slide->id],
	'files' => 'true']) !!}

			<div class="well">
				<div class="row">
					<div class="col-sm-12">
						@if(isset($messages))
							<span class="alert">{{ $messages }}</span>
						@endif
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="control-group">
							<label class="control-label">Obecny plik</label>
							<div class="controls">
								{{ HTML::image(App\Models\Slider::$uploadPath.$school->name.'/'.$slide->filename, $slide->filename, array('style' => 'height: 100px')) }}
								{{ $slide->filename }}
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="control-group">
							<label class="control-label">Nowy plik {{ MyValidator::printMessage($errors, 'image') }}</label>
							<div class="controls">
								<input type="file" name="image" />
								<p class="help-block">Plik graficzny: rozmiar {{ App\Models\Slider::SLIDE_WIDTH }}px x {{ App\Models\Slider::SLIDE_HEIGHT }}px | format: jpg</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="control-group">
							<label class="control-label">Link</label>
							<div class="controls">
								<input id="link" name="link" type="text" placeholder="128 znaków | http://www.adres.pl/parametry" maxlength="128" class="input-xlarge" value="{{ $slide->link }}"  required>
								{{ MyValidator::printMessage($errors, 'link') }}
								<p class="help-block">Wklej lub wpisz adres www, do którego zostanie przeniesiony użytkownik po kliknięciu w slajd</p>

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="control-group">
							<label class="control-label">Opis</label>
							<div class="controls">
								<textarea name="description" id="slidefile-desc" class="ckeditor">{{ $slide->description }}</textarea>
								{{ MyValidator::printMessage($errors, 'description') }}
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="control-group">
							<div class="controls">
								<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-save"></span> Aktualizuj slajd</button>
							</div>
						</div>
					</div>
				</div>

			</div>
			{!!  Form::close() !!}
		</div>
	</div>
@stop