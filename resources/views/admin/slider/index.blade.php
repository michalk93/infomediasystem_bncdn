@extends('admin.default')

@section('admin-content')
	@include('admin.parts.messages')
	<div class="row">
		<div class="col-sm-12">
			<a href="{{ URL::route('admin-addslide', $school->name) }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Dodaj slajd</a>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-sm-12">
			<table class="table">
				<thead>
				<tr>
					<th>ID</th>
					<th>Nazwa pliku</th>
					<th>Podgląd</th>
					<th>Opis</th>
					<th style="width: 36px;">Menu</th>
				</tr>
				</thead>
				<tbody>
				@if($slides)
					@foreach ($slides as $slide)
						<tr>
							<td>{{$slide->id}}</td>
							<td>{{$slide->filename}}</td>
							<td>{{ HTML::image(\App\Models\Slider::$uploadPath.$school->name.'/'.$slide->filename, $slide->filename, ['style' => "height: 50px"]) }}</td>
							<td>{!! $slide->description !!}</td>
							<td>
								<a href="{{ route('admin-editslide', [$school->name, $slide->id]) }}" title="Edytuj slajd"><span class="glyphicon glyphicon-pencil"></span></a>
								<a href="#removeConfirmModal{{$slide->id}}" role="button" data-toggle="modal" title="Usuń slajd"><span class="glyphicon glyphicon-trash"></span></a>
							</td>

							<div class="modal fade" id="removeConfirmModal{{$slide->id}}" tabindex="-1" role="dialog" aria-labelledby="removeConfirmModal{{$slide->id}}Label">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Usuwanie slajdu</h4>
										</div>
										<div class="modal-body">
											<p>Czy na pewno chcesz usunąć slajd: {{HTML::image('img/slider/'.$school->name.'/'.$slide->filename, $slide->filename, array('style' => "height: 50px"))}} ?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Nie usuwaj</button>
											<a href="{{ route('admin-delslide', array($school->name, $slide->id)) }}" class="btn btn-primary">Usuń</a>
										</div>
									</div>
								</div>
							</div>
						</tr>
					@endforeach
				@else
					<div class="alert alert-block">
						<h4>Brak slajdów
							<a href="{{ URL::route('admin-addslide', $school->name) }}" class="btn btn-large pull-right">Dodaj slajd</a>
						</h4>
						<p>Nie dodano jeszcze żadnych slajdów</p>
					</div>
				@endif

				</tbody>
			</table>
		</div>
	</div>
@stop