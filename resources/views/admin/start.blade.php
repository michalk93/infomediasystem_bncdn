@extends('admin.default')

@section('admin-content')

@section('admin-css')
	<style>
		div.panel-header {
			height: 30px;
			padding: 5px;
			line-height: 30px;
			color: #aaa;
			background-color: #eee;
			border: 1px solid #ccc;
			text-transform: uppercase;
			font-weight: bold;
		}
		div.panel-content {
			border: 1px solid #ccc;
			background-color: #efefef;
		}
		div.panel-content a.fast-link {
			display: block;
			padding: 15px 0;
			font-size: 1.4em;
		}

	</style>
@stop

<div class="row">
	<div class="col-sm-12 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading text-center">Szybki dostęp</div>
			<div class="panel-body">
				<div class="list-group">
					<a href="{{route('admin-addoffer', 'teachers')}}" class="list-group-item">Dodaj ofertę dla nauczycieli</a>
					<a href="{{route('admin-timetable-add', 'teachers')}}" class="list-group-item">Dodaj harmonogram dla nauczycieli</a>
					<a href="{{route('admin-addnews', 'teachers')}}" class="list-group-item">Dodaj news'a dla nauczycieli</a>
					<a href="{{route('admin-applications', 'teachers')}}" class="list-group-item">Wyświetl zgłoszenia</a>
				</div>

			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading text-center">Statystyki</div>
			<div class="panel-body">
				<ul class="list-group">
					<li class="list-group-item">
						<span class="badge">{{ $offer_number['teachers'] + $offer_number['adults'] }}</span>
						Ilość wszystkich ofert
					</li>
					<li class="list-group-item"><span class="badge">{{ $offer_number['teachers'] }}</span>Ilość ofert dla nauczycieli</li>

					<li class="list-group-item"><span class="badge">{{ $offer_number['adults'] }}</span>Ilość ofert dla dorosłych</li>
					<hr>
					<li class="list-group-item"><span class="badge">{{ $news_number['teachers'] + $news_number['adults']}}</span>Ilość aktualności</li>

					<li class="list-group-item"><span class="badge">{{ $news_number['teachers'] }}</span>Ilość aktualności dla nauczycieli</li>


					<li class="list-group-item"><span class="badge">{{ $news_number['adults'] }}</span>Ilość aktualności dla dorosłych</li>

					<hr>
					<li class="list-group-item"><span class="badge">{{ $slides_number['teachers'] + $slides_number['adults']}}</span>Ilość wszystkich slajdów</li>


					<li class="list-group-item"><span class="badge">{{ $slides_number['teachers'] }}</span>Ilość slajdów dla nauczycieli</li>


					<li class="list-group-item"><span class="badge">{{ $slides_number['adults'] }}</span>Ilość slajdów dla dorosłych</li>

					<hr>
					<li class="list-group-item"><span class="badge">{{ $newsletter_number['teachers'] + $newsletter_number['adults']}}</span>Ilość adresów e-mail w newsletterze</li>


					<li class="list-group-item"><span class="badge">{{ $newsletter_number['teachers'] }}</span>Ilość adresów e-mail w newsletterze dla nauczycieli</li>


					<li class="list-group-item"><span class="badge">{{ $newsletter_number['adults'] }}</span>Ilość adresów e-mail w newsletterze dla dorosłych</li>

					<hr>
					<li class="list-group-item"><span class="badge">{{ $categories_number['teachers'] + $categories_number['adults']}}</span>Ilość kategorii</li>


					<li class="list-group-item"><span class="badge">{{ $categories_number['teachers'] }}</span>Ilość kategorii dla nauczycieli</li>


					<li class="list-group-item"><span class="badge">{{ $categories_number['adults'] }}</span>Ilość kategorii dla dorosłych</li>
					<hr>
					<li class="list-group-item"><span class="badge">{{ $status_number['all'] }}</span>Ilość statusów</li>
				</ul>

			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading text-center">Zgłoszenia</div>
			<div class="panel-body">
				<ul class="list-group">
					<li class="list-group-item @if($applications_number['notconfirmed'] > 0) list-group-item-danger @endif">
						<span class="badge">{{ $applications_number['notconfirmed'] }}</span>
						Nowe niezatwierdzone zgłoszenia
					</li>
					<li class="list-group-item">
						<span class="badge">{{ $applications_number['all'] }}</span>
						Ilość zgłoszeń
					</li>
					<li class="list-group-item">
						<span class="badge">{{ $applications_number['confirmed'] }}</span>
						Ilość zgłoszeń zatwierdzonych
					</li>
					<hr>
					<li class="list-group-item">
						<span class="badge">{{ $applications_number['teachers']['all'] }}</span>
						Ilość zgłoszeń dla nauczycieli
					</li>
					<li class="list-group-item @if($applications_number['teachers']['notconfirmed'] > 0) list-group-item-danger @endif" >
						<span class="badge">{{ $applications_number['teachers']['notconfirmed'] }}</span>
						Nowe niezatwierdzone zgłoszenia nauczycieli
					</li>
					<li class="list-group-item">
						<span class="badge">{{ $applications_number['teachers']['confirmed'] }}</span>
						Ilość zatwierdzonych zgłoszeń nauczycieli
					</li>
					<hr>
					<li class="list-group-item">
						<span class="badge">{{ $applications_number['adults']['all'] }}</span>
						Ilość zgłoszeń dla dorosłych
					</li>
					<li class="list-group-item @if($applications_number['adults']['notconfirmed'] > 0) list-group-item-danger @endif" >
						<span class="badge">{{ $applications_number['adults']['notconfirmed'] }}</span>
						Nowe niezatwierdzone zgłoszenia dorosłych
					</li>

					<li class="list-group-item">
						<span class="badge">{{ $applications_number['adults']['confirmed'] }}</span>
						Ilość zatwierdzonych zgłoszeń dla dorosłych
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@stop