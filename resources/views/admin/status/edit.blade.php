@extends('admin.default')

@section('admin-content')
@include('admin.parts.validation-errors')

<div class="row">
	<div class="col-sm-12 col-md-6">
		{{ Form::open(['route' => ['admin-editstatus-post', $status->id], 'method' => 'POST', 'class' => 'form-horizontal']) }}
			<label>Edytuj status</label>
			<input type="text" class="form-control" name="name" value="{{ $status->name }}" required><br/>
			<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-save"></span> Aktualizuj status</button>
		</div>
		{{ Form::close() }}
	</div>
</div>


@stop