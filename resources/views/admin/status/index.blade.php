@extends('admin.default')

@section('admin-content')
@include('admin.parts.messages')
@include('admin.parts.validation-errors')

<div class="row">
    <div class="col-sm-12">
        <div class="btn-toolbar well">
            {!! Form::open(['route' => 'admin-status']) !!}
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <input type="text" class="form-control" name="name" placeholder="Wpisz nazwę nowego statusu (max. 60 znaków)" maxlength="60" autofocus="autofocus" tabindex="1" required>
                </div>
                <div class="col-sm-12 col-md-6">
                    <button type="submit" class="btn btn-default" tabindex="2"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Dodaj status</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="well">
            <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nazwa statusu</th>
                  <th style="width: 36px;">Menu</th>
                </tr>
              </thead>
              <tbody>
                @if(count($status))
                    @foreach ($status as $oneStatus)
                        <tr>
                          <td>{{$oneStatus->id}}</td>
                          <td>{{$oneStatus->name}}</td>
                          <td>
                              <a href="{{ route('admin-editstatus', $oneStatus->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
                              <a data-target="#removeConfirmModal{{$oneStatus->id}}" role="button" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span></a>
                          </td>
                      <!-- Modal -->
                            <div id="removeConfirmModal{{$oneStatus->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removeConfirmModal{{$oneStatus->id}}Label">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title" id="removeConfirmModal{{$oneStatus->id}}Label">Usuwanie statusu</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p>Czy na pewno chcesz usunąć status: <strong>{{ $oneStatus->name }}</strong>?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Nie usuwaj</button>
                                            <a href="{{ route('admin-delstatus', $oneStatus->id) }}" class="btn btn-primary">Usuń</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>
                    @endforeach
                @else
                    <div class="alert alert-block">
                      <h4>Brak statusów dla ofert</h4>
                      <p>Nie dodano jeszcze żadnych statusów. Powyżej możesz dodać status.</p>
                    </div>
                @endif

              </tbody>
            </table>
        </div>
    </div>
</div>
@stop