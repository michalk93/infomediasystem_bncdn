@extends('admin.default')

@section('admin-content')
@include('admin.parts.validation-errors')
	<div class="row">
		<div class="col-sm-12">
			{{ Form::open(['route' => ['admin-timetable-edit-post', $school->name, $timetable->id], 'files' => 'true'])}}

			{{-- Kategoria kursu --}}
			<div class="control-group">
				<label class="control-label">Kategoria</label>
				<div class="controls">
					@if($categories)
						<select id="id-category" name="id_category" type="text" class="form-control" tabindex="1" autofocus>
							@foreach($categories as $category)
								@if($timetable->id_category == $category->id)
									<option value="{{$category->id}}" selected>{{$category->name}}</option>
								@else
									<option value="{{$category->id}}">{{$category->name}}</option>
								@endif
							@endforeach
						</select>
					@else
						<span class="alert"><span>Brak dodanych kategorii. </span><a class="btn btn-small" href="{!! route('admin-categories', $school->name) !!}">Dodaj kategorię</a></span>
					@endif
					{!! MyValidator::printMessage($errors, 'id_category') !!}
				</div>
			</div>

			{{-- Timetable description --}}
			<div class="control-group">
				<label class="control-label">Opis</label>
				<div class="controls">
					<input id="news-content" name="description" type="text" placeholder="255 znaków" maxlength="255" class="form-control" tabindex="2" value="{{$timetable->description}}">
					{!! MyValidator::printMessage($errors, 'description') !!}
				</div>
			</div>

			{{-- File input --}}
			<div class="control-group">
				<label class="control-label">Istniejący plik</label>
				<div class="controls">
					<input type="text" value="{{$timetable->filename}}" class="form-control" disabled> {{link_to_route('timetable-download', "Pobierz",array($school->name, $timetable->id))}}</span>
					{!! MyValidator::printMessage($errors, 'file') !!}
				</div>
			</div>

			{{-- File input --}}
			<div class="control-group">
				<label class="control-label">Nowy plik</label>
				<div class="controls">
					<input type="file" name="file" tabindex="3">
					{!! MyValidator::printMessage($errors, 'file') !!}
				</div>
			</div>

			{{-- Form button --}}
			<div class="control-group">
				<div class="controls">
					<input value="Aktualizuj" type="submit" class="btn btn-success" tabindex="4">
				</div>
			</div>

			{!! Form::close() !!}

		</div>
	</div>



@stop