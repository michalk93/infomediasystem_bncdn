@extends('admin.default')

@section('admin-content')
	@include('admin.parts.messages')
	<div class="row">
		<div class="col-sm-12">
			<a href="{{ URL::route('admin-timetable-add', $school->name) }}" role="button" class="btn btn-primary">Dodaj harmonogram</a>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-sm-12">
			<table class="table">
				<thead>
				<tr>
					<th>ID</th>
					<th>Opis</th>
					<th>Kategoria</th>
					<th>Plik</th>
					<th>Data</th>
					<th style="width: 36px;">Menu</th>
				</tr>
				</thead>
				<tbody>
				@if($timetables->isEmpty())
					<div class="alert alert-block">
						<h4>Brak ofert</h4>
						<p>Nie dodano jeszcze żadnego harmonogramu</p>
					</div>
				@else
					@foreach ($timetables as $timetable)
						<tr>
							<td>{{$timetable->id}}</td>
							<td>{{$timetable->description}}</td>
							<td>{{$timetable->category->name}}</td>
							<td>{{$timetable->filename}}</td>
							<td>{{$timetable->updated_at}}</td>
							<td>
								<a href="{{ route('admin-timetable-delete', array($school->name, $timetable->id)) }}" role="button" title="Usuń harmonogram"><span class="glyphicon glyphicon-trash"></span></a>
								<a href="{{ route('admin-timetable-edit', array($school->name, $timetable->id)) }}" role="button" title="Edytuj harmonogram"><span class="glyphicon glyphicon-pencil"></span></a>
							</td>
						</tr>
					@endforeach
				@endif

				</tbody>
			</table>
			{{ $timetables->render() }}
		</div>
	</div>
@stop