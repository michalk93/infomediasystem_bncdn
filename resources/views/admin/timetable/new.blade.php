@extends('admin.default')

@section('admin-content')
@include('admin.parts.validation-errors')
<div class="row">
	<div class="col-sm-12">
		{!!  Form::open(['route' => ['admin-timetable-add-post', 'school' => $school->name], 'files' => 'true']) !!}

		{{-- Kategoria kursu --}}
		<div class="control-group">
			<label class="control-label">Kategoria</label>
			<div class="controls">
				@if($categories)
					<select id="id-category" name="id_category" type="text" class="form-control">
						@foreach($categories as $category)
							<option value="{{$category->id}}">{{$category->name}}</option>
						@endforeach
					</select>
				@else
					<span class="alert"><span>Brak dodanych kategorii. </span><a class="btn btn-small" href="{{ route('admin-categories', $school->name) }}">Dodaj kategorię</a></span>
				@endif
				{!! MyValidator::printMessage($errors, 'id_category')  !!}
			</div>
		</div>

		{{-- Timetable description --}}
		<div class="control-group">
			<label class="control-label">Opis</label>
			<div class="controls">
				<input id="news-content" name="description" type="text" placeholder="255 znaków" maxlength="255" class="form-control" tabindex="1" value="{{old('description')}}">
				{!! MyValidator::printMessage($errors, 'description')  !!}
			</div>
		</div>

		{{-- File input --}}
		<div class="control-group">
			<label class="control-label">Plik</label>
			<div class="controls">
				<input type="file" name="file">
				{!! MyValidator::printMessage($errors, 'file')  !!}
			</div>
		</div>

		{{-- Form button --}}
		<div class="control-group">
			<div class="controls">
				<input value="Dodaj" type="submit" class="btn btn-success" tabindex="3">
			</div>
		</div>

		{!! Form::close() !!}

	</div>
</div>
@endsection