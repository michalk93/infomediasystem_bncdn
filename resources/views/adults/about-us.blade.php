@extends('adults.start')

@section('main-content')
<div id="about-us-wrapper">
	<div class="media">
	  <a class="pull-left" href="#">
	    {{HTML::image('img/about-us-center.jpg', 'about-us', array('class' => 'img-polaroid', 'width' => '128'))}}
	  </a>
	  <div class="media-body text-left">
	    <h4 class="media-heading color">MISJA</h4>

	    <div class="media">
	    	<div><p>Beskidzkie Niepubliczne Centrum Doskonalenia Nauczycieli w Suchej Beskidzkiej jest placówką wspierającą dyrektorów i nauczycieli we wszystkich obszarach ich aktywności zawodowej. Pomagamy dyrektorom i nauczycielom w rozwoju zawodowym poprzez indywidualne formy pracy na etapach planowania, realizacji i ewaluacji.</p>
			<p>Prowadzimy szkolenia dostosowane do potrzeb uczestników pod względem formy, tematu, czasu trwania i miejsca.Wspieramy szkolenia w planowaniu rozwoju i tworzenia programów uwzględniając specyfikę każdej placówki. Szczególnie bliskie są nam działania propagujące twórczą aktywność nauczycieli i rad pedagogicznych.</p>
			</div>

	    </div>
	  </div>
	</div>

	<div class="media">
	  <a class="pull-right" href="#">
	    {{HTML::image('img/about-us-right.jpg', 'about-us', array('class' => 'img-polaroid', 'width' => '300', ))}}
	  </a>
	  <div class="media-body">
	    <h4 class="media-heading color">WIZJA</h4>
		<div>
			<p>Pragniemy, by placówka oprócz działań realizowanych aktualnie poszerzała swoja ofertę edukacyjną o:</p>
			<ul>
				<li>Kursy nadające dodatkowe kwalifikacje:  kurs pedagogiczny da nauczycieli praktycznej nauki zawodu</li>
				<li>Kursy doskonalące w zakresie profilaktyki szkolnej</li>
				<li>Seminaria</li>
				<li>Warsztaty metodyczne</li>
				<li>Szkolenia rad pedagogicznych</li>
			</ul>
		</div>
	  </div>
	</div>
</div>
@stop