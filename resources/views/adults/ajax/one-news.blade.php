<article>
	<header>
		<span>{{ $news->updated_at }}</span>
	</header>
	<section><em class="muted">{{ $news->header }}</em></section>
	<section>{{ $news->content }}</section>
</article>