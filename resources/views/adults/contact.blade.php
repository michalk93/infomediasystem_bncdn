@extends('adults.start')

@section('main-content')
<div id="contact-wrapper" class="row">
	<div class="pull-left">
		<p>Masz pytanie? Jesteśmy do Twojej dyspozycji.</p>
		<div id="phone-contact" class="clear">
			{{ HTML::image('img/phone-icon-blue.jpg', 'phone', array('class' => 'pull-left')) }}
			<div class="contact-text left">
				<h3>(33) 874 45 36</h3>
			</div>
			<div class="clear"></div>
		</div>
		<div id="address-contact" class="clear">
			{{ HTML::image('img/address-icon-blue.jpg', 'phone', array('class' => 'pull-left')) }}
			<div class="contact-text left">
				<h3>INFO-MEDIA SYSTEM</h3>
				<span>Sucha Beskidzka, ul. Kościelna 5</span><br>
				<span>34-200 Sucha Beskidzka</span><br>
				<span>(budynek ZS im. W. Goetla - pokój 109)</span>
			</div>
			<div class="clear"></div>

		</div>
	</div>
	<div class="pull-left">
		<div id="email-contact">
			{{ HTML::image('img/mail-icon-blue.jpg', 'phone', array('class' => 'left')) }}
			<div class="contact-text pull-left">
				<h3><a href="mailto:bncdn@poczta.fm">bncdn@poczta.fm</a></h3>
				<p>lub skorzystaj z poniższego formularza kontaktowego</p>
			</div>
		</div>
		<div id="email-form" class="clear right">
			<form action="{{ route('contact-email-send', $school) }}" method="post">
				<div>
					<label for="name">Imię i nazwisko:</label>
					<input type="text" id="name" name="name" placeholder="Wpisz swoje imię i nazwisko" required>
					{{ MyValidator::printMessage($errors, 'name') }}
				</div>
				<div>
					<label for="email" class="left">Twój e-mail:</label>
					<input type="email" id="email" name="email" placeholder="Wpisz swój adres email" required>
					{{ MyValidator::printMessage($errors, 'email') }}
				</div>
				<div>
					<label for="subject">Temat:</label>
					<input type="text" id="subject" name="subject" placeholder="Wpisz temat wiadomości" required>
					{{ MyValidator::printMessage($errors, 'subject') }}
				</div>
				<div>
					<label for="content">Treść:</label>
					<textarea name="content" id="content" cols="30" rows="10" placeholder="Wpisz treść wiadomości" required></textarea>
					{{ MyValidator::printMessage($errors, 'content') }}
				</div>
				<div>
					<input type="submit" id="email-submit-bt" value="WYŚLIJ WIADOMOŚĆ" class="right bold" >
				</div>
			</form>
		</div>
	</div>
	<h3>Gdzie jesteśmy?</h3>
</div>
<div class="thumbnail" style="padding: 10px; margin: 10px">
	<iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Beskidzkie+Niepubliczne+Centrum+Doskonalenia+Nauczycieli,+Ko%C5%9Bcielna,+Sucha+Beskidzka&amp;aq=0&amp;oq=beskidzkie+nie&amp;sll=50.045545,19.971777&amp;sspn=0.151025,0.41851&amp;ie=UTF8&amp;hq=Beskidzkie+Niepubliczne+Centrum+Doskonalenia+Nauczycieli,&amp;hnear=Ko%C5%9Bcielna,+34-200+Sucha+Beskidzka,+suski,+Wojew%C3%B3dztwo+ma%C5%82opolskie&amp;t=m&amp;cid=17143844740310878795&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Beskidzkie+Niepubliczne+Centrum+Doskonalenia+Nauczycieli,+Ko%C5%9Bcielna,+Sucha+Beskidzka&amp;aq=0&amp;oq=beskidzkie+nie&amp;sll=50.045545,19.971777&amp;sspn=0.151025,0.41851&amp;ie=UTF8&amp;hq=Beskidzkie+Niepubliczne+Centrum+Doskonalenia+Nauczycieli,&amp;hnear=Ko%C5%9Bcielna,+34-200+Sucha+Beskidzka,+suski,+Wojew%C3%B3dztwo+ma%C5%82opolskie&amp;t=m&amp;cid=17143844740310878795&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">Wyświetl większą mapę</a></small>
</div>
@stop