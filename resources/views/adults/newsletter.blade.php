@extends('adults.start')

@section('main-content')
<div class="newsletter-wrapper">
	<div class="thumbnail center well well-small text-center" style="padding: 50px 0;">
		<h2>Newsletter</h2>
		<p>Zapisz się na newsletter, aby otrzymywać najnowsze informacje</p>
		<form action="{{route('newsletter-post', $school)}}" method="post">
			<div class="input-prepend">
				<span class="add-on"><i class="icon-envelope"></i></span>
				<input type="text" name="email" placeholder="twoj@email.pl">
			</div>
			<div>
				@if(!empty($message))
					{{$message}}
				@endif
			</div>
			<input type="submit" value="Subskrybuj!" class="btn btn-large" />
		</form>
	</div>
</div>
@stop