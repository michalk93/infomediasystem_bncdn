@extends('adults.start')

@section('main-content')
<div id="offert-wrapper" class="clearfix">
	<div class="clearfix">
		{{ Session::get('msgSignUpSuccess', '')}}
		{{ Session::forget('msgSignUpSuccess') }}
		<nav id="offert-menu" class="pull-left">
			<h4 class="color">Kategorie</h4>
			@if($categories)
				@foreach($categories as $category)
					@if($offer->id_category == $category->id)
						<a href="{{route('offer', array($school, $category->id))}}" class="offert-link active">{{ $category->name }}</a>
					@else
						<a href="{{route('offer', array($school, $category->id))}}" class="offert-link">{{ $category->name }}</a>
					@endif
				@endforeach
				<a href="{{route('timetable', array($school))}}" class="offert-link active" style="text-transform: uppercase; margin: 20px 0">Harmonogramy</a>
			@else
				<h4 class="text-center color">Brak kategorii</h4>
			@endif
		</nav>
		<section id="offert-viewport" class="pull-left clearfix">
			@if($offer->status_name != "Rekrutacja zakończona")
			<p class="text-center" style="margin-bottom: 10px;"><a href="{{ route('offer-signup', array($school, $offer->id)) }}" class="btn btn-mini">Zapisz się na kurs przez Internet</a></p>
			@endif
			<table class="table table-striped">
			<tr>
				<th>NAZWA</th>
				<td><strong>{{ $offer->name }}</strong></td>
			</tr>
			<tr>
				<th>KATEGORIA</th>
				<td>{{ $offer->category_name }}</td>
			</tr>
			<tr>
				<th>STATUS</th>
				<td>
					{{ $offer->status_name }}
					@if($offer->date_at)
						: {{ $offer->date_at }}
					@endif
				</td>
			</tr>
			<tr>
				<th>LICZBA GODZIN</th>
				<td>{{ $offer->hours }}</td>
			</tr>
			<tr>
				<th>CENA</th>
				<td>{{ $offer->price }}</td>
			</tr>
			@if(!empty($offer->recipient))
			<tr>
				<th>ADRESACI</th>
				<td>{{ $offer->recipient }}</td>
			</tr>
			@endif
			@if(!empty($offer->range))
			<tr>
				<th>ZAKRES</th>
				<td>{{ $offer->range }}</td>
			</tr>
			@endif
			@if(!empty($offer->syllabus))
			<tr>
				<th>PROGRAM</th>
				<td>{{ $offer->syllabus }}<td>
			</tr>
			@endif
			@if(!empty($offer->qualifications)
	)		<tr>
				<th>KWALIFIKACJE</th>
				<td>{{ $offer->qualifications }}</td>
			</tr>
			@endif
			@if(!empty($offer->additional))
			<tr>
				<th>DODATKOWE INFORMACJE</th>
				<td>{{ $offer->additional }}</td>
			</tr>
			@endif
			@if(!empty($offer->filename))
			<tr>
				<th>ZAŁĄCZNIK/PLIK</th>
				<td> {{ HTML::link(route('offer-download', array($school, $offer->filename)), $offer->file_desc.' (pobierz)', array('class' => 'btn')) }}</td>
			</tr>
			@endif
		</table>
		@if($offer->status_name != "Rekrutacja zakończona")
		<div class="text-center">
			<a href="{{ route('offer-signup', array($school, $offer->id)) }}" class="btn btn-large">Zapisz się na kurs przez Internet</a>
		</div>
		@endif
		</section>
	</div>
	@include('subviews.sign-info')
</div>

@stop