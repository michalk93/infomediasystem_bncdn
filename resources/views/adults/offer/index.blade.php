@extends('adults.start')

@section('main-content')
<div id="offert-wrapper" class="clearfix">
	<div class="clearfix">
		@if(Session::has('msgSignUpSuccess'))
			@include('subviews.query-modal')
		@endif
		{{ Session::get('msgSignUpSuccess', '')}}
		{{ Session::forget('msgSignUpSuccess') }}
		<nav id="offert-menu" class="pull-left">
			<h4 class="color">Kategorie</h4>
			@if($categories)
				@foreach($categories as $category)
					@if($id_category == $category->id)
						<a href="{{route('offer', array($school, $category->id))}}" class="offert-link active">{{ $category->name }}</a>
					@else
						<a href="{{route('offer', array($school, $category->id))}}" class="offert-link">{{ $category->name }}</a>
					@endif
				@endforeach
				<a href="{{route('timetable', array($school))}}" class="offert-link active" style="text-transform: uppercase; margin: 20px 0">Harmonogramy</a>
			@else
				<h4 class="text-center color">Brak kategorii</h4>
			@endif
		</nav>
		<section id="offert-viewport" class="pull-left clearfix">
			<header>Kliknij wybrany kurs, aby wyświetlić szczegółowe informacje</header>
			<div class="clearfix">
				@if($offer)
					@foreach ($offer as $oneOffer)
						<a href="{{ URL::route('offer-details', array($school, $oneOffer->id)) }}" class="offert-item left">
							<div class="offert-item-content">
								<div class="left item-icon">
									{{HTML::image('img/offert-item-icon.png', "offert-item")}}
									<p>{{ $oneOffer->hours }} godzin</p>
								</div>
								<div class="item-title pull-left">
									<h5>{{ $oneOffer->name }}</h5>
								</div>
								<div class="clear item-status">
									<p>
										{{$oneOffer->status_name}}
										@if($oneOffer->date_at)
										: {{ $oneOffer->date_at }}
										@endif
									</p>
								</div>
							</div>
							<div class="hover-overlay bg-color">Kliknij, aby wyświetlić szczegóły</div>
						</a>
					@endforeach
				@else
					<h4 class="text-center color">Brak ofert</h4>
				@endif
			</div>
			@if($offer)
				<footer class="text-center"> {{ $offer->links() }} </footer>
			@endif
		</section>
	</div>
	@include('subviews.sign-info')
</div>
@stop