@extends('adults.start')

@section('main-content')
<div style="padding: 10px 100px">
 <form id="form-signup" action="{{ route('offer-signup-post', array($school, $offer->id)) }}" method="post">
    <fieldset>
        {{-- Sign up offer form --}}
		<div>
			<div id="creator-control-buttons" class="pull-right clearfix">
		    	<button type="submit" id="btn-submit" class="btn pull-right" data-title="Wyślij zgłoszenie" data-content="Jeżeli wypełniłeś już cały formularz, wyślij go do nas i złóż zgłoszenie klikając ten przycisk">Złóż zgłoszenie</button>
		    </div>
			<p style="font-size: 24px">Formularz zgłoszeniowy</p>
			<p style="font-size: 16px"><i>{{ $offer->category_name }}: {{ $offer->name }}</i></p>
			
		</div>

		
		<style>
			input::-webkit-input-placeholder {
			   color: #999;
			   font-weight: normal;
			}

			input:-moz-placeholder { /* Firefox 18- */
			   color: red;  
			}

			input::-moz-placeholder {  /* Firefox 19+ */
			   color: red;  
			}

			input:-ms-input-placeholder {  
			   color: red;  
			}
			form#form-signup {
				min-height: 500px;
			}
			label {
				width: 200px;
				text-align: right;
				float:left;
			}

			.tab-content {
				margin-top: 40px;
			}
			.tab-content >div {
				margin: 0 20px 20px 0; padding: 0px 20px; color: #333;
				position: relative;
			}

			.tab-content > div > input[type="text"] {
				background: none; box-shadow: none !important; border-radius: 0px; border: none; border-bottom: 1px solid #333; width: 400px; padding: 0;
				font-style: italic; margin-left: 5px; font-weight: bold;
			}
			.tab-content > div > input[type="text"]:focus {
				border-color: #06f;
				border-width: 2px;
			}

			.tab-content >div > div {
				position: absolute;
				right: 0;
				top: 0;
			}

			div#creator-control-buttons {
				margin: 20px 0;
			}
			</style>

	    @if($errors->count())
	 		<div class="alert alert-danger">
		 		<i class="icon-exclamation-sign"></i> W formularzu wystąpiły błędy. Popraw je i wyślij formularz ponownie.
		 	</div>
	 	@endif
	    

		{{-- TAB 1 CONTENT --}}
		<div id="tab-1-content" class="tab-content">
			
				
	        {{-- Lastname --}}
	        <div>
	        	<label  for="lastname">Nazwisko:</label>
                <input id="lastname" name="lastname" type="text" autofocus="autofocus" value="{{Input::old('lastname')}}" data-content="Wpisz swoje nazwisko">
                <div class="pull-right">{{MyValidator::printMessage($errors, 'lastname')}}</div>
	        </div>

	        {{-- Firstname --}}

            <div>
            	<label for="firstname">Pierwsze imię:</label>
                <input id="firstname" name="firstname" type="text" value="{{Input::old('firstname')}}" data-content="Wpisz swoje imię">
                <div class="pull-right">{{MyValidator::printMessage($errors, 'firstname')}}</div>
            </div>


	        {{-- Secondname --}}
	        <div>
	        	<label for="secondname">Drugie imię:</label>
                <input id="secondname" name="secondname" type="text" value="{{Input::old('secondname')}}" data-content="Jeżeli posiadasz drugie imię wpisz je w tym polu lub pozostaw to pole puste" >
                <div class="pull-right">{{MyValidator::printMessage($errors, 'secondname')}}</div>
            </div>

	        {{-- Date of birth --}}
	        <div>
	        	<label for="birthdate">Data urodzenia:</label>
                <input id="birthdate" name="birthdate" type="text" value="{{Input::old('birthdate')}}" data-content="Wpisz datę urodzenia w formacie: RRRR-MM-DD, np. 1976-05-12. Możesz też wybrać odpowiednią datę korzystając z wyświetlonego kalendarza, wybierając najpierw rok, potem miesiąc, a na końcu dzień swoich urodzin.">
                <div class="pull-right">{{MyValidator::printMessage($errors, 'birthdate')}}</div>
            </div>

	        {{-- Place of birth --}}
	        <div>
	        	<label for="birthplace">Miejsce urodzenia:</label>
                <input id="birthplace" name="birthplace" type="text" value="{{Input::old('birthplace')}}" data-content="Wpisz nazwę miejscowości, w której się urodziłeś">
                <div class="pull-right">{{MyValidator::printMessage($errors, 'birthplace')}}</div>
            </div>

	        {{-- Address --}}
	        <div>
	        	<label for="address">Adres:</label>
                <input id="address" name="address" type="text" value="{{Input::old('address')}}" data-content="Podaj swój adres wraz z numerem domu/lokalu, ulicą/osiedlem, kodem pocztowym i pocztą, np. Warszawa ul. Wolności 17, 00-999 Warszawa">
                <div class="pull-right">{{MyValidator::printMessage($errors, 'address')}}</div>
            </div>

	        {{-- E-mail address --}}
	        <div>
	        	<label for="email">Adres e-mail:</label>
                <input id="email" name="email" type="text" value="{{Input::old('email')}}" data-content="Wpisz swój prawdziwy adres e-mail, który posłuży nam do dalszego kontaktu z Tobą">
                <div class="pull-right">{{MyValidator::printMessage($errors, 'email')}}</div>
            </div>

	        {{-- Phone number --}}
	        <div>
	        	<label for="phonenumber">Telefon kontaktowy:</label>
                <input id="phonenumber" name="phonenumber" type="text" value="{{Input::old('phonenumber')}}" data-content="Podaj numer telefonu, pod którym możemy się z Tobą skontaktować">
                <div class="pull-right">{{MyValidator::printMessage($errors, 'phonenumber')}}</div>
            </div>

            {{-- Education --}}
	         <div>
	         	<label for="education">Wykształcenie:</label>
                <input id="education" name="education" type="text" value="{{Input::old('education')}}" data-content="Wpisz swój stopień wykształcenia (zawodowe/średnie/wyższe)">
                <div class="pull-right">{{MyValidator::printMessage($errors, 'education')}}</div>
            </div>
            <hr>
	       {{-- Agreement to processing personal data --}}
			<div class="clearfix">
				<input id="agreement" name="agreement" type="checkbox" >
                <em>Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w zgłoszeniu na potrzeby realizacji procesu rekrutacji i realizacji kursu oraz wykonywania zadań określonych prawem realizowanych przez BNCDN, zgodnie z ustawą z dnia 29.08.1997 r. o ochronie danych osobowych (Dz.U. z 1997 nr 133, poz. 883).</em>
                {{MyValidator::printMessage($errors, 'agreement')}}
	        </div>

	         {{-- Agreement to receiving newsletter --}}
			<div class="control-group">
				<input id="newsletter" name="newsletter" type="checkbox">
                <em>Chcę otrzymywać informacje o nowych ofertach na adres e-mail podany podczas rejestracji</em>
	        </div>
        </div>

       
    </fieldset>
    <div class="text-center">
    	<a href="{{ route('offer', $school) }}" class="btn">Rezygnuj</a>
		<input type="reset" value="Wyczyść formularz" class="btn">
		<button type="submit" class="btn" data-trigger="hover" data-title="Wyślij zgłoszenie" data-content="Jeżeli wypełniłeś już cały formularz, wyślij go do nas i złóż zgłoszenie klikając ten przycisk">Złóż zgłoszenie</button>
    </div>

</form>
</div>

@stop

@section('css-styles')
	@parent
	{{HTML::style('css/bootstrap/datepicker.css')}}
	{{HTML::style('css/bootstrap/bootstrap-switch.css')}}
@stop

@section('js-scripts')
	@parent
	{{HTML::script('js/bootstrap/bootstrap-datepicker.js')}}
	{{HTML::script('js/bootstrap/locales/bootstrap-datepicker-pl.js')}}
	{{HTML::script('js/bootstrap/bootstrap-switch.min.js')}}
	<script>
		$(document).ready(function(){
			$('input#birthdate').datepicker({
				format: 'yyyy-mm-dd',
				language: 'pl',
				startView: 2,
				autoclose: true
			})
			$('button:submit, #btn-next, #btn-prev').popover({'container' : 'body', 'placement' : 'bottom', 'trigger' : 'hover'});
			$('input:text').popover({'trigger' : 'focus', 'placement' : 'bottom', 'container': 'body', 'title' : 'Wskazówka wypełnienia'});
			$('input:focus').popover('show');


			/*activeTab = 0;
			NUM_TAB = 1;
			$('button#btn-next').click(function(){
				activeTab = ++activeTab % NUM_TAB;
			})
			$('button#btn-prev').on('click', function(){
				activeTab = --activeTab % NUM_TAB;
			})

			$('div#creator-control-buttons > button').on('click', function(){
				if(activeTab % NUM_TAB) {
					$('button#btn-prev').show();
				}else{
					$('button#btn-prev').hide();
				}
				if((activeTab % NUM_TAB) == (NUM_TAB-1)){
					$('button#btn-next').hide();
					$('button#btn-submit').show();
				}else {
					$('button#btn-next').show();
					$('button#btn-submit').hide();
				}
				$('.tab-content.active').hide().removeClass('active');
				$('.tab-content').eq(activeTab).show().addClass('active');

				$('div#tabs-label > div').fadeTo('medium', 0.3).eq(activeTab).fadeTo('medium', 1)

			})*/
		})
	</script>
@stop