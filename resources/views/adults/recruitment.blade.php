@extends('adults.start')

@section('main-content')
<div id="recruitment-wrapper">
	<div class="row">
		<div class="span12 text-center">
			{{HTML::image('img/recruitment.jpg', 'recruitment.jpg')}}
		</div>
	</div>
	<div class="row">
		<section class="span4">
			<div class="content">
				<h3 class="color">Ogólne informacje</h3>
				<p>Na kurs można zapisać się telefonicznie lub przez Internet, wybierając odpowiedni kurs i wypełniając formualrz zgłoszeniowy na naszej stronie. Można także zapisać się bezpośrednio w sekretariacie Centrum.</p>
				<p>Kursy rozpoczynamy w momencie zgłoszenia sie wystarczającej ilości chętnych.</p>
			</div>
		</section>
		<section class="span4">
			<div class="content">
				<h3 class="color">Wymagania</h3>
				<p>
					Dokumenty, które należy złożyć w sekretariacie Centrum przed pierwszymi zajęciami:
				</p>
				<ol>
					<li>Dyplom ukończenia studiów wyższych lub ksero dyplomu poświadczone przez jednostkę przyjmujacą dokumenty (dla kursów kawlifikacyjnych)</li>
					<li>Zaświadczenie o zatrudnieniu w placówce oświatowej</li>
					<li>Kwestionariusz osobowy (druk dostepny w sekretariacie)</li>
					<li>Zobowiazanie finansowe - dotyczy kursów kawlifikacyjnych (druk dostepny w sekretariacie)</li>
				</ol>
				
			</div>
		</section>
		<section class="span4">
			<div class="content">
				<h3 class="color">Opłaty</h3>
				<p>Wpłaty za kursy prosimy kierować na konto lub wpłacać bezpośrednio w sekretariacie Centrum.</p>
				<div>
					<p class="bold">Dane do przelewu:</p>
					<p>INFO-MEDIA SYSTEM </p>
					<p>34-200 Sucha Beskidzka, ul. Zofii Karaś 9</p>	
					<p>Bank Spółdzielczy o/Sucha Beskidzka</p>
				</div>
				<div>
					<p class="bold">Nr konta:</p>
					<p>14 8128 0005 0002 8613 2000 0010</p>
				</div>
			</div>
		</section>
	</div>
</div>
@stop