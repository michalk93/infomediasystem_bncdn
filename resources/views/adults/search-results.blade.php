@extends('adults.start')

@section('main-content')
<div id="search-results-wrapper">
@if(isset($error))
	<div class="alert alert-error">{{ $error }}</div>
@else
	@if(empty($results['offer']))
		<h3>Brak rezultatów w ofertach</h3>
	@else
		<h3>Znaleziono w ofertch - {{ count($results['offer']) }}</h3>
		@foreach($results['offer'] as $offer_result)
			<div><a href="{{route('offer', [$school, $offer_result->id_category])}}">{{ $offer_result->name }}</a></div>
		@endforeach
	@endif
	<hr>
	@if(empty($results['news']))
		<h3>Brak rezultatów w aktualnościach</h3>
	@else
		<h3>Znaleziono w aktualnościach - {{ count($results['news']) }}</h3>
		@foreach($results['news'] as $news_result)
			<div>{{ $news_result->content }}</div>
		@endforeach
	@endif
@endif
</div>
@stop