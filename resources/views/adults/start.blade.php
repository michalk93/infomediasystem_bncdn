@extends('templates.adults')

@section('css-styles')
	@parent
 	{{ HTML::style('css/teachers/teachers-school.css') }}
 	{{ HTML::style('css/bootstrap/bootstrap.css') }}
@stop

@section('js-scripts')
	@parent
	{{ HTML::script('js/bootstrap/bootstrap.min.js', array('type' => 'text/javascript')) }}
@stop

@section('site-title')
BNC - Dorośli
@stop

@section('title-header')
<span>BESKIDZKIE NIEPUBLICZNE CENTRUM KSZTAŁCENIA DOROSŁYCH</span>
@stop


{{------------------------------------}}
{{--     MAIN CONTENT SECTION       --}}
{{------------------------------------}}
@section('main-content')
	{{-- SLIDER --}}
	@if(!empty($slides))
		<div id="slider-wrapper" class="wrapper">
			<div id="homeCarousel" class="carousel slide">
				<ol class="carousel-indicators">
				@foreach($slides as $slide)
					<li data-target="#myCarousel" data-slide-to="{{ $slide->id }}"></li>
				@endforeach
				</ol>
				<!-- Carousel items -->
				<div class="carousel-inner">
				@foreach($slides as $slide)
					<div class="item text-center">
						<a href="{{$slide->link}}">
							{{ HTML::image(App\Models\Slider::$uploadPath.$school.'/'.$slide->filename, $slide->filename, array('height' => App\Models\Slider::SLIDE_HEIGHT, 'width' => App\Models\Slider::SLIDE_WIDTH)) }}
						</a>
					@if(!empty($slide->description))
					<a href="{{$slide->link}}">
	               		<div class="carousel-caption">
	                  		{{ $slide->description }}
	                	</div>
	                </a>
	                @endif
	                </div>
	            @endforeach

				</div>
				<!-- Carousel nav -->
				<a class="carousel-control left" href="#homeCarousel" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#homeCarousel" data-slide="next">&rsaquo;</a>
			</div>
		</div>
		{{HTML::image('img/carousel-shadow.png', '')}}
	@endif

	{{-- News --}}
	@if(isset($news) && !$news->isEmpty())
	<div id="news-wrapper" class="wrapper row">
		<section id="news" class="span6">
			<header>
				<h3>Aktualności</h3>
			</header>
			
				@foreach($news as $oneNews)
					<article>
						<div>
							<?php $printDate = App\Models\News::printDate($oneNews->updated_at); ?>
							{{-- HTML::image('img/icon-news.png', '', array('class' => 'pull-left', 'width' => 32, 'height' => 32)) --}} 
							<div class="pull-left news-date bg-color">{{ $printDate['day'] }}<br>{{ $printDate['month'] }}</div>
							<section>{{ \Illuminate\Support\Str::words($oneNews->content, 50) }}</section>
							<footer><a href="{{ route('news', array('school' => $school, 'id' => $oneNews->id)) }}" class="pull-right more" title="Kliknij, aby przeczytać więcej">więcej...</a></footer>
							<div class="clear"></div>
						</div>
					</article>
				@endforeach
				<div id="newsDetailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 id="myModalLabel">Aktualności</h3>
					</div>
					{{ HTML::image('img/icon-news.png', '', array('class' => 'pull-left clearfix', 'width' => 64, 'height' => 64)) }}
					<div class="ajax-content"></div>
					<button class="btn pull-right" title="Kliknij, aby powrócić">Zamknij</button>
	<div class="clear"></div>
				</div>
				{{$news->links()}}
		</section>
		<section id="last-offers" class="span6">
			<header>
				<h3>Ostatnio dodane oferty</h3>
			</header>
			<div class="clearfix">
			@if(!empty($lastOffers))
				@foreach($lastOffers as $lastOffer)
					<a href="{{ route('offer-details', array($school, $lastOffer->id)) }}" class="offert-item left bg-color">
						<div class="offert-item-content clearfix ">
							<div class="last-offer-title pull-left">
								<h5>
									{{ HTML::image('img/icon-offer.png', '', array('width' => 32, 'height' => '32', 'class' => 'pull-left')) }}
									{{ $lastOffer->name }}
								</h5>
							</div>
						</div>
					</a>
				@endforeach
				@include('subviews.modal-offer')
			@else
				<h4>Ostatnio nie dodano ofert</h4>
			@endif
			</div>
			@include('subviews.sign-info')
		</section>
	</div>
	@endif
	{{-- END News --}}
	<div class="clear"></div>
	
@stop