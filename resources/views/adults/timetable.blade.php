@extends('adults.start')

@section('main-content')
<div id="timetable-wrapper" class="wrapper">
	<div class="row">
		<div class="span4">
			<nav id="offert-menu" class="pull-left">
				<h4 class="color">Kategorie</h4>
				@if($categories)
					@foreach($categories as $category)
						@if($id_category == $category->id)
							<a href="{{route('timetable', array($school, $category->id))}}" class="offert-link active">{{ $category->name }}</a>
						@else
							<a href="{{route('timetable', array($school, $category->id))}}" class="offert-link">{{ $category->name }}</a>
						@endif
					@endforeach
				@else
					<h4 class="text-center color">Brak kategorii</h4>
				@endif
			</nav>
		</div>
		<div class="span8" id="timetable-links">
			@if($timetables->isEmpty())
				<h4 class="text-center color">Brak harmonogramów dla kursów w tej kategorii</h4>
			@else
				<h4 class="text-center color">Dostępne harmonogramy dla kursów w wybranej kategorii</h4>
				<h6 class="text-center">Kliknij na dany harmonogram, aby go pobrać</h6>
				<ul style="list-style-type: none">
					@foreach ($timetables as $timetable)
						<li>
							<a href="{{ route('timetable-download', array($school, $timetable->id)) }}">{{ $timetable->description }}
							<span class="label pull-right">Pobierz</span></a>
						</li>
					@endforeach
				</ul>
			@endif
		</div>
	</div>
</div>

@stop