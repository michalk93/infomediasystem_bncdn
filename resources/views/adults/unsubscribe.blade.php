@extends('adults.start')

@section('main-content')
	
<div class="newsletter-wrapper">
	<div class="thumbnail center well well-small text-center" style="padding: 50px 0;">
		<h2>Newsletter</h2>
		<p>Podaj swój adres e-mail, aby wypisać się z newslettera</p>
		<form action="{{route('newsletter-unsubscribe-post', $school)}}" method="post">
			<div class="input-prepend">
				<span class="add-on"><i class="icon-envelope"></i></span>
				<input type="text" name="email" placeholder="twoj@email.pl">
			</div>
			<div>
				@if(!empty($message))
					{{$message}}
				@endif
			</div>
			<input type="submit" value="Wypisz się" class="btn btn-large" />
		</form>
	</div>
</div>

@stop