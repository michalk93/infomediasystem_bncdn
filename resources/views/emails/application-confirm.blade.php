@extends('emails.template')

@section('title')
	Zatwierdzenie zgłoszenia
@stop

@section('content')
	<p>Witaj, {{ $data->firstname }} {{ $data->lastname }}!</p>
	<p>Twoje zgłoszenie na kurs: <strong>{{ $data->course_name }}</strong> zostało potwierdzone.</p>
	<p><strong>Przed dokonaniem wpłaty, prosimy o kontakt z sekretariatem w celu ustalenia szczegółów.</strong></p>
	<p>
		Dane do przelewu na konto:<br>
		INFO-MEDIA SYSTEM <br>
		34-200 Sucha Beskidzka, ul. Zofii Karaś 9 <br>
		Bank Spółdzielczy o/Sucha Beskidzka <br>
		Nr konta: <strong>14 8128 0005 0002 8613 2000 0010</strong>
	</p>
	<p>Możesz także dokonać wpłaty osobiście w sekretariacie Centrum.</p>
@stop