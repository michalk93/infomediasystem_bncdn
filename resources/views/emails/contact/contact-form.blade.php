<?php date_default_timezone_set('Europe/Warsaw') ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>{{ $subject }}</title>
        <style>
        body * {
            font-size: 14px;
        }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="background-color: #fafafa; font-family: arial, sans-serif; font-size: 14px !important">
        <center>
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
                <tr>
                    <td align="center" valign="top">
                        <!-- // Begin Template Preheader \\ -->
                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">
                            <tr>
                                <td valign="top" class="preheaderContent" >
                                
                                    <!-- // Begin Module: Standard Preheader \ -->
                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                        <tr>
                                            <td valign="top">
                                                <div mc:edit="std_preheader_content" style="color:#707070; font-family:Arial; font-size:10px; text-align:left;">
                                                     Wiadmość z formularza kontaktowego na stronie bncdn.pl
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Module: Standard Preheader \ -->
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \\ -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer" style="background-color: #FFF">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- // Begin Template Header \\ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader" style="border-bottom: 5px solid #505050">
                                        <tr>
                                            <td class="headerContent">
                                            
                                                <!-- // Begin Module: Letterhead, Center Header Image \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <tr>
                                                        <td valign="middle" colspan="3" style="text-align: center">
                                                            <a href="{{ route('home') }}" style="display: block; text-decoration: none; color: #505050; font-weight: bold; text-align: center" title="Beskidzkie Niepubliczne Centrum Doskonalenia Nauczycieli">
                                                                {{ HTML::image('img/logo_email.jpg', 'BNCDN - Beskidzkie Niepubliczne Centrum Doskonalenia Nauczycieli') }}
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Letterhead, Center Header Image \\ -->

                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Header \\ -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- // Begin Template Body \\ -->
                                    <table border="0" cellpadding="10" cellspacing="0" width="600" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContent">
                                            
                                                <!-- // Begin Module: Standard Content \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <div mc:edit="std_content00">
                                                                <h2 class="h2">WIADOMOŚĆ Z FORMULARZA KONTAKTOWEGO</h2>
                                                                <p><strong>Nadawca: </strong>{{$name}} ({{$email}})</p>
                                                                <p><strong>Temat: </strong>{{$subject}}</p>
                                                                <p><strong>Treść:</strong><br>{{$content}}</p>
                                                                <p><em>Wiadomość wysłana dnia {{date('d-m-Y')}} o godzinie {{date('H:i:s')}}</em></p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Standard Content \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Body \\ -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- // Begin Template Footer \\ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter" style="border-top: 3px solid #909090">
                                        <tr>
                                            <td valign="top" class="footerContent">
                                            
                                                <!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="social">
                                                            <div mc:edit="std_social" style="float: right">
                                                                <a  href="http://facebook.com">{{ HTML::image('img/social-fb.png', 'Facebook') }}</a>
                                                                <a href="http://google.pl">{{ HTML::image('img/social-google.png', 'Google+') }}</a>
                                                                <a href="http://twitter.com">{{ HTML::image('img/social-twitter.png', 'Twitter') }}</a>
                                                            </div>
                                                            <div mc:edit="std_footer" style="float: left; color: #707070">
                                                                <em>Copyright &copy; 2013 bncdn.pl, All rights reserved.</em>
                                                                <br>
                                                                Our mailing address is:
                                                                <strong>bncdn@poczta.fm</strong>
                                                                
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="utility">
                                                            <div mc:edit="std_utility" style="color: #707070; text-decoration: none">
                                                                @yield('links')
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Standard Footer \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>