@extends('emails.template')

@section('mail-title')
	Aktualności {!! $created_at  !!}
@stop

@section('content')
	<p><strong>Aktualności {!! $created_at  !!}</strong></p>
	<p>{!!  $content  !!}</p>
@stop

@section('links')
	<em> {!! HTML::linkRoute('newsletter-unsubscribe', 'wypisz się z newslettera', $school->id, array('style' => 'font-size: 12px; text-decoration: none; color: #707070;')) !!} </em>
@stop