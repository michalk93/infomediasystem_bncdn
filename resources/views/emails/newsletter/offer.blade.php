@extends('emails.template')

@section('mail-title')
	Nowa oferta
@stop

@section('content')
	<p>Przedstawiamy nową ofertę: <br><strong>{{ $offer['name'] }}</strong> w kategorii <strong>{{ $category['name'] }}</strong></p>
	<p>Jeżeli jesteś zainteresowany większą ilością szczegółów, odwiedź naszą stronę {{ HTML::linkRoute('school-home', 'bncdn.pl', $school->id) }} aby dokładniej zapoznać się z ofertą.</p>
@stop

@section('links')
	<em> {{HTML::linkRoute('newsletter-unsubscribe', 'wypisz się z newslettera', $school->id, array('style' => 'font-size: 12px; text-decoration: none; color: #707070;'))}} </em>
@stop