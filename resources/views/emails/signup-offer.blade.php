@extends('emails.template')

@section('title')
	Zgłoszenie na kurs
@stop

@section('content')
	<p>Witaj, {{ $inputs['firstname'] }} {{ $inputs['lastname'] }}!</p>
	<p>Twoje zgłoszenie na kurs: <strong>{{ $offer->name }}</strong> w kategorii <strong>{{ $offer->category->name }}</strong>, zostało zarejestrowane pomyślnie.</p>
	
	<p>Po zweryfikowaniu poprawności zgłoszenia, skontaktujemy sie z Tobą drogą mailową lub telefoniczną.</p>
	<p>W załączniku znajduje się Twoje zgłoszenie, wygenerowane automatycznie, w formie pliku PDF, który możesz wydrukować.</p>
@stop