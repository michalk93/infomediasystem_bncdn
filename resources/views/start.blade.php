@extends('templates.main-tpl')

@section('css-styles')
	{{ HTML::style('css/main.css') }}
	{{ HTML::style('css/animate.css') }}
@stop

@section('js-scripts')
	{{ HTML::script('js/jquery.easing.1.3.js', array('type' => 'text/javascript')) }}
	{{ HTML::script('js/jquery.lettering.js', array('type' => 'text/javascript')) }}
	{{ HTML::script('js/jquery.textillate.js', array('type' => 'text/javascript'))}}
@stop

@section('site-title')
	BNC Sucha Beskidzka
@stop

@section('body')
	<div id="wrapper" class="clearfix">
		<header>
			<img src="img/quote-mark.jpg" alt="quotation-mark.jpg" id="left-quote-mark">
			<h1>
				<span class="green-cite">A ten, który posiada</span>
				<span class="darkgray-cite">wiedzę</span>
				<span class="blue-cite">nie będzie narzekał</span>
			</h1>
			<img src="img/quote-mark.jpg" alt="quotation-mark.jpg" id="right-quote-mark">
		</header>
		<section id="menu-links" class="clearfix">
			<a href="{{ route('school-home', 'teachers') }}" class="left" id="menu-teacher-school-link">
				<img src="img/menu-teacher.jpg" alt="menu-teacher.jpg" class="left">
				<span class="left menu-text">
					<span class="title-menu-link">
						<img src="img/teacher-school-icon.png" alt="teacher-school-icon.png" class="left">
						<h2>CENTRUM DOSKONALENIA NAUCZYCIELI</h2>
					</span>
					<span class="more-text">Kliknij, aby dowiedzieć się więcej...</span>
					<span class="description">Wspieramy dyrektorów i nauczycieli w aktywności zawodowej poprzez indywidualne formy pracy na etapach planowania, realizacji i ewaluacji</span>
					<span class="offert-for-text">Oferta dla nauczycieli</span>
				</span>
				<div class="hover-overlay">
					<h2>CENTRUM DOSKONALENIA NAUCZYCIELI</h2>
					<div>
						<img class="animate-arrow" src="img/enter-arrow.png" alt="">
						<img src="img/enter-door.png" alt="">
					</div>
					<h2>Kliknij po szczegółowe informacje</h2>
				</div>
			</a>
			<div class="left"></div>
			<a href="{{ route('school-home', 'adults') }}" class="right" id="menu-adult-school-link">
				<img src="img/menu-students.jpg" alt="menu-students.jpg" class="right">
				<span class="left menu-text">
					<span class="title-menu-link">
						<img src="img/adults-school-icon.png" alt="adults-school-icon.png" class="left">
						<h2>CENTRUM KSZTAŁCENIA DOROSŁYCH</h2>
					</span>
					<span class="more-text">Kliknij, aby dowiedzieć się więcej...</span>
					<span class="description">Wspieramy właścicieli firm, przedsiębiorców oraz indywidualne jednostki we wszystkich obszarach ich aktywności zawodowej</span>
					<span class="offert-for-text">Oferta dla dorosłych</span>
				</span>
				<div class="hover-overlay">
					<h2>CENTRUM KSZTAŁCENIA DOROSŁYCH</h2>
					<div>
						<img class="animate-arrow" src="img/enter-arrow.png" alt="">
						<img src="img/enter-door.png" alt="">
					</div>
					<h2>Kliknij po szczegółowe informacje</h2>
				</div>
			</a>
		</section>
	</div>
	<script type="text/javascript">
		var $IntervalID=null;$('#menu-links a').hover(function(){$(this).children('div.hover-overlay').stop(true,true).fadeTo('<slow></slow>',1,function(){var $object=this;$animate=function(){$($object).find('img.animate-arrow').animate({left:"227px"},1500,function(){$(this).css("left","-142px")});}
$animate();$IntervalID=setInterval($animate,1500);});},function(){clearInterval($IntervalID);$(this).children('div.hover-overlay').fadeTo('medium',0);});$('header h1 span').textillate({loop:true,minDisplayTime:5000,in:{effect:'rotateInDownLeft',sync:true},out:{effect:'rotateOutDownLeft',sync:true}});
	</script>
	<footer class="clearfix">
		<style>
		footer div {
			width: 192px;
			margin: 0 auto;
			padding-top: 20px;
			clear: both;
		}
		footer div a {
			display: block;
			width: 64px;
			height: 64px;
			overflow: hidden;
			z-index: -99;
		}
		footer div a img {
			z-index: 99;
		}
		</style>
		<div class="clearfix">
			<a href="https://www.facebook.com/bncdn" class="left" target="_blank">{{HTML::image('img/social-fb.png', 'Facebook')}}</a>
			<a href="https://plus.google.com/110870634975103617434" rel="publisher" class="left" target="_blank">{{HTML::image('img/social-google.png', 'Google+')}}</a>
			<a href="https://twitter.com/BNCDN" class="left" target="_blank">{{HTML::image('img/social-twitter.png', 'Twitter')}}</a>
		</div>
		<div itemscope itemtype="http://data-vocabulary.org/Organization" style="text-align: center">
			<span itemprop="name">Beskidzkie Niepubliczne Centrum Doskonalenia Nauczycieli</span><br />
			<span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address"><span itemprop="locality">Sucha Beskidzka, </span><span itemprop="street-address">ul. Kościelna 5</span><br />tel. <span itemprop="tel">33 874 45 36</span></span>
		</div>
		<script>
		$('footer div a').hover(function(){
			$(this).css('background-color', '#000').children('img').stop(true, false).animate({'margin-top': '-64px'}, 'slow');
		}, function(){
			$(this).css('background-color', '#FFF').children('img').stop(true, false).animate({'margin-top': '0px'}, 'slow');
		})
		</script>
	</footer> 
@stop