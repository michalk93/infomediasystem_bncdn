<nav id="menu-offer">
    <h4>Kategorie</h4>
    @if($categories)
        @foreach($categories as $category)
            <a href="{{route('offer', array($school->name, $category->id))}}" class="item @if($id_category == $category->id) active @endif">{{ $category->name }}</a>
        @endforeach
        <a href="{{route('timetable', array($school->name))}}" class="item active" style="text-transform: uppercase; margin: 20px 0">Harmonogramy</a>
    @else
        <h4 class="text-center color">Brak kategorii</h4>
    @endif
</nav>