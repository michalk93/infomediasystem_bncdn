@if(session()->has('flash_notification.message'))
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-{{ session('flash_notification.level') }} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{!! session('flash_notification.message') !!}
			</div>
		</div>
	</div>
@endif