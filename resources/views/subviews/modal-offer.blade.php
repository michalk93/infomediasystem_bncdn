<div id="offer-details-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="offer-modal-label" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="offer-modal-label">
			{{ HTML::image('img/icon-offer.png', '', array('width' => 64, 'height' => 64)) }}
			Szczegółowe informacje o ofercie
		</h3>
	</div>
	<div class="ajax-content" >
		{{-- HERE JS/AJAX SCRIPT LOAD INFORMATIONS ABOUT OFFER --}}
	</div>
</div>