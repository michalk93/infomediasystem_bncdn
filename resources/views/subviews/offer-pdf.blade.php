<html>
<head>
	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	 <style>
	 body {
	 	font-family: dejavu sans;
	 	font-size: 12px;
	 }
	 table{
	 	width: 100%;
	 }
	 table tr td {
	 	padding-top: 10px;
	 }
	 table tr td:nth-child(1){
	 	width: 30%;
	 }
	 table tr td:nth-child(2){
	 	font-style: italic;
	 	width: 70%;
	 }
	 table tr td:nth-child(2) p {
	 	border-bottom: 1px dotted;
	 	margin: 0;
	 	padding: 0;
	 }
	 .title {
	 	border-bottom: 2px #ebebeb solid;
	 	padding-left: 20px;
	 	padding-bottom: 2px;
	 	font-weight: normal;
	 	font-size: 14px;
	 	color: #777;
	 	margin-top: 20px;
	 	clear: both;
	 }
	 </style>
</head>
<body style="">
	<div style="text-align: right">
		<span>Sucha Beskidzka, {{ date('d.m.Y') }}</span>
	</div>
	<div style="margin: 50px auto 0px auto">
		<h3 style="text-align: center">ZGŁOSZENIE</h3>
	</div>

	<table>
		<tr>
			<td colspan="2">
				<div class="title">Deklaracja udziału</div>
			</td>
		</tr>
		<tr>
			<td>Deklaruję udział w kursie</td>
			<td><p>{{ $offer->name }}</p></td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="title">Dane personalne</div>
			</td>
		</tr>
		<tr>
			<td>Nazwisko i imiona</td>
			<td><p>{{ ucfirst($data['lastname']) }} {{ ucfirst($data['firstname']) }} {{ ucfirst($data['secondname']) }}</p></td>
		</tr>
		<tr>
			<td>Data i miejsce urodzenia</td>
			<td><p>{{ $data['birthdate'] }} {{ ucwords($data['birthplace']) }}</p></td>
		</tr>
		<tr>
			<td>Adres zamieszkania</td>
			<td><p>{{ ucwords($data['address']) }}</p></td>
		</tr>
		<tr>
			<td>Telefon</td>
			<td><p>{{ $data['phonenumber'] }}</p></td>
		</tr>
		<tr>
			<td>Adres e-mail</td>
			<td><p>{{ $data['email'] }}</p></td>
		</tr>

		<tr>
			<td colspan="2">
				<div class="title">Wykształcenie</div>
			</td>
		</tr>
		<tr>
			<td>Uczelnia</td>
			<td><p>{{ $data['school'] }}</p></td>
		</tr>
		<tr>
			<td>Specjalizacja</td>
			<td><p>{{ ucwords($data['specialization']) }}</p></td>
		</tr>

		<tr>
			<td colspan="2">
				<div class="title">Praca</div>
			</td>
		</tr>
		<tr>
			<td>Miejsce pracy</td>
			<td><p>{{ $data['workplace'] }}</p></td>
		</tr>
		<tr>
			<td>Stanowisko</td>
			<td><p>{{ $data['position'] }}</p></td>
		</tr>
		<tr>
			<td>Przedmiot nauczania</td>
			<td><p>{{ $data['subject'] }}</p></td>
		</tr>
		<tr>
			<td>Stopień awansu zawodowego</td>
			<td><p>{{ $data['degree'] }}</p></td>
		</tr>
		<tr>
			<td>Staż pracy w oświacie</td>
			<td><p>{{ $data['seniority'] }}</p></td>
		</tr>


	</table>
	<div style="padding-top: 20px">
		<em>Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w zgłoszeniu na potrzeby realizacji procesu rekrutacji i realizacji kursu oraz wykonywania zadań określonych prawem realizowanych przez BNCDN, zgodnie z ustawą z dnia 29.08.1997 r. o ochronie danych osobowych (Dz.U. z 1997 nr 133, poz. 883).</em>
	</div>
	<div style="text-align: right; margin-top: 50px;">
		<div>................................................................</div>
		<div><sup>(podpis kandydata)</sup></div>

	</div>
</body>
</html>