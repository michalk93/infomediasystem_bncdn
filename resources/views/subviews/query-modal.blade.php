<div id="modal-query" class="modal hide fade" style="display: block;" aria-hidden="false">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Ankieta - 5 pytań do Ciebie</h3>
  </div>
  <div class="modal-body">
  	{{ Session::get('msgSignUpSuccess', '')}}
    <p>Zechciej poświęcić jeszcze chwilę, na wypełnienie krótkiej (5 pytań) ankiety, dotyczącej funkcjonalności serwisu i zapisu na kursy za pomocą Internetu. Będzimy Ci bardzo wdzięczni za pomoc w ulepszeniu serwisu.</p>
    <p>Ankieta jest anonimowa i ma na celu ulepszenie funkcjonowania serwisu dla Twojej korzyści.</p>
    <hr>
    <form action="{{route('query-form', $school)}}" method="POST">
    	<h4>Pytania dla Ciebie</h4>
    	<p>
    		<p><b>1. Czy korzystajac z serwisu napotykasz jakieś problemy, trudności?</b></p>
    		<label class="radio">
		  		<input type="radio" name="question1" id="question1-yes" value="TAK"> TAK
		  		<p style="display:none">
		  			<label>Jeśli tak, to wypisz je poniżej:</label>
		  			<textarea style="width: 95%; min-height: 100px;" name="question1-text"></textarea>
		  		</p>
			</label>
			<label class="radio">
				<input type="radio" name="question1" id="question1-no" value="NIE"> NIE
			</label>
    	</p>
    	<p>
    		<p><b>2. Czy serwis jest dla Ciebie wystarczająco intuicyjny w obsłudze?</b></p>
    		<label class="radio">
		  		<input type="radio" name="question2" id="question2-yes" value="TAK"> TAK
			</label>
			<label class="radio">
				<input type="radio" name="question2" id="question2-no" value="NIE"> NIE
				<p style="display:none">
		  			<label>Jeśli nie, to napisz dlaczego:</label>
		  			<textarea style="width: 95%; min-height: 100px;" name="question2-text"></textarea>
		  		</p>
			</label>
    	</p>
    	<p>
    		<p><b>3. Czy podoba Ci się sposób zapisu na kurs oraz nowy formularz zgłoszeniowy wraz ze wskazówkami wypełniania?</b></p>
    		<label class="radio">
		  		<input type="radio" name="question3" id="question3-yes" value="TAK"> TAK
			</label>
			<label class="radio">
				<input type="radio" name="question3" id="question3-no" value="NIE"> NIE
				<p style="display:none">
		  			<label>Jeśli nie, to napisz dlaczego:</label>
		  			<textarea style="width: 95%; min-height: 100px;" name="question3-text"></textarea>
		  		</p>
			</label>
    	</p>
    	<p>
    		<p><b>4. Czy podoba Ci się szata graficzna serwisu?</b></p>
    		<label class="radio">
		  		<input type="radio" name="question4" id="question4" value="TAK"> TAK
			</label>
			<label class="radio">
				<input type="radio" name="question4" id="question4" value="NIE"> NIE
				<p style="display:none">
		  			<label>Jeśli nie, to napisz dlaczego:</label>
		  			<textarea style="width: 95%; min-height: 100px;" name="question4-text"></textarea>
		  		</p>
			</label>
    	</p>
    	<p>
    		<p><b>5. Masz jakieś inne uwagi, sugestie, propozycje - śmiało podaj je poniżej:</b></p>
    		
		  	<textarea style="width: 95%; min-height: 100px;" name="question5-text"></textarea>
    	</p>
    
  </div>
  <div class="modal-footer">
    <button type="button" class="btn" data-dismiss="modal" >Nie, dziękuję</button>
    <button type="submit" class="btn btn-success">Wyślij ankietę</button>
  </div>
  </form>
  <script>
  	$(document).ready(function(){
  		$('#question1-yes').on('click', function(){
  			$(this).next('p').slideDown();
  		})
  		$('#question1-no').on('click', function(){
  			$('#question1-yes').next('p').slideUp();
  		})

  		$('#question2-no').on('click', function(){
  			$(this).next('p').slideDown();
  		})
  		$('#question2-yes').on('click', function(){
  			$('#question2-no').next('p').slideUp();
  		})

  		$('#question3-no').on('click', function(){
  			$(this).next('p').slideDown();
  		})
  		$('#question3-yes').on('click', function(){
  			$('#question3-no').next('p').slideUp();
  		});

  		$('#modal-query').modal('show');
  	})
  </script>
</div>