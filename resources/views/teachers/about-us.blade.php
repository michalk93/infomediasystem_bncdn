@extends('teachers.start')

@section('site-title')
	O nas
@stop

@section('description')
	Zapoznaj się z naszą misją i poznaj nasze cele i adresatów. Mamy jasną wizję dalszego rozwoju.
@stop

@section('keywords')
	o bncdn, beskidzkie centrum doskonalenia nauczycieli, misja, wizja, cele, rozwój, adresaci kursów
@stop

@section('main-content')
<div id="view-aboutUs">
	<div class="row">
		<div class="col-sm-3 text-center">
			{{HTML::image('img/about-us-center.jpg', 'about-us', array('width' => '200', 'class' => 'img-responsive'))}}
		</div>
		<div class="col-sm-9">
			<div class="row">
				<div class="col-sm-12">
					<h4>MISJA</h4>

					<div class="media">
						<div><p>Beskidzkie Niepubliczne Centrum Doskonalenia Nauczycieli w&nbsp;Suchej Beskidzkiej jest placówką wspierającą dyrektorów i&nbsp;nauczycieli we wszystkich obszarach ich aktywności zawodowej. Pomagamy dyrektorom i&nbsp;nauczycielom w&nbsp;rozwoju zawodowym poprzez indywidualne formy pracy na etapach planowania, realizacji i&nbsp;ewaluacji.</p>
							<p>Prowadzimy szkolenia dostosowane do potrzeb uczestników pod względem formy, tematu, czasu trwania i&nbsp;miejsca. Wspieramy szkolenia w planowaniu rozwoju i&nbsp;tworzenia programów uwzględniając specyfikę każdej placówki. Szczególnie bliskie są nam działania propagujące twórczą aktywność nauczycieli i&nbsp;rad pedagogicznych.</p>
						</div>

					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-9">
					<h4>WIZJA</h4>
					<div>
						<p>Pragniemy, by placówka oprócz działań dotychczas realizowanych, poszerzała swoja ofertę edukacyjną:</p>
						<ul>
							<li>Kursy nadające dodatkowe kwalifikacje: m.in. kurs pedagogiczny dla nauczycieli praktycznej nauki zawodu</li>
							<li>Kursy doskonalące w zakresie profilaktyki szkolnej</li>
							<li>Seminaria</li>
							<li>Warsztaty metodyczne</li>
							<li>Szkolenia rad pedagogicznych</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3">
					{{HTML::image('img/about-us-right.jpg', 'about-us', array('class' => 'pull-right', 'style' => 'width:100%'))}}
				</div>
			</div>



		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">


		</div>
	</div>
</div>
@stop