
 <div class="modal-body">
	<table class="table table-striped">
		<tr>
			<th>NAZWA</th>
			<td><strong>{{ $offer->name }}</strong></td>
		</tr>
		<tr>
			<th>KATEGORIA</th>
			<td>{{ $offer->category_name }}</td>
		</tr>
		<tr>
			<th>STATUS</th>
			<td>{{ $offer->status_name }}</td>
		</tr>
		<tr>
			<th>LICZBA GODZIN</th>
			<td>{{ $offer->hours }}</td>
		</tr>
		<tr>
			<th>CENA</th>
			<td>{{ $offer->price }} zł</td>
		</tr>
		@if(!empty($offer->recipient))
		<tr>
			<th>ADRESACI</th>
			<td>{{ $offer->recipient }}</td>
		</tr>
		@endif
		@if(!empty($offer->range))
		<tr>
			<th>ZAKRES</th>
			<td>{{ $offer->range }}</td>
		</tr>
		@endif
		@if(!empty($offer->syllabus))
		<tr>
			<th>PROGRAM</th>
			<td>{{ $offer->syllabus }}<td>
		</tr>
		@endif
		@if(!empty($offer->qualifications)
)		<tr>
			<th>KWALIFIKACJE</th>
			<td>{{ $offer->qualifications }}</td>
		</tr>
		@endif
		@if(!empty($offer->additional))
		<tr>
			<th>DODATKOWE INFORMACJE</th>
			<td>{{ $offer->additional }}</td>
		</tr>
		@endif
		@if(!empty($offer->filename))
		<tr>
			<th>ZAŁĄCZNIK/PLIK</th>
			<td> {{ HTML::link(route('offer-download', array($school->name, $offer->filename)), $offer->file_desc.' (pobierz)', array('class' => 'btn')) }}</td>
		</tr>
		@endif
	</table>
</div>
<div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
	<a href="{{ route('offer-signup', array($school->name, $offer->id)) }}" class="btn btn-success">Zapisz się na kurs</a>
</div>