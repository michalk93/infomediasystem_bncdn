@extends('teachers.start')

@section('site-title')
	Kontakt
@endsection

@section('description')
	Skontakuj się z nami telefonicznie, listownie lub mailowo. Możesz także skorzystać z formularza kontaktowego na stronie lub przyjść do nas osobiście.
@endsection

@section('keywords')
	bncdn, beskidzkie centrum doskonalenia nauczycieli, kontakt, telefon, faks, email, formularz kontaktowy, informacje, adres, lokalizacja, mapa
@endsection

@section('main-content')
<div id="view-contact">
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<h3 class="title">Masz pytanie? Jesteśmy do Twojej dyspozycji.</h3>
			<div id="phone-contact" class="contact-item clearfix">
				<img width="64" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDMyLjY2NiAzMi42NjYiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMyLjY2NiAzMi42NjY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8cGF0aCBkPSJNMjguMTg5LDE2LjUwNGgtMS42NjZjMC01LjQzNy00LjQyMi05Ljg1OC05Ljg1Ni05Ljg1OGwtMC4wMDEtMS42NjRDMjMuMDIxLDQuOTc5LDI4LjE4OSwxMC4xNDksMjguMTg5LDE2LjUwNHogICAgTTE2LjY2Niw3Ljg1NkwxNi42NjUsOS41MmMzLjg1MywwLDYuOTgzLDMuMTMzLDYuOTgxLDYuOTgzbDEuNjY2LTAuMDAxQzI1LjMxMiwxMS43MzUsMjEuNDM2LDcuODU2LDE2LjY2Niw3Ljg1NnogTTE2LjMzMywwICAgQzcuMzI2LDAsMCw3LjMyNiwwLDE2LjMzNGMwLDkuMDA2LDcuMzI2LDE2LjMzMiwxNi4zMzMsMTYuMzMyYzAuNTU3LDAsMS4wMDctMC40NSwxLjAwNy0xLjAwNmMwLTAuNTU5LTAuNDUtMS4wMS0xLjAwNy0xLjAxICAgYy03Ljg5NiwwLTE0LjMxOC02LjQyNC0xNC4zMTgtMTQuMzE2YzAtNy44OTYsNi40MjItMTQuMzE5LDE0LjMxOC0xNC4zMTljNy44OTYsMCwxNC4zMTcsNi40MjQsMTQuMzE3LDE0LjMxOSAgIGMwLDMuMjk5LTEuNzU2LDYuNTY4LTQuMjY5LDcuOTU0Yy0wLjkxMywwLjUwMi0xLjkwMywwLjc1MS0yLjk1OSwwLjc2MWMwLjYzNC0wLjM3NywxLjE4My0wLjg4NywxLjU5MS0xLjUyOSAgIGMwLjA4LTAuMTIxLDAuMTg2LTAuMjI4LDAuMjM4LTAuMzU5YzAuMzI4LTAuNzg5LDAuMzU3LTEuNjg0LDAuNTU1LTIuNTE4YzAuMjQzLTEuMDY0LTQuNjU4LTMuMTQzLTUuMDg0LTEuODE0ICAgYy0wLjE1NCwwLjQ5Mi0wLjM5LDIuMDQ4LTAuNjk5LDIuNDU4Yy0wLjI3NSwwLjM2Ni0wLjk1MywwLjE5Mi0xLjM3Ny0wLjE2OGMtMS4xMTctMC45NTItMi4zNjQtMi4zNTEtMy40NTgtMy40NTdsMC4wMDItMC4wMDEgICBjLTAuMDI4LTAuMDI5LTAuMDYyLTAuMDYxLTAuMDkyLTAuMDkyYy0wLjAzMS0wLjAyOS0wLjA2Mi0wLjA2Mi0wLjA5My0wLjA5MnYwLjAwMmMtMS4xMDYtMS4wOTYtMi41MDYtMi4zNC0zLjQ1Ny0zLjQ1OSAgIGMtMC4zNi0wLjQyNC0wLjUzNC0xLjEwMi0wLjE2OC0xLjM3N2MwLjQxLTAuMzExLDEuOTY2LTAuNTQzLDIuNDU4LTAuNjk5YzEuMzI2LTAuNDI0LTAuNzUtNS4zMjgtMS44MTYtNS4wODQgICBjLTAuODMyLDAuMTk1LTEuNzI3LDAuMjI3LTIuNTE2LDAuNTUzYy0wLjEzNCwwLjA1Ny0wLjIzOCwwLjE2LTAuMzU5LDAuMjRjLTIuNzk5LDEuNzc0LTMuMTYsNi4wODItMC40MjgsOS4yOTIgICBjMS4wNDEsMS4yMjgsMi4xMjcsMi40MTYsMy4yNDUsMy41NzZsLTAuMDA2LDAuMDA0YzAuMDMxLDAuMDMxLDAuMDYzLDAuMDYsMC4wOTUsMC4wOWMwLjAzLDAuMDMxLDAuMDU5LDAuMDYyLDAuMDg4LDAuMDk1ICAgbDAuMDA2LTAuMDA2YzEuMTYsMS4xMTgsMi41MzUsMi43NjUsNC43NjksNC4yNTVjNC43MDMsMy4xNDEsOC4zMTIsMi4yNjQsMTAuNDM4LDEuMDk4YzMuNjctMi4wMjEsNS4zMTItNi4zMzgsNS4zMTItOS43MTkgICBDMzIuNjY2LDcuMzI2LDI1LjMzOSwwLDE2LjMzMywweiIgZmlsbD0iIzY2Y2MwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" class="pull-left" />
				<div class="contact-item-text">
					<h4>tel/fax: (33) 874 45 36</h4>
				</div>
			</div>
			<div id="address-contact" class="contact-item clearfix">
				<img width="64" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ4NS40MTEgNDg1LjQxMSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDg1LjQxMSA0ODUuNDExOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCI+CjxnPgoJPHBhdGggZD0iTTAsODEuODI0djMyMS43NjNoNDg1LjQxMVY4MS44MjRIMHogTTI0Mi43MDgsMjgwLjUyNkw0My42MTIsMTA1LjY5MWgzOTguMTg3TDI0Mi43MDgsMjgwLjUyNnogICAgTTE2My4zOTcsMjQyLjY0OUwyMy44NjcsMzY1LjE3OFYxMjAuMTE5TDE2My4zOTcsMjQyLjY0OXogTTE4MS40ODIsMjU4LjUzM2w2MS4yMiw1My43NjJsNjEuMjItNTMuNzYyTDQ0MS45MjQsMzc5LjcySDQzLjQ4NyAgIEwxODEuNDgyLDI1OC41MzN6IE0zMjIuMDA4LDI0Mi42NTVsMTM5LjUzNS0xMjIuNTM2djI0NS4wNTlMMzIyLjAwOCwyNDIuNjU1eiIgZmlsbD0iIzY2Y2MwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" class="pull-left" />
				<div class="contact-item-text">
					<h4>INFO-MEDIA SYSTEM</h4>
					<span>Sucha Beskidzka, ul. Kościelna 5</span><br>
					<span>34-200 Sucha Beskidzka</span><br>
					<span>(budynek ZS im. W. Goetla - pokój 109)</span>
				</div>
			</div>
			<div id="email-contact" class="contact-item clearfix">
				<img width="64" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDIzNS4wNjUgMjM1LjA2NSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMjM1LjA2NSAyMzUuMDY1OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTE1Ni4wMTksNTguODMyaDE4LjAyNWwtMTIuMzU4LDY5Ljc3MmMtMS4zNzYsOC4wNjYtMS40MiwxMy45MDgtMC4xMzEsMTcuNTE0ICAgIGMxLjI4OSwzLjU5NSw0LjE2Niw1LjQwMSw4LjYyNiw1LjQwMXM4LjgzOC0xLjAyOCwxMy4xMy0zLjA4OWM0LjI5MS0yLjA2MSw4LjE0OC01LjIzMiwxMS41ODUtOS41MjRzNi4yMjItOS42OTIsOC4zNzEtMTYuMjE0ICAgIGMyLjE0My02LjUyMSwzLjIyLTE0LjI0NSwzLjIyLTIzLjE3NmMwLTEyLjg3NC0yLjE1NC0yMy44NjEtNi40NC0zMi45NTVjLTQuMjkxLTkuMDk0LTEwLjE3MS0xNi42MTEtMTcuNjMzLTIyLjUyOSAgICBjLTcuNDY4LTUuOTI5LTE2LjI2OC0xMC4yNTgtMjYuMzk2LTEzLjAwNXMtMjEuMDIyLTQuMTI4LTMyLjctNC4xMjhjLTEzLjM4NiwwLTI1LjgzLDIuNDA0LTM3LjMyOCw3LjIxMiAgICBjLTExLjQ5OCw0LjgxNC0yMS40OTUsMTEuNDIyLTI5Ljk5NiwxOS44MjVjLTguNDk2LDguNDE0LTE1LjE0OCwxOC40MTEtMTkuOTUsMzAuMDAyYy00LjgwOCwxMS41OC03LjIwNywyNC4xNDktNy4yMDcsMzcuNzE0ICAgIGMwLDEzLjM5MSwyLjAxMiwyNS40LDYuMDQzLDM2LjA0NWM0LjAzNiwxMC42NSw5LjkxNSwxOS42OTUsMTcuNjM5LDI3LjE2OGM3LjcyMyw3LjQ2MiwxNy4yOTEsMTMuMjEyLDI4LjcwMiwxNy4yNDIgICAgYzExLjQxMSw0LjAzLDI0LjUwMyw2LjA0OCwzOS4yNyw2LjA0OGM0Ljk3MSwwLDEwLjYzMy0wLjU5OCwxNi45ODYtMS43OTVjNi4zNTMtMS4yMDcsMTIuMTAyLTMuMDA4LDE3LjI1My01LjQxMmw3Ljk3OSwyNC43MjEgICAgYy03LjAzOCwzLjQzMi0xNC4yMDEsNS44MzEtMjEuNDk1LDcuMjAxYy03LjI5OSwxLjM3Ni0xNS42NjUsMi4wNjctMjUuMTAxLDIuMDY3Yy0xNi42NDksMC0zMi4wNTgtMi40MS00Ni4yMTYtNy4yMDcgICAgYy0xNC4xNjMtNC44MTQtMjYuNDI4LTExLjkzMy0zNi44MTItMjEuMzdjLTEwLjM4OS05LjQ0Mi0xOC40OTgtMjEuMTU4LTI0LjM0LTM1LjE0N0MyLjkxNSwxNTcuMjMsMCwxNDEuMDQ5LDAsMTIyLjY4NiAgICBjMC0xOC43MSwzLjM0NS0zNS42MiwxMC4wNDYtNTAuNzI1YzYuNjktMTUuMDkzLDE1LjcwMy0yNy45NzMsMjcuMDMyLTM4LjYyM2MxMS4zMy0xMC42MzksMjQuNDU5LTE4LjgzLDM5LjM5LTI0LjU5ICAgIGMxNC45My01Ljc0NCwzMC43Mi04LjYyMSw0Ny4zNjktOC42MjFjMTUuNzksMCwzMC40NywyLjI3OSw0NC4wMjksNi44MjFjMTMuNTU0LDQuNTUzLDI1LjMxMywxMS4wNjgsMzUuMjY3LDE5LjU3ICAgIGM5Ljk1NCw4LjQ5NiwxNy43NjksMTguODg0LDIzLjQzNywzMS4xNmM1LjY2MiwxMi4yNjUsOC40OTYsMjYuMjE2LDguNDk2LDQxLjgzMmMwLDEwLjk4Ny0xLjkyNSwyMS4yNzgtNS43OTMsMzAuODk5ICAgIGMtMy44NjIsOS42MTYtOS4xODEsMTcuOTM4LTE1Ljk1OCwyNC45NzZjLTYuNzg4LDcuMDMzLTE0LjY4LDEyLjYwOC0yMy42ODcsMTYuNzMxYy05LjAwNyw0LjExNy0xOC42NjcsNi4xNzktMjguOTYzLDYuMTc5ICAgIGMtNC4yOTcsMC04LjI3OC0wLjQ2OC0xMS45NzctMS40MTRjLTMuNjg4LTAuOTQ2LTYuNzc3LTIuNTM1LTkuMjYzLTQuNzY1Yy0yLjQ5MS0yLjIzNS00LjMzNS01LjE1MS01LjUzNy04Ljc1NyAgICBjLTEuMjAyLTMuNjAxLTEuNTUtOC4wNjEtMS4wMjgtMTMuMzg2aC0xLjAyOGMtMi41NzgsMy42MDYtNS4zNjMsNy4xMjUtOC4zNjUsMTAuNTUyYy0zLjAwOCwzLjQzOC02LjMwOSw2LjQ4My05LjkxNSw5LjE0MyAgICBjLTMuNjAxLDIuNjY1LTcuNTU1LDQuNzY1LTExLjg0MSw2LjMwOWMtNC4yOTcsMS41MzktOS4wMTMsMi4zMTctMTQuMTYzLDIuMzE3Yy00LjExNywwLTguMDY2LTAuOTAzLTExLjg0MS0yLjY5OCAgICBjLTMuNzgtMS44MDYtNy4wNDQtNC4zNC05Ljc4NS03LjU5OGMtMi43NTItMy4yNTgtNC45MzktNy4yMDctNi41NjUtMTEuODQ2Yy0xLjYzNy00LjYzNC0yLjQ0OC05Ljc4NS0yLjQ0OC0xNS40NDcgICAgYzAtMTAuNjM5LDEuNzEzLTIwLjk4NCw1LjE0NS0zMS4wMjRjMy40MzItMTAuMDQ2LDguMTE1LTE4LjkyMywxNC4wMzMtMjYuNjQ2czEyLjc4Ny0xMy45NCwyMC41OTgtMTguNjY3ICAgIGM3LjgxMS00LjcxNiwxNi4wOTQtNy4wNzYsMjQuODQtNy4wNzZjNi4wMDUsMCwxMS4wNzQsMC44OTcsMTUuMTkxLDIuNjk4YzQuMTIzLDEuODA2LDcuODkyLDQuMTY2LDExLjMzLDcuMDg3TDE1Ni4wMTksNTguODMyeiAgICAgTTEzOS43OTUsODkuOTkyYy0yLjIzLTEuODkzLTQuNTQ3LTMuMzUtNi45NDYtNC4zNzhjLTIuNDA0LTEuMDMzLTUuNDA2LTEuNTQ1LTkuMDEzLTEuNTQ1Yy01LjE0NSwwLTkuOTEsMS40NTgtMTQuMjgzLDQuMzc4ICAgIGMtNC4zNzgsMi45MTUtOC4xNTksNi42ODUtMTEuMzMsMTEuMzI0Yy0zLjE4Miw0LjYzNC01LjYyNCw5LjgzNC03LjM0MywxNS41NzJjLTEuNzEzLDUuNzYtMi41NzgsMTEuMzc5LTIuNTc4LDE2Ljg2NyAgICBjMCw1LjY2NywxLjE1OSwxMC4zMDcsMy40ODEsMTMuOTEzYzIuMzE3LDMuNTk1LDYuMzA0LDUuNDAxLDExLjk2Niw1LjQwMWMyLjQwNCwwLDQuOTgyLTAuNzI5LDcuNzIzLTIuMTgxICAgIGMyLjc1Mi0xLjQ2Myw1LjQwNi0zLjM5NCw3Ljk5LTUuNzk4YzIuNTczLTIuMzk5LDUuMDU4LTUuMTUxLDcuNDYyLTguMjM1YzIuNDA0LTMuMDk1LDQuNTQ3LTYuMzUzLDYuNDQtOS43ODVMMTM5Ljc5NSw4OS45OTJ6IiBmaWxsPSIjNjZjYzAwIi8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" class="pull-left" />
				<div class="contact-item-text">
					<h4><a href="mailto:bncdn@poczta.fm">bncdn@poczta.fm</a></h4>
					<p>lub skorzystaj z formularza kontaktowego na tej stronie</p>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<h3 class="title">Formularz kontaktowy</h3>
			<div id="email-form">
				{!! Form::open(['route' => ['contact-email-send', $school->name]]) !!}
					<div>
						<label for="name">Imię i nazwisko:</label>
						<input type="text" id="name" name="name" placeholder="Wpisz swoje imię i nazwisko" value="{{old('name')}}" required>
						<div class="text-danger">{!! MyValidator::printMessage($errors, 'name') !!}</div>
					</div>
					<div>
						<label for="email" class="left">Twój e-mail:</label>
						<input type="email" id="email" name="email" placeholder="Wpisz swój adres email" value="{{old('email')}}" required>
						<div class="text-danger">{!! MyValidator::printMessage($errors, 'email') !!}</div>
					</div>
					<div>
						<label for="subject">Temat:</label>
						<input type="text" id="subject" name="subject" placeholder="Wpisz temat wiadomości" value="{{old('subject')}}" required>
						<div class="text-danger">{!! MyValidator::printMessage($errors, 'subject') !!}</div>
					</div>
					<div>
						<label for="content">Treść:</label>
						<textarea name="content" id="content" placeholder="Wpisz treść wiadomości" required>{{old('content')}}</textarea>
						<div class="text-danger">{!! MyValidator::printMessage($errors, 'content') !!}</div>
					</div>

					{!! Recaptcha::render() !!}
					<div class="text-danger">{!! MyValidator::printMessage($errors, 'g-recaptcha-response') !!}</div>
					<div>
						<input type="submit" class="btn btn-primary" value="WYŚLIJ WIADOMOŚĆ">
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h3>Zobacz na mapie, gdzie jesteśmy</h3>
			<div class="thumbnail">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7811.401188098687!2d19.592943856239543!3d49.739283385761006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47167ebbdb7be0cd%3A0xedeb2bf596adca4b!2sBeskidzkie+Niepubliczne+Centrum+Doskonalenia+Nauczycieli!5e0!3m2!1spl!2spl!4v1469280191137" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

@endsection