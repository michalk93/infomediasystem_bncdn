@extends('templates.school-tpl')

@section('css-styles')
	@parent
 	{{ HTML::style('css/teachers/teachers-school.css') }}
 	{{ HTML::style('css/bootstrap/bootstrap.css') }}
@stop

@section('js-scripts')
	@parent
	{{ HTML::script('js/bootstrap/bootstrap.min.js', array('type' => 'text/javascript')) }}
@stop

@section('site-title')
BNC - Nauczyciele
@stop

@section('title-header')
<span>BESKIDZKIE NIEPUBLICZNE CENTRUM DOSKONALENIA NAUCZYCIELI</span>
@stop


{{------------------------------------}}
{{--     MAIN CONTENT SECTION       --}}
{{------------------------------------}}
@section('main-content')
	{{-- SLIDER --}}
	@if(!empty($slides))
		<div id="slider-wrapper" class="wrapper">
			<div id="homeCarousel" class="carousel slide">
				<ol class="carousel-indicators">
				@foreach($slides as $slide)
					<li data-target="#myCarousel" data-slide-to="{{ $slide->id }}"></li>
				@endforeach
				</ol>
				<!-- Carousel items -->
				<div class="carousel-inner">
				@foreach($slides as $slide)
					<div class="item text-center">
						<a href="{{$slide->link}}">
							{{ HTML::image(Slider::$uploadPath.$school->name.'/'.$slide->filename, $slide->filename, array('height' => Slider::SLIDE_HEIGHT, 'width' => Slider::SLIDE_WIDTH)) }}
						</a>
					@if(!empty($slide->description))
					<a href="{{$slide->link}}">
	               		<div class="carousel-caption">
	                  		{{ $slide->description }}
	                	</div>
	                </a>
	                @endif
	                </div>
	            @endforeach

				</div>
				<!-- Carousel nav -->
				<a class="carousel-control left" href="#homeCarousel" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#homeCarousel" data-slide="next">&rsaquo;</a>
			</div>
		</div>
		{{HTML::image('img/carousel-shadow.png', '')}}
	@endif

	<h3 class="text-center color">Ups! Niestety nie ma strony o podanym adresie ;(</h3>
	
@stop