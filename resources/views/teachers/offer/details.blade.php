@extends('teachers.start')

@section('site-title')
	{{ $offer->name }}
@stop

@section('description')
	Szczegóły dotyczące kursu "{{ strtolower($offer->name) }}" w kategorii "{{strtolower($offer->category_name)}}"
@stop

@section('keywords')
	bncdn, beskidzkie centrum doskonalenia nauczycieli, kursy, szkolenia,
	{{ strtolower($offer->name) }}, {{strtolower($offer->category_name)}}, {{ $offer->hours }} godzin,
@stop

@section('main-content')
<div id="view-offerDetails" class="row">
	<div class="col-sm-12 col-md-3">
		@include('subviews.categories', ['id_category' => $offer->category_id])
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-1">
		<section id="viewport-offerDetails">
			@if($offer->status_name != "Rekrutacja zakończona")
				<p class="text-center"><a href="{{ route('offer-signup', array($school->name, $offer->id)) }}" class="btn btn-primary">Zapisz się na kurs przez Internet</a></p>
			@endif
			<table class="table table-striped" itemscope itemtype="http://data-vocabulary.org/Product" itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
				<tr>
					<th>NAZWA</th>
					<td><strong itemprop="name">{!! $offer->name !!}</strong></td>
				</tr>
				<tr>
					<th>KATEGORIA</th>
					<td itemprop="category">{!! $offer->category_name !!}</td>
				</tr>
				<tr>
					<th>STATUS</th>
					<td>
						{!!  $offer->status_name  !!}
						@if($offer->date_at)
							: {!! $offer->date_at  !!}
						@endif
					</td>
				</tr>
				<tr>
					<th>LICZBA GODZIN</th>
					<td>{!! $offer->hours !!}</td>
				</tr>
				<tr>
					<th>CENA</th>
					<meta itemprop="currency" content="PLN" />
					<td itemprop="price">{!! $offer->price !!}</td>
				</tr>
				@if(!empty($offer->recipient))
					<tr>
						<th>ADRESACI</th>
						<td>{!! $offer->recipient !!}</td>
					</tr>
				@endif
				@if(!empty($offer->range))
					<tr>
						<th>ZAKRES</th>
						<td itemprop="description">{!! $offer->range !!}</td>
					</tr>
				@endif
				@if(!empty($offer->syllabus))
					<tr>
						<th>PROGRAM</th>
						<td>{!! $offer->syllabus !!}</td>
					</tr>
				@endif
				@if(!empty($offer->qualifications)
        )		<tr>
					<th>KWALIFIKACJE</th>
					<td>{!! $offer->qualifications !!}</td>
				</tr>
				@endif
				@if(!empty($offer->additional))
					<tr>
						<th>DODATKOWE INFORMACJE</th>
						<td>{!! $offer->additional !!}</td>
					</tr>
				@endif
				@if(!empty($offer->filename))
					<tr>
						<th>ZAŁĄCZNIK/PLIK</th>
						<td> {!!  HTML::link('uploaded_files/'.$school->name.'/'.$offer->filename, $offer->file_desc.' (pobierz)', array('class' => 'btn')) !!}</td>
					</tr>
				@endif
			</table>
			@if($offer->status_name != "Rekrutacja zakończona")
				<div class="text-center">
					<a href="{{ route('offer-signup', array($school->name, $offer->id)) }}" class="btn btn-primary btn-large">Zapisz się na kurs przez Internet</a>
				</div>
			@endif
		</section>
		@include('subviews.sign-info')
	</div>

	</div>
</div>

@stop