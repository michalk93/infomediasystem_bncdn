@extends('teachers.start')

@section('site-title')
	Oferta kursów i szkoleń
@stop

@section('description')
	Centrum Doskonalenia Nauczycieli posiada bogatą ofertę kursów i szkoelń. Oferujemy m.in. kursy w zakresie studiów podyplomowych, wspomagania nauczyciela, technologii informacyjnych, edukacji elementarnej czy szkolenia BHP.
@stop

@section('keywords')
	bncdn, beskidzkie centrum doskonalenia nauczycieli, kursy kwalifikacyjne, kursy sucha beskidzka, szkolenia bncdn, oferta bncdn, kursy bncdn,
	{{strtolower($categories->implode('name', ','))}}
@stop


@section('main-content')
	@if(Session::has('msgSignUpSuccess'))
		@include('subviews.query-modal')
	@endif
	{{ Session::get('msgSignUpSuccess', '')}}
	{{ Session::forget('msgSignUpSuccess') }}
<div id="view-offer" class="row">
	<div class="col-sm-12 col-md-3">

		@include('subviews.categories')
	</div>
	<div class="col-sm-12 col-md-8 col-md-offset-1">
		<section id="list-offer">
			<div>
				@if($offer)
					@foreach ($offer as $oneOffer)

							<div class="row item">
								<div class="col-sm-2 text-center">
									<p>{{HTML::image('img/offert-item-icon.png', "offert-item")}}</p>
									<p>{{ $oneOffer->hours }} {{\App\Helpers\ViewHelpers::getHoursPhrase($oneOffer->hours)}}</p>
								</div>
								<div class="col-sm-8">
									<h5>{!! $oneOffer->name !!}</h5>
									<p>
										{!! $oneOffer->status->name !!}
										@if($oneOffer->date_at)
											: {{ $oneOffer->date_at }}
										@endif
									</p>
								</div>
								<div class="col-sm-2 text-center">
									<p><a href="{{ URL::route('offer-details', array($school->name, $oneOffer->id)) }}" class="btn btn-default">Szczegóły</a></p>
									@if($oneOffer->status->name != "Rekrutacja zakończona")
										<a href="{{ URL::route('offer-signup', array($school->name, $oneOffer->id)) }}" class="btn btn-primary btn-sm">Zapisz się</a>
									@endif

								</div>
							</div>
					@endforeach
				@else
					<h4 class="text-center color">Brak ofert</h4>
				@endif
			</div>
			@if($offer)
				<footer class="text-center"> {{ $offer->render() }} </footer>
			@endif
		</section>
		@include('subviews.sign-info')
	</div>
</div>
@stop