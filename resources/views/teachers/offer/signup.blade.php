@extends('teachers.start')

@section('site-title')
	Zgłoszenie na kurs: {!! $offer->name !!}
@stop

@section('description')
	Wypełnij formularz zgłoszeniowy na kurs "{!! strtolower($offer->name) !!}" przez Internet, a my skontaktujemy się z Tobą.
@stop

@section('keywords')
	bncdn, beskidzkie centrum doskonalenia nauczycieli, kursy kwalifikacyjne, kursy sucha beskidzka, szkolenia bncdn, oferta bncdn, kursy bncdn, zgłoszenie bncdn, formularz bncdn, zgłoszenie na kurs {!! strtolower($offer->name) !!}, formularz {!! strtolower($offer->name) !!}, formularz zgłoszeniowy, zapisy na kurs {!! strtolower($offer->name) !!}
@stop

@section('main-content')
<div id="view-offerSignUp" class="row">
	<div class="col-sm-11 col-sm-offset-1">

	{!! Form::open(['route' => ['offer-signup-post', $school->name, $offer->id], 'id' => 'form-signup', 'method' => 'post']) !!}
	 <div class="row">
		 <div class="col-sm-12">
			 <h3>Formularz zgłoszeniowy na kurs</h3>
			 <h4>{!! $offer->category->name !!}: {!!  $offer->name  !!}</h4>
		 </div>
	 </div>
	 @if($errors->any())
	 <div class="row">
		 <div class="col-sm-12">
			 <p class="alert alert-danger">
				 W formularzu wystąpiły błędy.<br/>Popraw je we wskazanych miejscach i wyślij formularz ponownie.
			 </p>
		 </div>
	 </div>
	 @endif
	 <div class="row">
		 <div class="col-sm-12">
			 <h4>1. Dane personalne</h4>
		 </div>
	 </div>
	 <div class="row">
		 <div class="col-sm-12 col-md-6">
			  {{--Lastname --}}
			 <label  for="lastname">Nazwisko:</label>
			 <input id="lastname" name="lastname" type="text" autofocus="autofocus" value="{!!old('lastname')!!}">
			 <div class="text-danger">{!!MyValidator::printMessage($errors, 'lastname')!!}</div>
		 </div>
		 <div class="col-sm-12 col-md-6">
			 <p class="hint active">Wpisz swoje nazwisko</p>
		 </div>
	 </div>

	{{-- Firstname --}}

	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="firstname">Pierwsze imię:</label>
			<input id="firstname" name="firstname" type="text" value="{!!old('firstname')!!}">
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'firstname')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Wpisz swoje imię</p>
		</div>
	</div>

	{{-- Secondname --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="secondname">Drugie imię:</label>
			<input id="secondname" name="secondname" type="text" value="{!!old('secondname')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'secondname')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Jeżeli posiadasz drugie imię wpisz je w tym polu lub pozostaw to pole puste</p>
		</div>
	</div>

	{{-- Date of birth --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="birthdate">Data urodzenia:</label>
			<input id="birthdate" name="birthdate" type="text" value="{!!old('birthdate')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'birthdate')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Wpisz datę urodzenia w formacie: RRRR-MM-DD, np. 1976-05-12. Możesz też wybrać odpowiednią datę korzystając z wyświetlonego kalendarza, wybierając najpierw rok, potem mieisąc, a na końcu dzień swoich urodzin</p>
		</div>
	</div>

	{{-- Place of birth --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="birthplace">Miejsce urodzenia:</label>
			<input id="birthplace" name="birthplace" type="text" value="{!!old('birthplace')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'birthplace')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Wpisz nazwę miejscowości, w której się urodziłeś</p>
		</div>
	</div>

	{{-- Address --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="address">Adres:</label>
			<input id="address" name="address" type="text" value="{!!old('address')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'address')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Podaj swój adres wraz z numerem domu/lokalu, ulicą/osiedlem, kodem pocztowym i pocztą, np. Warszawa ul. Wolności 17, 00-999 Warszawa</p>
		</div>
	</div>

	 {{-- E-mail address --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="email">Adres e-mail:</label>
			<input id="email" name="email" type="text" value="{!!old('email')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'email')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Wpisz swój adres e-mail, który posłuży nam do dalszego kontaktu z Tobą</p>
		</div>
	</div>

	{{-- Phone number --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="phonenumber">Telefon kontaktowy:</label>
			<input id="phonenumber" name="phonenumber" type="text" value="{!!old('phonenumber')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'phonenumber')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Podaj numer telefonu, pod którym możemy się z Tobą skontaktować</p>
		</div>
	</div>



	 <div class="row">
		 <div class="col-sm-12">
			 <h4>2. Wykształcenie</h4>
		 </div>
	 </div>
	{{-- Name of school --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="school">Uczelnia:</label>
			<input id="school" name="school" type="text" autofocus="autofocus" value="{!!old('school')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'school')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Wpisz nazwę uczelni, którą ukończyłeś. Jeżli jest ich więcej niż jedna, rozdziel je znakiem ukośnika: '/'</p>
		</div>
	</div>

	{{-- Specialization --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="specialization">Specjalizacja:</label>
			<input id="specialization" name="specialization" type="text" value="{!!old('specialization')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'specialization')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Podaj nazwę specjalizacji, z którą ukończyłeś uczelnię. Jeżeli jest ich więcej niż jedna, rozdziel je znakiem ukośnika: '/'</p>
		</div>
	</div>


	 <div class="row">
		 <div class="col-sm-12">
			 <h4>3. Sytuacja zawodowa</h4>
		 </div>
	 </div>

	 {{-- Place of work --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="workplace">Miejsce pracy:</label>
			<input id="workplace" name="workplace" type="text" autofocus="autofocus" value="{!!old('workplace')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'workplace')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Wpisz nazwę placówki, w której obecnie pracujesz</p>
		</div>
	</div>

	 {{-- Position --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="position">Stanowisko:</label>
			<input id="position" name="position" type="text" value="{!!old('position')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'position')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Podaj nazwę stanowiska, na którym obecnie pracujesz</p>
		</div>
	</div>

	 {{-- Teaching subject --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="subject">Przedmiot nauczania:</label>
			<input id="subject" name="subject" type="text" value="{!!old('subject')!!}" />
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'subject')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Wpisz nazwę przedmiotu, którego nauczasz</p>
		</div>
	</div>

	 {{-- Degree of advancement --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="degree">Stopień awansu zawodowego:</label>
			<input id="degree" name="degree" type="text" value="{!!old('degree')!!}">
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'degree')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Podaj stopień swojego awansu zawodowego, np. nauczyciel mianowany</p>
		</div>
	</div>

	 {{-- Seniority --}}
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<label for="seniority">Staż pracy w oświacie:</label>
			<input id="seniority" name="seniority" type="text" value="{!!old('seniority')!!}">
			<div class="text-danger">{!!MyValidator::printMessage($errors, 'seniority')!!}</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<p class="hint">Wpisz swój staż pracy w oświacie, np. 10 lat</p>
		</div>
	</div>

	 {{-- Agreement to processing personal data --}}
	<div class="row">
		<div class="col-sm-12 col-md-1">
			<input name="agreement" type="checkbox" data-size="small" data-off-text="Nie" data-on-text="Tak" data-on-color="primary">
		</div>
		<div class="col-sm-12 col-md-11">
			<em>Wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w zgłoszeniu na potrzeby realizacji procesu rekrutacji i realizacji kursu oraz wykonywania zadań określonych prawem realizowanych przez BNCDN, zgodnie z ustawą z dnia 29.08.1997 r. o ochronie danych osobowych (Dz.U. z 1997 nr 133, poz. 883).</em>

			<div class="text-danger">{!!MyValidator::printMessage($errors, 'agreement')!!}</div>
		</div>
	</div>


	 {{-- Agreement to receiving newsletter --}}
	 <div class="row">
		 <div class="col-sm-12 col-md-1">
			 <input name="newsletter" type="checkbox" data-size="small" data-off-text="Nie" data-on-text="Tak" data-on-color="primary">
		 </div>
		 <div class="col-sm-12 col-md-11">
			 <em>Chcę otrzymywać informacje o nowych ofertach na adres e-mail podany podczas rejestracji</em>
		 </div>
	 </div>
		<div class="row">
			<div class="col-xs-12">
				{!! Recaptcha::render() !!}
				<div class="text-danger">{!!MyValidator::printMessage($errors, 'g-recaptcha-response')!!}</div>
			</div>
		</div>
	 <div class="row">
		 <div class="col-sm-12">
			 <a href="{!! route('offer', $school->name) !!}" class="btn btn-default">Rezygnuj</a>
			 <input type="reset" value="Wyczyść formularz" class="btn">
			 <button type="submit" class="btn btn-primary" data-trigger="hover" data-title="Wyślij zgłoszenie" data-content="Jeżeli wypełniłeś już cały formularz przechodząc przez 3 etapy wypełaniania danych, wyślij go do nas i złóż zgłoszenie klikając ten przycisk">Złóż zgłoszenie</button>
		 </div>
	 </div>

	{!!Form::close()!!}
	</div>
</div>

@stop

@section('css-styles')
	@parent
	{!!HTML::style('css/bootstrap/datepicker.css')!!}
	{!!HTML::style('css/bootstrap/bootstrap-switch.min.css')!!}
@stop

@section('js-scripts')
	@parent
	{!!HTML::script('js/bootstrap/bootstrap-datepicker.js')!!}
	{!!HTML::script('js/bootstrap/locales/bootstrap-datepicker-pl.js')!!}
	{!!HTML::script('js/bootstrap/bootstrap-switch.min.js')!!}
	<script type="text/javascript">
		$(document).ready(function(){
			$('input#birthdate').datepicker({
				format: 'yyyy-mm-dd',
				language: 'pl',
				startView: 2,
				autoclose: true,
			});
			$('input[type="checkbox"]').bootstrapSwitch();

			$('input[type="text"]').on('focus', function(){
				$('p.hint.active').hide()
				$(this).parent().next().children('p.hint').addClass('active').fadeIn('medium');
			})
		})


	</script>
@stop