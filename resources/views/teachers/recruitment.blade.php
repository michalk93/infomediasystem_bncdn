@extends('teachers.start')

@section('site-title')
	Rekrutacja i opłaty
@stop

@section('description')
	Zapoznaj się z procesem rekrutacja na nasze kursy i szkolenia oraz zasadami opłat.
@stop

@section('keywords')
	bncdn, beskidzkie centrum doskonalenia nauczycieli, rekrutacja, opłaty, dokumenty, numer konta, wymagania
@stop

@section('main-content')
<div id="view-recruitment">
	<div class="row">
		<div class="col-xs-12 text-center">
			{{HTML::image('img/recruitment.jpg', 'Rekrutacja', ['class' => 'img-responsive', 'style' => 'width: 100%'])}}
		</div>
	</div>
	<div class="row">
		<section class="col-sm-12 col-md-4">
			<div class="content">
				<h4 class="color">Ogólne informacje</h4>
				<p>Na kurs można zapisać się telefonicznie, dzwoniąc pod numer (33)&nbsp;874&nbsp;45&nbsp;36 lub przez Internet, wybierając odpowiedni kurs i wypełniając formualrz zgłoszeniowy na naszej stronie.<br/>Można także zapisać się bezpośrednio w sekretariacie Centrum.</p>
				<p>Kursy rozpoczynamy w momencie zgłoszenia sie wystarczającej ilości chętnych.</p>
			</div>
		</section>
		<section class="col-sm-12 col-md-4">
			<div class="content">
				<h4 class="color">Wymagania</h4>
				<p>
					Dokumenty, które należy złożyć w sekretariacie Centrum przed pierwszymi zajęciami:
				</p>
				<ol>
					<li>Dyplom ukończenia studiów wyższych lub ksero dyplomu</li>
					<li>Kserokopia dowodu osobistego</li>
					<li>Kwestionariusz osobowy (druk dostępny w sekretariacie)</li>
					<li>Podanie o przyjęcie na studia (druk dostępny w sekretariacie)</li>
				</ol>
				
			</div>
		</section>
		<section class="col-sm-12 col-md-4">
			<div class="content">
				<h4 class="color">Opłaty</h4>
				<p>Wpłaty za kursy prosimy kierować na konto lub wpłacać bezpośrednio w sekretariacie Centrum.</p>
				<div>
					<p>Dane do przelewu:</p>
					<p><strong>INFO-MEDIA SYSTEM<br/>
					34-200 Sucha Beskidzka, ul. Zofii Karaś 9 <br/>
					Bank Spółdzielczy o/Sucha Beskidzka</strong></p>
				</div>
				<div>
					<p>Nr konta:</p>
					<p><strong>14 8128 0005 0002 8613 2000 0010</strong></p>
				</div>
			</div>
		</section>
	</div>
</div>
@stop