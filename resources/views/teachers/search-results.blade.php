@extends('teachers.start')

@section('main-content')
<div id="search-results-wrapper" class="row">
	<div class="span12">
		<hr>
	@if(isset($error))
		<div class="alert alert-error">{{ $error }}</div>
	@else
		@if(empty($results['offer']))
			<h3 class="well">Brak rezultatów w ofertach</h3>
		@else
			<h3 class="well">Znaleziono w ofertch - {{ count($results['offer']) }}</h3>
			@foreach($results['offer'] as $offer_result)
				<a href="{{route('offer-details', [$school->name, $offer_result->id])}}"><p>{!! $offer_result->name !!}</p></a>
			@endforeach
		@endif
		<hr>
		@if(empty($results['news']))
			<h3 class="well">Brak rezultatów w aktualnościach</h3>
		@else
			<h3 class="well">Znaleziono w aktualnościach - {{ count($results['news']) }}</h3>
			@foreach($results['news'] as $news_result)
				<a href="{{route('school-home', $school->name)}}"><p>{!! Illuminate\Support\Str::words($news_result->content, 10)  !!}</p></a>
			@endforeach
		@endif
	@endif
	<hr>
	</div>
</div>
@stop