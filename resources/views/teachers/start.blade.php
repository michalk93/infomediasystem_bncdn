@extends('templates.teachers')

@section('css-styles')
	{{ HTML::style('css/teachers/main.css') }}
@stop

@section('site-title')
	Beskidzkie Niepubliczne Centrum Doskonalenia Nauczycieli
@stop

@section('title-header')
<span>
	BESKIDZKIE NIEPUBLICZNE<br/>
	<span>CENTRUM DOSKONALENIA NAUCZYCIELI</span>
</span>
@stop


{{------------------------------------}}
{{--     MAIN CONTENT SECTION       --}}
{{------------------------------------}}
@section('main-content')
	{{-- SLIDER --}}
	@if(!empty($slides))
		<div class="row">
			<div class="col-sm-12">
				<div id="homeCarousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
				@foreach($slides as $index => $slide)
					<li data-target="#homeCarousel" data-slide-to="{{ $index }}"></li>
				@endforeach
				</ol>
				<!-- Carousel items -->
				<div class="carousel-inner" role="listbox">
				@foreach($slides as $slide)
					<div class="item text-center">
						<a href="{{$slide->link}}">
							{{ HTML::image(\App\Models\Slider::$uploadPath.$school->name.'/'.$slide->filename, $slide->filename, array('height' => \App\Models\Slider::SLIDE_HEIGHT, 'width' => \App\Models\Slider::SLIDE_WIDTH)) }}
						</a>
					@if(!empty($slide->description))
					<a href="{{$slide->link}}">
	               		<div class="carousel-caption">
	                  		{!! $slide->description !!}
	                	</div>
	                </a>
	                @endif
	                </div>
	            @endforeach

				</div>
				<!-- Carousel nav -->
				<a class="carousel-control left" href="#homeCarousel" role="button" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#homeCarousel" role="button" data-slide="next">&rsaquo;</a>
			</div>
			</div>
		</div>
{{--		{{HTML::image('img/carou2sel-shadow.png', '')}}--}}
	@endif

	{{-- News --}}
	@if(isset($news) && !$news->isEmpty())
	<div id="news-wrapper" class="wrapper row">
		<section id="news" class="col-sm-12 col-lg-6">
			<header>
				<h3 class="color">AKTUALNOŚCI</h3>
			</header>
			<main>
				@foreach($news as $oneNews)
					<article class="news-item clearfix">
						@if($news->first() == $oneNews)
							<img class="pull-left" style="margin-right: 50px;" src="{{asset('img/news.jpg')}}" width="200" />
							<section>{!! \Illuminate\Support\Str::words($oneNews->content, 10) !!}</section>
						@else
							<div class="pull-left news-date">{{ $oneNews->printableDate['day'] }}<br>{{ $oneNews->printableDate['month'] }}</div>
							<section>{!! \Illuminate\Support\Str::words($oneNews->content, 20) !!}</section>
						@endif


						<footer class="text-right">
							<a data-toggle="modal" data-target="#newsDetailModal" data-href="{{ route('news', array('school' => $school->name, 'id' => $oneNews->id)) }}" class="btn btn-default more" title="Kliknij, aby przeczytać więcej">czytaj więcej</a>
						</footer>
					</article>
				@endforeach
				<div id="newsDetailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="newsDetailModal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h3 id="myModalLabel">Aktualności</h3>
							</div>
							<div class="modal-body">
								<article class="news-item clearfix">
									<div class="pull-left news-date"><span class="date-day"></span><br/><span class="date-month"></span></div>
									<section class="news-content"></section>
								</article>
							</div>
							<div class="modal-footer">
								<button class="btn pull-right" title="Kliknij, aby powrócić">Zamknij</button>
							</div>
						</div>
					</div>
				</div>
			</main>
			<footer>
				{{$news->links()}}
			</footer>
		</section>

		{{-- LAST ADDED OFFERS --}}
		<section id="last-offers" class="col-sm-12 col-lg-5 col-lg-offset-1">
			<header>
				<h3 class="color">STUDIA PODYPLOMOWE</h3>
			</header>
			<div class="clearfix">
			@if(!empty($postgraduateStudies))
				@foreach($postgraduateStudies as $study)
					<a href="{{ URL::route('offer-details', array($school->name, $study->id)) }}" class="offert-item left">
						<div class="offert-item-content clearfix">
							<div class="last-offer-title pull-left">
								<h5>
									{{ HTML::image('img/icon-offer.png', '', array('width' => 32, 'height' => 32, 'class' => 'pull-left')) }}
									{{ $study->name }}
								</h5>
							</div>
						</div>
					</a>
				@endforeach
				@include('subviews.modal-offer')
			@else
				<h4>Brak ofert dla studiów podyplomowych</h4>
			@endif
			</div>
			@include('subviews.sign-info')
			<section>
				<div class="pull-left coolcard""><a href="{{action('PagesController@show', [ $school->name,'wsparcie-dla-szko'])}}" rel="nofollow">
					{{HTML::image('img/wspomaganie2.jpg', 'System kompleksowego wspomagania pracy szkoły lub placówki oświatowej')}}
					{{--<div style="position: absolute; text-align: center; width: 100%; top: 60%; font: normal 16px Roboto,arial,sans-serif;">--}}
						{{--<p>WSPOMAGANIE SZKOŁY LUB PLACÓWKI OŚWIATOWEJ</p>--}}
						{{--<p style="margin-top: 20px; color: #737373; font-size:11px">Nowa forma wsparcia dla szkół i nauczycieli</p>--}}
					{{--</div>--}}
				</a></div>
				<div class="pull-right coolcard"><a href="http://sun-school.weebly.com/" rel="nofollow">
					{{HTML::image('img/sun-school.jpg', 'Język angielski dla dzieci i młodzieży')}}
					{{--<div style="position: absolute; text-align: center; width: 100%; top: 60%; font: normal 16px Roboto,arial,sans-serif;">--}}
						{{--<p>RAPORT</p>--}}
						{{--<p style="margin-top: 20px; color: #737373; font-size:11px">Z&nbsp;ewaluacji problemowej przeprowadzonej przez zespół wizytatorów Kuratorium Oświaty w&nbsp;Krakowie w&nbsp;dniach od&nbsp;10&nbsp;marca&nbsp;do&nbsp;10&nbsp;kwietnia&nbsp;2014&nbsp;r.</p>--}}
					{{--</div>--}}
				</a>
				</div>
			</section>
		</section>
	</div>
	@endif
	{{-- END News --}}
	<div class="clear"></div>
	
@stop