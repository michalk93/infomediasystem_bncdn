@extends('teachers.start')

@section('site-title')
    {{$site->title}}
@stop

@section('main-content')
    <div style="padding: 50px 20px">
        {!!  $site->content  !!}

        @if($site->files->count())
            <hr>
            <h5>Do pobrania</h5>
            <ul>
                @foreach($site->files as $file)
                    <li><a href="download/{{$file->id}}">{!! $file->description !!} (Klinkij, aby pobrać)</a></li>
                @endforeach
            </ul>
        @endif
    </div>

@stop