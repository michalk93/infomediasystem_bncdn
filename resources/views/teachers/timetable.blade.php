@extends('teachers.start')

@section('site-title')
	Harmonogramy
@stop

@section('description')
	Zobacz i pobierz aktualne harmonogramy naszych kursów.
@stop

@section('keywords')
	o bncdn, beskidzkie centrum doskonalenia nauczycieli, harmonogramy kursów, terminy szkoleń
@stop

@section('main-content')
<div id="view-timetable" class="wrapper">
	<div class="row">
		<div class="col-sm-12 col-md-3">
			<nav id="menu-offer" class="pull-left">
				<h4 class="color">Kategorie</h4>
				@if($categories)
					@foreach($categories as $category)
						@if($id_category == $category->id)
							<a href="{{route('timetable', array($school->name, $category->id))}}" class="item active">{{ $category->name }}</a>
						@else
							<a href="{{route('timetable', array($school->name, $category->id))}}" class="item">{{ $category->name }}</a>
						@endif
					@endforeach
				@else
					<h4 class="text-center color">Brak kategorii</h4>
				@endif
			</nav>
		</div>
		<div class="col-md-9" id="timetable-links">
			@if($timetables->isEmpty())
				<h4 class="text-center color">Brak harmonogramów dla kursów w tej kategorii</h4>
			@else
				<h4 class="text-center color">Dostępne harmonogramy dla kursów w wybranej kategorii</h4>
				<h6 class="text-center">Kliknij na dany harmonogram, aby go pobrać</h6>
				<table class="table">
					@foreach ($timetables as $timetable)
						<tr>
							<td>
								<a href="{{ asset(App\Models\Timetable::$storePath.$school->name.'/'.$timetable->filename) }}">{{ $timetable->description }}
									<span class="btn btn-sm btn-default pull-right"><span class="glyphicon glyphicon-download"></span> Pobierz</span></a>
							</td>

						</tr>
					@endforeach
				</table>
			@endif
		</div>
	</div>
</div>

@stop