@extends('teachers.start')

@section('site-title')
	Newsletter
@stop

@section('description')
	Dziękujemy za dotychczasową subskrybcję naszego newslettera
@stop

@section('keywords')
	o bncdn, beskidzkie centrum doskonalenia nauczycieli, newsletter, wypisanie, usunięcie subskrybcji
@stop

@section('main-content')
	<div class="row">
		<div class="col-sm-12 text-center">
			<h2>Newsletter</h2>
			<p>Podaj swój adres e-mail, aby wypisać się z newslettera</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-6 col-md-offset-3 text-center">
			{!! Form::open(['route' => ['newsletter-unsubscribe-post', $school->name]]) !!}
			<div class="input-prepend">
				<span class="add-on"><i class="icon-envelope"></i></span>
				<input type="text" class="form-control" name="email" placeholder="kowalski@gmail.com">
			</div>
			<div>
				@if(!empty($message))
					{{$message}}
				@endif
			</div>
			<input type="submit" value="Wypisz się" class="btn btn-large" />
			{!! Form::close() !!}
		</div>
	</div>

@stop