@extends('templates.main-tpl')

@section('css-styles')
    {{ HTML::style('css/'.$school->name.'/school-tpl.css') }}
    {{ HTML::style('css/'.$school->name.'/colors.css') }}
    {{ HTML::style('css/'.$school->name.'/adults-school.css') }}
@stop

@section('js-scripts')
    {{ HTML::script('js/whcookies.js') }}
    {{ HTML::script('js/jquery-1.10.1.min.js') }}
    {{ HTML::script('js/bncdn.js') }}
    <script>var BASE = "{{ Request::root() }}"; </script>
@stop

@section('body')
    <div id="social-icons">
        <a href="https://www.facebook.com/bncdn" class="social-link clearfix bg-color" target="_blank">
            <span>Facebook</span>
            {{ HTML::image('img/social-fb.png', 'FB', array('class' => 'social-icon pull-right', 'width' => 64, 'height' => 128)) }}
        </a>
        <a href="https://plus.google.com/110870634975103617434" rel="publisher" class="social-link clearfix bg-color" target="_blank">
            <span>Google+</span>
            {{ HTML::image('img/social-google.png', 'G+', array('class' => 'social-icon pull-right', 'width' => 64, 'height' => 128)) }}
        </a>
        <a href="https://twitter.com/BNCDN" class="social-link clearfix bg-color" target="_blank">
            <span>Twitter</span>
            {{ HTML::image('img/social-twitter.png', 'T', array('class' => 'social-icon pull-right', 'width' => 64, 'height' => 128)) }}
        </a>
    </div>
    {{-- TOP WRAPPER --}}
    <div id="top-wrapper" class="wrapper">
        <header class="clearfix" itemscope itemtype="http://data-vocabulary.org/Organization">
            <h4 class="left" itemprop="name">@yield('title-header')</h4>
            <h5 class="text-right">
                <address itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                    <span>34-200 <span itemprop="locality">Sucha Beskidzka</span>, <span itemprop="street-address">ul. Kościelna 5</span></span><br>
                    <span>e-mail: bncdn@poczta.fm | tel/fax <span itemprop="tel">(33) 874 45 36</span></span>
                </address>
            </h5>
        </header>
    </div>

    {{-- NAVIGATION AND SEARCH ENGINE --}}
    <div id="nav-wrapper" class="wrapper bg-color">
        <div class="clearfix">
            <nav class="left clearfix">
                <a href="{{route('home') }}" class="left" data-name="home" title="Strona startowa">{{HTML::image('img/home-icon.png', "H", array("height" => 35, 'width' => 39))}}</a>
                <a href="{{route('school-home', $school->name)}}" class="left" title="Strona główna" data-name="teacherschool">Home</a>
                <a href="{{route('offer', $school->name)}}" class="left" title="Oferta kształcenia" data-name="offert">Oferta</a>
                <a href="{{route('recruitment', $school->name)}}" class="left" title="Rekrutacja">Rekrutacja</a>
                <a href="{{route('about', $school->name)}}" class="left" title="O naszej placówce">O nas</a>
                <a href="{{route('timetable', $school->name)}}" class="left" title="Harmonogramy kursów">Harmonogramy</a>
                <a href="{{route('contact', $school->name)}}" class="left" title="Kontakt">Kontakt</a>
            </nav>
            <form action="{{ route('search', $school->name) }}" id="search_form" class="right" method="post">
                <input type="text" placeholder="Szukaj..." name="search_text" id="input-search-text" class="left">
                <input type="image" src="{{ URL::to('img/find-icon.png') }}" id="input-btn-image" title="Szukaj">
            </form>
        </div>
    </div>


    {{-- MAIN DYNAMIC CONTENT --}}
    <div id="main-wrapper" class="wrapper clearfix">
        <main class="clearfix">
            @yield('main-content')
        </main>
    </div>

    {{-- SPECIAL LINKS --}}
    <div id="special-links-wrapper" class="wrapper clearfix">
        <section class="row">
            <div class="span4 thumbnail text-center">
                <div>
                    <h4 class="text-center">Newsletter</h4>
                    {{ HTML::image('img/newsletter.jpg', "newsletter.jpg", array('width' => 260, 'height' => 180)) }}
                    <p>Bądź na bierząco! Podaj adres e-mail, aby otrzymywać aktualne informacje na temat kursów i szkoleń. Nic nie ujdzie Twojej uwadze.</p>
                    <div class="link"><h4>{{ HTML::linkRoute('newsletter', 'Zapisz się', $school->name) }}</h4></div>

                </div>
            </div>
            <div class="span4 thumbnail text-center">
                <div>
                    <h4  class="text-center">Pytania i odpowiedzi</h4>
                    {{ HTML::image('img/forum.jpg', "forum.jpg", array('width' => 260, 'height' => 180)) }}

                    <p>Nie jesteś czegoś pewien? Komunikuj się z innymi użytkownikami. Od nich dowiesz się więcej - pytaj i odpowiadaj. Zajrzyj do systemy pytań i odpowidzi BNCDN Q&A.</p>
                    <div class="link"><h4 class="text-center"><a href="#">Przejdź do BNCDN Q&A</a></h4></div>
                </div>
            </div>
            <div class="span4 thumbnail text-center">
                <div>
                    <h4 class="text-center">Platforma e-learningowa</h4>
                    {{ HTML::image('img/elearning.jpg', "elearning.jpg", array('width' => 260, 'height' => 180)) }}
                    <p>Wychodząc naprzeciw nowym rozwiązaniom oraz technice, nauczamy z wykorzystaniem komputerów oraz sieci Internet. Teraz nauka jet łatwa i przyjemna.</p>
                    <div class="link"><h4 class="text-center"><a href="http://platforma.bncdn.pl">Przejdź do platformy</a></h4></div>

                </div>
            </div>
        </section>
    </div>

    {{-- FOOTER --}}
    <div id="footer-wrapper" class="wrapper clearfix bg-color">
        <footer>
            Copyright © {{date('Y')}} bncdn.pl, All rights reserved
        </footer>
    </div>
@stop
