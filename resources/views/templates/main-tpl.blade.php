<!doctype html>
<html lang="pl">
<head>
	<meta name="google-site-verification" content="KWl80wVuI3RIR8KsN2PQOtqeXdcWaB5mEUmXc0-o-4k" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="@yield('description', 'Beskidzkie Niepubliczne Centrum Doskonalenia Nauczycieli jest placówką pomagającą dyrektorom i nauczycielom w rozwoju zawodowym poprzez różne formy doskonalenia, takie jak kursy kwalifikacyjne, kursy, warsztaty oraz seminaria')">
	<meta name="keywords" content="@yield('keywords', 'bncdn, beskidzkie centrum doskonalenia nauczycieli, kursy kwalifikacyjne, kursy sucha beskidzka, ośrodek doskonalenia nauczycieli, kursy pedagogiczne dla nauczycieli, małopolskie centrum doskonalenia nauczycieli')">
	<link href="{{url('favicon.ico')}}" rel="icon" type="image/x-icon" />
	{{--SECTION FOR CSS STYLES--}}
	@yield('css-styles')

	{{--SECTION FOR JS SCRIPTS--}}
	@yield('js-head')

	<title>@yield('site-title') | BNCDN</title>
</head>
<body>
	@yield('body')
	<!--[if lt IE 9]>
	{{ HTML::script("js/html5shiv.js") }}
	<![endif]-->
	{{ HTML::script('js/whcookies.js', array('type' => 'text/javascript')) }}
	{{--{{ HTML::script('js/jquery-1.10.1.min.js', array('type' => 'text/javascript')) }}--}}
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
	@yield('js-scripts')
</body>
</html>