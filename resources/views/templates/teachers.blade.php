@extends('templates.main-tpl')

@section('js-scripts')
    {{ HTML::script('js/bootstrap/bootstrap.min.js', array('type' => 'text/javascript')) }}
	{{ HTML::script('js/bncdn.js') }}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react-dom.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js"></script>
	{{HTML::script('js/react-components.js', ['type' => 'text/babel'])}}

	<script>var BASE = "{{ Request::root() }}"; </script>
	<script type="text/javascript">
		$('#nav-wrapper').on('affixed.bs.affix', function(){
			$('#nav-scroll-title').show().animate({width: 280});
		});
		$('#nav-wrapper').on('affix-top.bs.affix', function(){
			$('#nav-scroll-title').animate({'width': '0'}, function(){$(this).hide()});
		})

		$('#trigger-search-form').on('click', function(){
			$('#search-form').slideToggle();
		})
	</script>
@stop

@section('body')
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.7&appId=503619179749987";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	<div id="social-icons">
		<a href="https://www.facebook.com/bncdn" class="social-link clearfix bg-color" target="_blank">
			<span>Facebook</span>
			{{ HTML::image('img/social-fb.png', 'FB', array('class' => 'social-icon pull-right', 'width' => 64, 'height' => 128)) }}
		</a>
	</div>
	{{-- TOP WRAPPER --}}
	<div id="top-wrapper" class="wrapper">
		<div class="container">
			<header class="row" itemscope itemtype="http://data-vocabulary.org/Organization">
				<div class="col-sm-12 col-lg-6">
					<h4 itemprop="name">@yield('title-header')</h4>
				</div>
				<div class="col-sm-12 col-lg-6">
					<div class="row">
						<div class="col-sm-7">
							<h5>
								<img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDQ2Ni41ODMgNDY2LjU4MiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDY2LjU4MyA0NjYuNTgyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggZD0iTTIzMy4yOTIsMGMtODUuMSwwLTE1NC4zMzQsNjkuMjM0LTE1NC4zMzQsMTU0LjMzM2MwLDM0LjI3NSwyMS44ODcsOTAuMTU1LDY2LjkwOCwxNzAuODM0ICAgYzMxLjg0Niw1Ny4wNjMsNjMuMTY4LDEwNC42NDMsNjQuNDg0LDEwNi42NGwyMi45NDIsMzQuNzc1bDIyLjk0MS0zNC43NzRjMS4zMTctMS45OTgsMzIuNjQxLTQ5LjU3Nyw2NC40ODMtMTA2LjY0ICAgYzQ1LjAyMy04MC42OCw2Ni45MDgtMTM2LjU1OSw2Ni45MDgtMTcwLjgzNEMzODcuNjI1LDY5LjIzNCwzMTguMzkxLDAsMjMzLjI5MiwweiBNMjMzLjI5MiwyMzMuMjkxYy00NC4xODIsMC04MC0zNS44MTctODAtODAgICBzMzUuODE4LTgwLDgwLTgwYzQ0LjE4MiwwLDgwLDM1LjgxNyw4MCw4MFMyNzcuNDczLDIzMy4yOTEsMjMzLjI5MiwyMzMuMjkxeiIgZmlsbD0iIzY2Y2MwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" class="pull-left" />
							<address class="location" itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
								<span>Budynek Zespołu Szkół im. W. Goetla</span><br/>
								<span>34-200 <span itemprop="locality">Sucha Beskidzka</span>,
								<span itemprop="street-address">ul. Kościelna 5</span>
								</span>
							</address>
							</h5>
						</div>
						<div class="col-sm-5">
							<h5>
								<img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDMyLjY2NiAzMi42NjYiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMyLjY2NiAzMi42NjY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8cGF0aCBkPSJNMjguMTg5LDE2LjUwNGgtMS42NjZjMC01LjQzNy00LjQyMi05Ljg1OC05Ljg1Ni05Ljg1OGwtMC4wMDEtMS42NjRDMjMuMDIxLDQuOTc5LDI4LjE4OSwxMC4xNDksMjguMTg5LDE2LjUwNHogICAgTTE2LjY2Niw3Ljg1NkwxNi42NjUsOS41MmMzLjg1MywwLDYuOTgzLDMuMTMzLDYuOTgxLDYuOTgzbDEuNjY2LTAuMDAxQzI1LjMxMiwxMS43MzUsMjEuNDM2LDcuODU2LDE2LjY2Niw3Ljg1NnogTTE2LjMzMywwICAgQzcuMzI2LDAsMCw3LjMyNiwwLDE2LjMzNGMwLDkuMDA2LDcuMzI2LDE2LjMzMiwxNi4zMzMsMTYuMzMyYzAuNTU3LDAsMS4wMDctMC40NSwxLjAwNy0xLjAwNmMwLTAuNTU5LTAuNDUtMS4wMS0xLjAwNy0xLjAxICAgYy03Ljg5NiwwLTE0LjMxOC02LjQyNC0xNC4zMTgtMTQuMzE2YzAtNy44OTYsNi40MjItMTQuMzE5LDE0LjMxOC0xNC4zMTljNy44OTYsMCwxNC4zMTcsNi40MjQsMTQuMzE3LDE0LjMxOSAgIGMwLDMuMjk5LTEuNzU2LDYuNTY4LTQuMjY5LDcuOTU0Yy0wLjkxMywwLjUwMi0xLjkwMywwLjc1MS0yLjk1OSwwLjc2MWMwLjYzNC0wLjM3NywxLjE4My0wLjg4NywxLjU5MS0xLjUyOSAgIGMwLjA4LTAuMTIxLDAuMTg2LTAuMjI4LDAuMjM4LTAuMzU5YzAuMzI4LTAuNzg5LDAuMzU3LTEuNjg0LDAuNTU1LTIuNTE4YzAuMjQzLTEuMDY0LTQuNjU4LTMuMTQzLTUuMDg0LTEuODE0ICAgYy0wLjE1NCwwLjQ5Mi0wLjM5LDIuMDQ4LTAuNjk5LDIuNDU4Yy0wLjI3NSwwLjM2Ni0wLjk1MywwLjE5Mi0xLjM3Ny0wLjE2OGMtMS4xMTctMC45NTItMi4zNjQtMi4zNTEtMy40NTgtMy40NTdsMC4wMDItMC4wMDEgICBjLTAuMDI4LTAuMDI5LTAuMDYyLTAuMDYxLTAuMDkyLTAuMDkyYy0wLjAzMS0wLjAyOS0wLjA2Mi0wLjA2Mi0wLjA5My0wLjA5MnYwLjAwMmMtMS4xMDYtMS4wOTYtMi41MDYtMi4zNC0zLjQ1Ny0zLjQ1OSAgIGMtMC4zNi0wLjQyNC0wLjUzNC0xLjEwMi0wLjE2OC0xLjM3N2MwLjQxLTAuMzExLDEuOTY2LTAuNTQzLDIuNDU4LTAuNjk5YzEuMzI2LTAuNDI0LTAuNzUtNS4zMjgtMS44MTYtNS4wODQgICBjLTAuODMyLDAuMTk1LTEuNzI3LDAuMjI3LTIuNTE2LDAuNTUzYy0wLjEzNCwwLjA1Ny0wLjIzOCwwLjE2LTAuMzU5LDAuMjRjLTIuNzk5LDEuNzc0LTMuMTYsNi4wODItMC40MjgsOS4yOTIgICBjMS4wNDEsMS4yMjgsMi4xMjcsMi40MTYsMy4yNDUsMy41NzZsLTAuMDA2LDAuMDA0YzAuMDMxLDAuMDMxLDAuMDYzLDAuMDYsMC4wOTUsMC4wOWMwLjAzLDAuMDMxLDAuMDU5LDAuMDYyLDAuMDg4LDAuMDk1ICAgbDAuMDA2LTAuMDA2YzEuMTYsMS4xMTgsMi41MzUsMi43NjUsNC43NjksNC4yNTVjNC43MDMsMy4xNDEsOC4zMTIsMi4yNjQsMTAuNDM4LDEuMDk4YzMuNjctMi4wMjEsNS4zMTItNi4zMzgsNS4zMTItOS43MTkgICBDMzIuNjY2LDcuMzI2LDI1LjMzOSwwLDE2LjMzMywweiIgZmlsbD0iIzY2Y2MwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" class="pull-left" />
								<div class="contact">
									<span>e-mail: bncdn@poczta.fm</span><br/>
									<span>tel/fax: <span itemprop="tel">(33) 874 45 36</span></span>
								</div>
							</h5>
						</div>
					</div>
					{{--<h5 class="text-right">--}}
						{{--<address itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">--}}
							{{--<span>34-200 <span itemprop="locality">Sucha Beskidzka</span>, <span itemprop="street-address">ul. Kościelna 5</span></span><br>--}}
							{{--<span>e-mail: bncdn@poczta.fm | tel/fax <span itemprop="tel">(33) 874 45 36</span></span>--}}
						{{--</address>--}}
					{{--</h5>--}}
				</div>
			</header>
		</div>
	</div>

	{{-- NAVIGATION AND SEARCH ENGINE --}}
	<div id="nav-wrapper" class="wrapper clearfix" data-spy="affix" data-offset-top="100">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-11">
					<div id="nav-scroll-title" class="pull-left">
						<h4 itemprop="name">@yield('title-header')</h4>
					</div>
					<nav>
						{{--<a href="{{route('home') }}" class="left" data-name="home" title="Strona startowa">{{HTML::image('img/home-icon.png', "H", array("height" => 35, 'width' => 39))}}</a>--}}
						<a href="{{route('school-home', $school->name)}}" class="pull-left {{\App\Helpers\ViewHelpers::isActiveRoute('school-home')}}" title="Strona główna" data-name="teacherschool">Home</a>
						<a href="{{route('offer', $school->name)}}" class="pull-left {{\App\Helpers\ViewHelpers::isActiveRoute('offer')}}" title="Oferta kształcenia" data-name="offert">Oferta</a>
						<a href="{{route('recruitment', $school->name)}}" class="pull-left {{\App\Helpers\ViewHelpers::isActiveRoute('recruitment')}}" title="Rekrutacja">Rekrutacja</a>
						<a href="{{route('about', $school->name)}}" class="pull-left {{\App\Helpers\ViewHelpers::isActiveRoute('about')}}" title="O naszej placówce">O nas</a>
						<a href="{{route('timetable', $school->name)}}" class="pull-left {{\App\Helpers\ViewHelpers::isActiveRoute('timetable')}}" title="Harmonogramy kursów">Harmonogramy</a>
						<a href="{{route('contact', $school->name)}}" class="pull-left {{\App\Helpers\ViewHelpers::isActiveRoute('contact')}}" title="Kontakt">Kontakt</a>

					</nav>
				</div>
				<div class="col-sm-12 col-lg-1 text-right">
					<img id="trigger-search-form" role="button" src="{{ URL::to('img/find-icon.png') }}" alt="Szukaj"/>
				</div>
			</div>

			<div class="row" id="search-form">
				<div class="col-sm-12">
					<form action="{{ route('search', $school->name) }}" method="get">
						<input type="text" placeholder="Wpisz czego szukasz..." name="search_text">
						<p><button type="submit" class="btn btn-primary">Szukaj</button></p>
					</form>
				</div>
			</div>
		</div>
	</div>

	</div>


	{{-- MAIN DYNAMIC CONTENT --}}
	<main id="main-wrapper" class="wrapper clearfix">
		<div class="container">
				@include('subviews.messages')
				@yield('main-content')
		</div>
	</main>
	
	{{-- FOOTER --}}
	<footer id="footer-wrapper" class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-8">
					<div class="row">
						<div class="col-sm-12">
							<section class="title">
								<h4 itemprop="name">@yield('title-header')</h4>
							</section>
						</div>
					</div>
					<div class="row">
						<div id="services" class="col-sm-12 col-lg-5">
							<section id="newsletter">
								{{--<h4 class="text-center">Newsletter</h4>--}}
								{{--						{{ HTML::image('img/newsletter.jpg', "newsletter.jpg", array('width' => 260, 'height' => 180)) }}--}}
								{{--<form action="{{route('newsletter-post', $school)}}" method="post">--}}
									{{--<p>{{$errors->first('email')}}</p>--}}
									{{--<input type="text" name="email" placeholder="kowalski@gmail.com">--}}
									{{--<button type="submit" class="btn btn-default btn-sm">Zapisz się</button>--}}
								{{--</form>--}}
							</section>

							<section>
								<h4 class="text-center">Platforma e-learningowa</h4>
								{{--						{{ HTML::image('img/elearning.jpg', "elearning.jpg", array('width' => 260, 'height' => 180)) }}--}}
								<p class="text-justify">Wychodząc naprzeciw nowym rozwiązaniom oraz technice, nauczamy z wykorzystaniem komputerów oraz sieci Internet. Teraz nauka jet łatwa i przyjemna.</p>
								<a href="http://platforma.bncdn.pl" class="btn btn-default btn-sm">Przejdź do platformy</a>
							</section>
						</div>
						<div class="col-sm-12 col-lg-6 col-lg-offset-1">
							<h4>ZOBACZ TAKŻE</h4><br/>
							<h5><a href="http://sun-school.weebly.com/" target="_blank">SUN SCHOOL</a></h5>
							<p>Język angielski dla dzieci i młodzieży</p>
							<br/>
							<h5><a href="{{route('school-home', 'adults')}}" target="_blank">CENTRUM KSZTAŁCENIA DOROSŁYCH</a></h5>
							<p>Kursy i szkolenia dla osób dorosłych</p>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-lg-4">
					<section style="clear:both; padding-top: 10px">
						<div class="fb-page"
							 data-tabs="messages,timeline"
							 data-href="https://www.facebook.com/bncdn"
							 data-width="380"
							 data-height="380"
							 data-hide-cover="false"></div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-lg-6">
					Copyright © {{date('Y')}} bncdn.pl, All rights reserved
				</div>
				<div class="col-sm-12 col-lg-6 text-right">
					<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
				</div>
			</div>

		</div>
	</footer>
@stop
